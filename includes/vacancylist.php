<?php
require_once("localobjectlist.php");
require_once("functions.php");
require_once("vacancy.php");

class VacancyList extends LocalObjectList
{
  var $message = "";
  function LoadFromDataBase($page = 1, $front = 0)
  {
    $start = 0;
    if ($page > 0)
    {
      $start = ($page - 1)*ITEMS_PER_PAGE;
    }
    $rows = array();
    $str = "";
    if ($front == 1)
    {
      $str = " WHERE vacActive = 1 ";
    }
    $query = "SELECT *
          FROM vacancy ".$str."
          ORDER BY vacSortOrder
          LIMIT ".$start.", ".ITEMS_PER_PAGE;
    $this->LoadFromSQL($query);
    for ($i=0;$i<$this->GetTotalCount();$i++) {
      if (isset($this->items[$i]) && isset($this->items[$i]['vacContent']))
      {
        $this->items[$i]["Preview"] = CreatePreview($this->items[$i]['vacContent']);
      }
    }
  }


  function GetTotalCount($front = 0)
  {
    global $corporate;
    $str = "";
    if ($front == 1)
    {
      $str = " WHERE vacActive = 1 ";
    }
    $query = "SELECT count(*) AS total FROM vacancy ".$str;
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }

  function Delete($data)
  {
    if (is_array($data) && $data)
    {
      $ids = implode(",", Connection::GetSQLArray($data));
      if ($ids)
      {
        $query = "DELETE FROM vacancy WHERE vacID IN (".$ids.")";
        $stmt = GetStatement();
        $stmt->Execute($query);

      }
    }
  }

	function MoveField($vacID, $updown)
	{
		$vac = new Vacancy();
		$vac->SetProperty('vacID', $vacID);
		$vac->LoadFromDataBase();
		$row = null;
		if (is_numeric($updown) && is_numeric($vacID))
		{
			$stmt = GetStatement();
			if ($updown == 1)
			{
				$query = "SELECT * FROM vacancy
						WHERE vacSortOrder >= ".intval($vac->GetProperty('vacSortOrder'))."
						AND vacID<>".intval($vacID)."
						ORDER BY vacSortOrder ASC
						LIMIT 1";
				$row = $stmt->FetchRow($query);
			}
			elseif ($updown == -1)
			{
				$query = "SELECT * FROM vacancy
						WHERE vacSortOrder <= ".intval($vac->GetProperty('vacSortOrder'))."
						AND vacID<>".intval($vacID)."
						ORDER BY vacSortOrder DESC
						LIMIT 1";
				$row = $stmt->FetchRow($query);
			}
			if ($row)
			{
				$query = "UPDATE vacancy SET
					vacSortOrder=".intval($row['vacSortOrder'])."
					WHERE vacID=".intval($vacID);
				$stmt->Execute($query);

				$query = "UPDATE vacancy SET
					vacSortOrder=".intval($vac->GetProperty('vacSortOrder'))."
					WHERE vacID=".intval($row['vacID']);
				$stmt->Execute($query);
			}
		}
	}

}
?>