<?php
require_once("localobject.php");

class Admin extends LocalObject
{
	function LoadFromDataBase()
	{
		if ($this->GetProperty('AdminID'))
		{
			$query = "SELECT *, IF(AdminLevel = 1, '', ' selected') as selected
					FROM admins
					WHERE AdminID = ".$this->GetPropertyForSQL('AdminID');
			$this->LoadFromSQL($query);
		}
	}

	function CheckInformation()
	{
		if ($this->GetProperty('AdminName') == null)
		{
			return "������� ���";
		}
		elseif ($this->GetProperty('AdminNick') == null)
		{
			return "������� �����";
		}
		elseif ($this->LoginExists())
		{
			return "����� ������������ ��� ����������";
		}
		return null;
	}

	function LoginExists()
	{
		$query = "SELECT count(*) as total FROM admins
				WHERE AdminNick=".$this->GetPropertyForSQL('AdminNick')."
				AND AdminID<>".$this->GetPropertyForSQL('AdminID');
		$stmt = GetStatement();
		$n = $stmt->FetchField($query, "total");
		if ($n)
		{
			return true;
		}
		return false;
	}

	//add/edit
	function Update()
	{
		global $post;
		$str = "AdminNick=".$this->GetPropertyForSQL('AdminNick').", AdminLevel =".$this->GetPropertyForSQL('AdminLevel').", AdminEmail=".$this->GetPropertyForSQL('AdminEmail').", AdminName=".$this->GetPropertyForSQL('AdminName').", AdminActive='".$post->GetProperty('AdminActive')."', AdminLevelName=".$this->GetPropertyForSQL('AdminLevelName');
		if ($this->GetProperty('AdminCoflexID') != 0)
		{                                       
			$str .= ", AdminCoflexID=".$this->GetPropertyForSQL('AdminCoflexID');
		}
		//echo($str);
		//exit;
		if ($this->GetProperty('AdminPassword'))
		{
			$str .= ", AdminPassword=".$this->GetPropertyForSQL('AdminPassword');
		}
		if ($this->GetProperty('AdminID'))
		{
			$query = "UPDATE admins SET ".$str." WHERE AdminID=".$this->GetPropertyForSQL('AdminID');
		}
		else
		{
			$query = "INSERT INTO admins SET ".$str.", AdminDateCreated=NOW()";
		}
		$stmt = GetStatement();
		$stmt->Execute($query);
	}


}


?>