<?php
require_once("localobject.php");

class DezSred extends LocalObject
{
	var $error = null;
	var $filesDir;

	function DezSred($filesDir = '', $data = array())
	{
		if ($filesDir && is_dir($filesDir))
		{
			$this->filesDir = $filesDir;
		}
		else
		{
			$this->filesDir = FILE_DEZ_SRED;
		}
		$this->LoadFromArray($data);
	}

	function CheckInformation($required = true)
	{
		if ($required)
		{
			if ($this->GetProperty('error') == 4)
			{
				return "�������� ����";
			}
			elseif ($this->GetProperty('size') == 0)
			{
				return "���� ������";
			}
		}
		return null;
	}


  function Add()
	{
		if (move_uploaded_file($this->GetProperty('tmp_name'), FILE_DEZ_SRED))
		{
			return $this->GetProperty('name');
		} else {
      return false;
    }
	}

}
?>