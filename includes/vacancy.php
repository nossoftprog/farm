<?php

class Vacancy extends LocalObject
{
  var $error = null;
  var $message = null;
  var $currLang;

  function Vacancy()
  {
  }

  function LoadFromDataBase()
  {
    if ($this->GetProperty("vacID"))
    {
      $query = "SELECT *
        FROM vacancy
        WHERE vacID = ".$this->GetPropertyForSQL("vacID");
      $this->LoadFromSQL($query);
    }
  }

  function CheckAddInformation()
  {
    return $this->CheckInformation();
  }

  function CheckUpdateInformation()
  {
    return $this->CheckInformation();
  }

  function CheckInformation()
  {
    if ($this->GetProperty("vacHeader") == null)
    {
      return "������� ���������";
    }
    elseif ($this->GetProperty("vacContent") == null)
    {
      return "��������� ��������";
    }
    return null;
  }

  //add/edit
  function Update()
  {
    global $post;
    $str = "vacHeader=".$this->GetPropertyForSQL('vacHeader').", vacContent =".$this->GetPropertyForSQL('vacContent').",  vacActive=".$post->GetPropertyForSQL('vacActive');
    if ($this->GetProperty('vacID'))
    {
      $query = "UPDATE vacancy SET ".$str." WHERE vacID=".$this->GetPropertyForSQL('vacID');
    }
    else
    {
      $query = "INSERT INTO vacancy SET ".$str;
    }
    $stmt = GetStatement();
    $stmt->Execute($query);
		if ($vacID = $stmt->GetLastInsertID())
		{
			$this->SetProperty('vacID', $vacID);
			$query = "UPDATE vacancy SET vacSortOrder=".$vacID." WHERE vacID=".$vacID;
			$stmt->Execute($query);
		}

  }
}
?>