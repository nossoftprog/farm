<?php

class News extends LocalObject
{
  var $error = null;
  var $message = null;
  var $currLang;

  function News()
  {
  }

  function LoadFromDataBase()
  {
    if ($this->GetProperty("NewsID"))
    {
      $query = "SELECT *, DATE_FORMAT(NewsDate, '".USER_DATE_FORMAT."') AS NewsDate1
        FROM news
        WHERE NewsID = ".$this->GetPropertyForSQL("NewsID");
      $this->LoadFromSQL($query);
    }
  }

  function CheckAddInformation()
  {
    return $this->CheckInformation();
  }

  function CheckUpdateInformation()
  {
    return $this->CheckInformation();
  }

  function CheckInformation()
  {
    if ($this->GetProperty("NewsHeader") == null)
    {
      return "������� ���������";
    }
    elseif ($this->GetProperty("NewsContent") == null)
    {
      return "�������� �������";
    }
    return null;
  }

  //add/edit
  function Update()
  {
    global $corporate;
    global $post;
    $dates = split("-", $post->GetProperty('NewsDate'));
    $str = "NewsHeader=".$this->GetPropertyForSQL('NewsHeader').", NewsContent =".$this->GetPropertyForSQL('NewsContent').", NewsCenter=".$post->GetPropertyForSQL('NewsCenter').", NewsRight=".$post->GetPropertyForSQL('NewsRight').", NewsActive=".$post->GetPropertyForSQL('NewsActive').", NewsCorporate=".$corporate.", NewsDate='".$dates[2]."-".$dates[1]."-".$dates[0]."'";
    if ($this->GetProperty('NewsID'))
    {
      $query = "UPDATE news SET ".$str." WHERE NewsID=".$this->GetPropertyForSQL('NewsID');
    }
    else
    {
      $query = "INSERT INTO news SET ".$str;
    }
    $stmt = GetStatement();
    $stmt->Execute($query);
		if ($newsID = $stmt->GetLastInsertID() && !$this->GetProperty('NewsID'))
		{
			$this->SetProperty('NewsID', $newsID);
			$query = "UPDATE news SET NewsSortOrder=".$newsID." WHERE NewsID=".$newsID;
			$stmt->Execute($query);
		}

  }
}
?>