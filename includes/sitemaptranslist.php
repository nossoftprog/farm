<?php
require_once("localobjectlist.php");

class SitemapTransList extends LocalObjectList
{
	var $message = "";
	var $pageID;
	var $languageID;

	function SitemapTransList($pageID, $langID)
	{
		$this->pageID = $pageID;
		$this->languageID = $langID;
	}

	function LoadFromDataBase()
	{
		$query = "SELECT *, 1 AS Translated
  			FROM sitemap_trans
			WHERE LanguageID=".intval($this->languageID)." AND PageID=".intval($this->pageID)."
			ORDER BY PageTransID DESC";
		$this->LoadFromSQL($query);
		if (!$this->items)
		{
			$query = "SELECT *, 0 AS Translated
					FROM language AS l
					LEFT  JOIN sitemap_trans AS t ON l.LanguageID = t.LanguageID
					WHERE t.PageID=".intval($this->pageID)."
					AND l.LanguageID<>".intval($this->languageID)."
					AND t.PageTransID IS NOT NULL
					ORDER  BY l.SortOrder
					LIMIT 1";
			$this->LoadFromSQL($query);
		}
	}

}
?>