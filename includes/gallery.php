<?php

class Gallery extends LocalObject
{
  var $error = null;
  var $message = null;
  var $currLang;

  function Gallery()
  {
  }

  function LoadFromDataBase()
  {
    if ($this->GetProperty("NewsID"))
    {
      $query = "SELECT *
        FROM gallery
        WHERE NewsID = ".$this->GetPropertyForSQL("NewsID");
      $this->LoadFromSQL($query);
    }
  }

  function CheckAddInformation()
  {
    return $this->CheckInformation();
  }

  function CheckUpdateInformation()
  {
    return $this->CheckInformation();
  }

  function CheckInformation()
  {
    if ($this->GetProperty("NewsHeader") == null)
    {
      return "������� ���������";
    }
    elseif ($this->GetProperty("NewsContent") == null)
    {
      return "��������� �������";
    }
    return null;
  }

  //add/edit
  function Update()
  {
    global $corporate;
    global $post;
    $dates = split("-", $post->GetProperty('NewsDate'));
    $str = "NewsHeader=".$this->GetPropertyForSQL('NewsHeader').", NewsContent =".$this->GetPropertyForSQL('NewsContent').",  NewsActive=".$post->GetPropertyForSQL('NewsActive').", NewsCorporate=".$corporate;
    if ($this->GetProperty('NewsID'))
    {
      $query = "UPDATE gallery SET ".$str." WHERE NewsID=".$this->GetPropertyForSQL('NewsID');
    }
    else
    {
      $query = "INSERT INTO gallery SET ".$str;
    }
    $stmt = GetStatement();
    $stmt->Execute($query);
		if ($newsID = $stmt->GetLastInsertID())
		{
			$this->SetProperty('NewsID', $newsID);
			$query = "UPDATE gallery SET NewsSortOrder=".$newsID." WHERE NewsID=".$newsID;
			$stmt->Execute($query);
		}

  }
}
?>