<?php
require_once("localobjectlist.php");
require_once("functions.php");
require_once("product.php");
require_once("ostlist.php");



class ProductList extends LocalObjectList
{
  var $message = "";

  function LoadFromDataBase($page = 1)
  {
    $start = 0;
    if ($page > 0)
    {
      $start = ($page - 1)*ITEMS_PER_PAGE;
    }
    $str = "";
//    if (isset($_SESSION['searchProd']))
//    {
//		  $str = Connection::GetSQLLike($_SESSION['searchProd']);
//          $strSearch = " AND MATCH (priceDesc, priceVendor) AGAINST ('" . Connection::GetSQLToSearch($_SESSION['searchProd']). "' IN BOOLEAN MODE)";
//    }
    $query = "SELECT *
          FROM price WHERE priceHistory=1".$this->SearchString()."
          ORDER BY priceDesc
          LIMIT ".$start.", ".ITEMS_PER_PAGE;
     //echo $query;
     //exit;
    $this->LoadFromSQL($query);
  }

  function LoadFromDataBaseStatistic($selected = 0)
  {
    $query = "SELECT *, IF(priceID = '" . $selected . "', 1, 0) as selected
          FROM price WHERE priceHistory=1 
          ORDER BY priceDesc";
    $this->LoadFromSQL($query);
  }

  function LoadFromDataBaseForCustomer($page = 1, $filter = '0')
  {
    $start = 0;
    $counter = ITEMS_PER_PAGE;
    if ($_SESSION['CatType'] == 0) {

    }

    /* Products will be soon */
    /*$_SESSION['ozid1'] = 0;
    $_SESSION['ozid2'] = 0;
    $_SESSION['ozid3'] = 0;
    $query = "SELECT  DISTINCT * FROM category WHERE CatDescription LIKE '%�������%' AND CatHistory=1";
		$conn = GetConnection();
    $stmt = $conn->CreateStatement();
    $rs = $stmt->Execute($query);
    if ($stmt->GetNumRows())
  	{
      if ($row = $rs->NextRow())
      {
        $_SESSION['ozid1'] = $row['CatID'];
      }
      if ($row = $rs->NextRow())
      {
        $_SESSION['ozid2'] = $row['CatID'];
      }
      if ($row = $rs->NextRow())
      {
        $_SESSION['ozid3'] = $row['CatID'];
      }
    }*/
    /*------------------*/

    if (WHOLE_PRICE == 1) {
      $counter = 50000; //$this->GetTotalCountForCustomer();
      //echo($counter); 1766
    }
    if ($page > 0 && WHOLE_PRICE != 1)
    {
      $start = ($page - 1)*ITEMS_PER_PAGE;
    }

    if ($_SESSION['CatType'] == 0) {
      if ($page == 1 ) {
        $counter = ITEMS_PER_PAGE_ALPHABET_FIRST;
        $start = 0;
      } else {
        $counter = ITEMS_PER_PAGE_ALPHABET_AJAX;
        $start = ITEMS_PER_PAGE_ALPHABET_FIRST + ($page - 2)*ITEMS_PER_PAGE_ALPHABET_AJAX;
      }

    }

    $str = "";
    if (isset($_SESSION['searchProd'])) 
    {
		  $str = Connection::GetSQLLike($_SESSION['searchProd']);
    }
    $HistOrderIDTemp=0;
    if (isset($_SESSION['OrderID']) && $_SESSION['OrderID'])
    {
      $HistOrderIDTemp = $_SESSION['OrderID'];
    }
    /*$query = "SELECT  DISTINCT price.PricePrice, price.PriceEntry, price.PriceTork, price.priceID, price.priceVendor, price.priceDesc, price.pricePack, price.PriceSize, price.PriceDate, price.PriceOstat, category.CatDescription, category.CatTreeLevel, category.CatNameTree, category.CatTree, category.CatID, category.CatIndex, price_cat.PriceCatCatID, price_addition.priceAddPriceID, price_addition.PriceAddTemp, history_.HistValue, price.priceHistory, price_status.*
          FROM (price, category, price_cat) LEFT JOIN price_addition ON price_addition.priceAddPriceID=price.priceID LEFT JOIN history_ ON history_.HistPriceID=price.priceID AND history_.HistOrderID='".$HistOrderIDTemp."' LEFT JOIN price_status ON price_status.PriceStatID = price_addition.priceAddTemp  WHERE price.priceID=price_cat.PriceCatPriceID AND category.CatID=price_cat.PriceCatCatID AND ( price_addition.PriceAddTemp!='99990' OR price_addition.PriceAddTemp IS NULL ) AND priceHistory=1 AND priceID!='0' AND CatHistory=1 AND PriceCatHistory=1".$this->SearchString().$this->SearchString_filtr()."
          ORDER BY category.CatNameTree, category.CatTree, priceDesc
          LIMIT ".$start.", " . $counter;*/

    $query = "SELECT  DISTINCT price.PricePrice, price.PriceEntry, price.PriceTork, price.priceID, price.priceVendor, price.priceDesc, price.pricePack, price.PriceSize, price.PriceDate, price.PriceOstat, category.CatDescription, category.CatTreeLevel, category.CatNameTree, category.CatTree, category.CatID, category.CatIndex, price_cat.PriceCatCatID, price_addition.priceAddPriceID, history_.HistValue, price.priceHistory, price_status.*
          FROM (price, category, price_cat) LEFT JOIN price_addition ON price_addition.priceAddPriceID=price.priceID LEFT JOIN history_ ON history_.HistPriceID=price.priceID AND history_.HistOrderID='".$HistOrderIDTemp."' LEFT JOIN price_status ON price_status.PriceStatID = price_addition.priceAddTemp  WHERE price.priceID=price_cat.PriceCatPriceID AND category.CatID=price_cat.PriceCatCatID AND priceHistory=1 AND priceID!='0' AND CatHistory=1 AND PriceCatHistory=1".$this->SearchString().$this->SearchString_filtr_add($filter)."
          ORDER BY category.CatNameTree, category.CatTree, priceDesc
          LIMIT ".$start.", " . $counter;

    /*$query_allprice = "SELECT  DISTINCT price.PricePrice, price.PriceEntry, price.PriceTork, price.priceID, price.priceVendor, price.priceDesc, price.pricePack, price.PriceSize, price.PriceDate, price.PriceOstat, category.CatDescription, price_cat.PriceCatCatID, price_addition.*, history_.HistValue, price.priceHistory,price_status.*
          FROM (price, category, price_cat) LEFT JOIN price_addition ON price_addition.priceAddPriceID=price.priceID LEFT JOIN history_ ON history_.HistPriceID=price.priceID AND history_.HistOrderID='".$HistOrderIDTemp."' LEFT JOIN price_status ON price_status.PriceStatID = price_addition.priceAddTemp  WHERE price.priceID=price_cat.PriceCatPriceID AND category.CatID=price_cat.PriceCatCatID  AND  ( price_addition.PriceAddTemp!='99990' OR price_addition.PriceAddTemp IS NULL ) AND priceHistory=1 AND priceID!='0' AND CatHistory=1 AND PriceCatHistory=1".$this->SearchString().$this->SearchString_filtr()."
          ORDER BY priceDesc
          LIMIT ".$start.", ".$counter;*/

    $query_allprice1 = "SELECT  DISTINCT price.PricePrice, price.PriceEntry, price.PriceTork, price.priceID, price.priceVendor, price.priceDesc, price.pricePack, price.PriceSize, price.PriceDate, price.PriceOstat, category.CatDescription, price_cat.PriceCatCatID, price_addition.*, history_.HistValue, price.priceHistory,price_status.*
          FROM (price, category, price_cat) LEFT JOIN price_addition ON price_addition.priceAddPriceID=price.priceID LEFT JOIN history_ ON history_.HistPriceID=price.priceID AND history_.HistOrderID='".$HistOrderIDTemp."' LEFT JOIN price_status ON price_status.PriceStatID = price_addition.priceAddTemp  WHERE price.priceID=price_cat.PriceCatPriceID AND category.CatID=price_cat.PriceCatCatID  AND priceHistory=1 AND priceID!='0' AND CatHistory=1 AND PriceCatHistory=1".$this->SearchString().$this->SearchString_filtr_add($filter)."
          ORDER BY priceDesc
          LIMIT ".$start.", ".$counter;

    $query_allprice = "SELECT DISTINCT v_price.*, 
          history_.HistValue, price_status.*, price_addition.priceAddPriceID FROM v_price
          LEFT JOIN price_addition ON price_addition.priceAddPriceID=priceID LEFT JOIN history_ ON history_.HistPriceID=priceID
          AND history_.HistOrderID='".$HistOrderIDTemp."' LEFT JOIN price_status ON price_status.PriceStatID = price_addition.priceAddTemp WHERE PriceID != '0' ".$this->SearchString_filtr_add($filter).$this->SearchString()." 
          LIMIT ".$start.", ".$counter; //.$this->SearchString_filtr($filter)

    $query_hot = "SELECT DISTINCT price.PricePrice, price.PriceEntry, price.PriceTork, price.priceID, price.priceVendor, price.priceDesc, price.pricePack, price.PriceSize, price.PriceDate, price.PriceOstat, category.CatDescription, price_cat.PriceCatCatID, price_addition.*, history_.HistValue, price.priceHistory, price_status.*
          FROM (price, category, price_cat, price_addition) LEFT JOIN history_ ON history_.HistPriceID=price.priceID AND history_.HistOrderID='".$HistOrderIDTemp."' INNER JOIN price_status ON price_status.PriceStatID = price_addition.priceAddTemp WHERE price_addition.priceAddPriceID=price.priceID AND price.priceID=price_cat.PriceCatPriceID AND category.CatID=price_cat.PriceCatCatID AND priceHistory=1 AND CatHistory=1 AND PriceCatHistory=1 AND priceID!='0' AND".$this->SearchString().$this->SearchString_filtr_add($filter)."
          ORDER BY price_status.PriceStatID"."".", priceDesc";

    $query_dyn = "SELECT * from category where CatTreeLevel=1 AND CatHistory=1 ORDER BY CatDescription";
//
//   echo $query . ".............";
//    echo "<br>".$query_allprice . ".............<br>";
//    echo $query_hot;
//    exit;


    $ostlist = new OstList;
    $ostlist->LoadFromDatabase();
    $ostcount = $ostlist->GetTotalCount();

      $product = new Product;

    if ($_SESSION['CatType'] != 1 || $filter == '1') {
      if ($_SESSION['CatType'] == 0 || $_SESSION['CatType'] == 1) {      
        $this->LoadFromSQL($query_allprice);
      }
      if ($_SESSION['CatType'] == 2) {      
        $this->LoadFromSQL($query_allprice1);
      }
      // echo(count($this->items)." aaaaaaaaaaaa");
        $priceID__ = '0';


      for ($i=count($this->items);$i>=0;$i--) {
        if (isset($this->items[$i]))
        {
            $this->items[$i]['PricePrice'] = $product->GetUserPrice(array("vip" => $this->items[$i]['PricePrice'],"entry" => $this->items[$i]['PriceEntry'],"tork" => $this->items[$i]['PriceTork'])); //Customer Discount
            if ( $this->items[$i]['PriceStatID'] == null ) { $this->items[$i]['PriceStatID'] = 0;  }
          /*if ($this->items[$i]['PriceCatCatID'] == $_SESSION['ozid1'] || $this->items[$i]['PriceCatCatID'] == $_SESSION['ozid2'] || $this->items[$i]['PriceCatCatID'] == $_SESSION['ozid3'])
          {
            $this->items[$i]['PriceCatCatID'] = null;
            $this->items[$i]['PriceOstat'] = "-";
          }
          for ($j=0;$j<$ostcount;$j++) {
            if (intval($this->items[$i]['PricePrice']) < intval($ostlist->items[$j]['ostPrice']))
            {
              if (intval($this->items[$i]['PriceOstat']) < intval($ostlist->items[$j]['ostValue']) || intval($this->items[$i]['PriceOstat']) > intval($ostlist->items[$j]['ostMaxValue'])) {
                $this->items[$i]['PriceOstat'] = "-";
              }
              break;
            }
          }*/
          //echo $this->items[$i]['priceID'] . "................" . $priceID__;
          /*if ($this->items[$i]['priceID'] == $priceID__) {
            unset($this->items[$i]);
          } else {
            $priceID__ = $this->items[$i]['priceID'];
          }*/
        }
      }


    } elseif (isset($_SESSION['searchProd']) && $_SESSION['searchProd'] != '') {
      $tempcat = "";
      //$templist = new LocalObjectList();
      //$templist->LoadFromSQL($query);
      $temparray = Array();
      $temparrayproduct = Array();
      $this->LoadFromSQL($query);
      $_SESSION['count_'] = count($this->items);
      $tempcat='';
      $tempcat1_temp='';

      $k = 0;
      $j = 1;
      $r_ = 1;

      for ($i=0;$i<count($this->items);$i++) {
        if (isset($this->items[$i]))
        {
            //$this->items[$i]['PricePrice'] = $product->GetUserPrice($this->items[$i]['PricePrice'],$this->items[$i]['PriceEntry']);
            $this->items[$i]['PricePrice'] = $product->GetUserPrice(array("vip" => $this->items[$i]['PricePrice'],"entry" => $this->items[$i]['PriceEntry'],"tork" => $this->items[$i]['PriceTork'])); //Customer Discount



            
            if ( $this->items[$i]['PriceStatID'] == null ) { $this->items[$i]['PriceStatID'] = 0;  }
          /*if ($this->items[$i]['PriceCatCatID'] == $_SESSION['ozid1'] || $this->items[$i]['PriceCatCatID'] == $_SESSION['ozid2'] || $this->items[$i]['PriceCatCatID'] == $_SESSION['ozid3'])
          {
            $this->items[$i]['PriceCatCatID'] = null;
            $this->items[$i]['PriceOstat'] = "-";
          }/*
          /*for ($j=0;$j<$ostcount;$j++) {
            if (intval($this->items[$i]['PricePrice']) < intval($ostlist->items[$j]['ostPrice']))
            {
              if (intval($this->items[$i]['PriceOstat']) < intval($ostlist->items[$j]['ostValue']) || intval($this->items[$i]['PriceOstat']) > intval($ostlist->items[$j]['ostMaxValue'])) {
                $this->items[$i]['PriceOstat'] = "-";
              }
              break;
            }
          }*/
          if ($this->items[$i]['CatNameTree'] != $this->items[$i]['CatTree'] && $tempcat1_temp != $this->items[$i]['CatTree'] ) {
            $tempcat1_temp = $this->items[$i]['CatTree'];
            $this->items[$i]['header_'] = $this->items[$i]['CatTree'];
          } else {
            $this->items[$i]['header_'] = '';
          }
        }
      /*}



      for ($i=0;$i<count($this->items);$i++) {*/
        //echo($this->items[$i]['CatTreeLevel']. " " . $this->items[$i]['CatDescription'] . "<br>");
        if (isset($this->items[$i]) && isset($this->items[$i]['CatNameTree']) && $this->items[$i]['CatNameTree'] != $tempcat)
        {
          if ($i != 0) {
            $temparray[$j]["productlistcat"] = $temparrayproduct;
            $j = $j + 1;
          } 

          if ($r_ == 0) {
            $temparray[$j]["first"] = 0;
          } else {
            $temparray[$j]["first"] = $r_;
          }
          $r_ = $r_ + 1;
          if (isset($_SESSION['searchProd']) && $_SESSION['searchProd'] != '') 
          {
             $temparray[$j]["searchExist"] = '0';
          } else {
             $temparray[$j]["searchExist"] = '1';
          }


          
          $temparray[$j]["CatDescription"] = $this->items[$i]['CatNameTree'];
          $temparray[$j]["CatID"] = $this->items[$i]['CatID'];
          $tempcat = $this->items[$i]['CatNameTree'];
          $temparrayproduct = Array();
          $k = 0;
        }
        if (isset($this->items[$i])) {
          $temparrayproduct[$k] = $this->items[$i];
          $k = $k + 1;
        }
      }
      $temparray[$j]["first"] = $r_;
      $temparray[$j]["productlistcat"] = $temparrayproduct;
      $this->LoadFromArray($temparray); 
      //$tempproducts = new LocalObjectList();
    } else {
      $this->LoadFromSQL($query_dyn);
      for ($i=0;$i<count($this->items);$i++) {
        if (isset($this->items[$i]))
        {
           $this->items[$i]['first'] = $i+1;
           $this->items[$i]['searchExist'] = '1';
        }
      }
    }
  }

    function LoadFromDataBaseForCustomerShort($catid_ = '0')
  {

    $start = 0;
    $counter = ITEMS_PER_PAGE;
    $page = 0;

    if (WHOLE_PRICE == 1) {
      $counter = 50000; //$this->GetTotalCountForCustomer();
    }
    if ($page > 0 && WHOLE_PRICE != 1)
    {
      $start = ($page - 1)*ITEMS_PER_PAGE;
    }
    $str = "";
    if (isset($_SESSION['searchProd'])) 
    {
		  $str = Connection::GetSQLLike($_SESSION['searchProd']);
    }
    $HistOrderIDTemp=0;
    if (isset($_SESSION['OrderID']) && $_SESSION['OrderID'])
    {
      $HistOrderIDTemp = $_SESSION['OrderID'];
    }

    $query = "SELECT  DISTINCT price.PricePrice, price.PriceEntry, price.PriceTork, price.priceID, price.priceVendor, price.priceDesc, price.pricePack, price.PriceSize, price.PriceDate, price.PriceOstat, category.CatDescription, category.CatTreeLevel, category.CatNameTree, category.CatTree, category.CatID, category.CatIndex, price_cat.PriceCatCatID, price_addition.priceAddPriceID, history_.HistValue, price.priceHistory, price_status.*
          FROM (price, category, price_cat) LEFT JOIN price_addition ON price_addition.priceAddPriceID=price.priceID LEFT JOIN history_ ON history_.HistPriceID=price.priceID AND history_.HistOrderID='".$HistOrderIDTemp."' LEFT JOIN price_status ON price_status.PriceStatID = price_addition.priceAddTemp  WHERE price.priceID=price_cat.PriceCatPriceID AND category.CatID=price_cat.PriceCatCatID AND priceHistory=1 AND priceID!='0' AND CatHistory=1 AND  PriceCatHistory=1 AND category.CatParentFirst='".$catid_."' ".$this->SearchString().$this->SearchString_filtr()."
          ORDER BY category.CatNameTree, category.CatTree, priceDesc
          LIMIT ".$start.", " . $counter;


//  echo "<br>".$query . ".............";
//    echo "<br>".$query_allprice . ".............<br>";
//    echo $query_hot;
//    exit;


      $product = new Product;


      $tempcat = "";
      //$templist = new LocalObjectList();
      //$templist->LoadFromSQL($query);
      $temparray = Array();
      $temparrayproduct = Array();
      $this->LoadFromSQL($query);
      $tempcat='';
      $tempcat1_temp='';

      $k = 0;
      $j = 1;
      $r_ = 1;

      for ($i=0;$i<count($this->items);$i++) {
        if (isset($this->items[$i]))
        {
            //$this->items[$i]['PricePrice'] = $product->GetUserPrice($this->items[$i]['PricePrice'],$this->items[$i]['PriceEntry']);
            $this->items[$i]['PricePrice'] = $product->GetUserPrice(array("vip" => $this->items[$i]['PricePrice'],"entry" => $this->items[$i]['PriceEntry'],"tork" => $this->items[$i]['PriceTork'])); //Customer Discount



            
            if ( $this->items[$i]['PriceStatID'] == null ) { $this->items[$i]['PriceStatID'] = 0;  }
          if ($this->items[$i]['CatNameTree'] != $this->items[$i]['CatTree'] && $tempcat1_temp != $this->items[$i]['CatTree'] ) {
            $tempcat1_temp = $this->items[$i]['CatTree'];
            $this->items[$i]['header_'] = $this->items[$i]['CatTree'];
          } else {
            $this->items[$i]['header_'] = '';
          }
        }
      }

  }

  function LoadFromDataBaseForCustomerHot1($page = 1)
  {

    /* Products will be soon */
    $HistOrderIDTemp=0;
    if (isset($_SESSION['OrderID']) && $_SESSION['OrderID'])
    {
      $HistOrderIDTemp = $_SESSION['OrderID'];
    }
    $_SESSION['ozid1'] = 0;
    $_SESSION['ozid2'] = 0;
    $_SESSION['ozid3'] = 0;
    $query = "SELECT * FROM category WHERE CatDescription LIKE '%�������%' AND CatHistory=1";
		$conn = GetConnection();
    $stmt = $conn->CreateStatement();
    $rs = $stmt->Execute($query);
    if ($stmt->GetNumRows())
  	{
      if ($row = $rs->NextRow())
      {
        $_SESSION['ozid1'] = $row['CatID'];
      }
      if ($row = $rs->NextRow())
      {
        $_SESSION['ozid2'] = $row['CatID'];
      }
      if ($row = $rs->NextRow())
      {
        $_SESSION['ozid3'] = $row['CatID'];
      }
    }
    /*------------------*/

    $query_hot = "SELECT price.*, category.CatDescription, price_cat.PriceCatCatID, price_addition.*, history_.HistValue, price_status.*
          FROM (price, category, price_cat, price_addition) LEFT JOIN history_ ON history_.HistPriceID=price.priceID AND history_.HistOrderID='".$HistOrderIDTemp."' INNER JOIN price_status ON price_status.PriceStatID = price_addition.priceAddTemp WHERE price_addition.priceAddPriceID=price.priceID AND price.priceID=price_cat.PriceCatPriceID AND category.CatID=price_cat.PriceCatCatID AND priceHistory=1 AND CatHistory=1 AND PriceCatHistory=1 
          ORDER BY price_status.PriceStatID, priceDesc";


    $ostlist = new OstList;
    $ostlist->LoadFromDatabase();
    $ostcount = $ostlist->GetTotalCount();
    $product = new Product;

      $this->LoadFromSQL($query_hot);
      for ($i=0;$i<$this->GetTotalCount();$i++) {
        if (isset($this->items[$i]))
        {
            //$this->items[$i]['PricePrice'] = $product->GetUserPrice($this->items[$i]['PricePrice'],$this->items[$i]['PriceEntry']);
            $this->items[$i]['PricePrice'] = $product->GetUserPrice(array("vip" => $this->items[$i]['PricePrice'],"entry" => $this->items[$i]['PriceEntry'],"tork" => $this->items[$i]['PriceTork'])); //Customer Discount
          if ($this->items[$i]['PriceCatCatID'] == $_SESSION['ozid1'] || $this->items[$i]['PriceCatCatID'] == $_SESSION['ozid2'] || $this->items[$i]['PriceCatCatID'] == $_SESSION['ozid3'])
          {
            $this->items[$i]['PriceCatCatID'] = null;
            $this->items[$i]['PriceOstat'] = "-";
          }
          for ($j=0;$j<$ostcount;$j++) {
            if (intval($this->items[$i]['PricePrice']) < intval($ostlist->items[$j]['ostPrice']))
            {
              if (intval($this->items[$i]['PriceOstat']) < intval($ostlist->items[$j]['ostValue']) || intval($this->items[$i]['PriceOstat']) > intval($ostlist->items[$j]['ostMaxValue'])) {
                $this->items[$i]['PriceOstat'] = "-";
              }
              break;
            }
          }
        }
      }
  }

  function LoadFromDataBaseForCustomerHotWeek($basket=0,$notin="")
  {
    $andnotin = "";
    if (isset($notin) && $notin) {
      $andnotin = implode(',', $notin);
      $andnotin = " AND priceID NOT IN (" . $andnotin . "  )";
    }
    /* Products will be soon */
    $HistOrderIDTemp=0;
    if (isset($_SESSION['OrderID']) && $_SESSION['OrderID'])
    {
      $HistOrderIDTemp = $_SESSION['OrderID'];
    }
    /*$_SESSION['ozid1'] = 0;
    $_SESSION['ozid2'] = 0;
    $_SESSION['ozid3'] = 0;
    $query = "SELECT * FROM category WHERE CatDescription LIKE '%�������%' AND CatHistory=1";
		$conn = GetConnection();
    $stmt = $conn->CreateStatement();
    $rs = $stmt->Execute($query);
    if ($stmt->GetNumRows())
  	{
      if ($row = $rs->NextRow())
      {
        $_SESSION['ozid1'] = $row['CatID'];
      }
      if ($row = $rs->NextRow())
      {
        $_SESSION['ozid2'] = $row['CatID'];
      }
      if ($row = $rs->NextRow())
      {
        $_SESSION['ozid3'] = $row['CatID'];
      }
    }*/
    //echo $basket . " aaaaaaaaaaaaa";
    $notforbasket = $this->SearchString().$this->SearchString_filtr();
    if ( $basket ) { $notforbasket = ""; }
    /*------------------*/
    //echo $notforbasket . " aaaaaaaaaaaaa";

    $query_hot = "SELECT DISTINCT price.*, category.CatDescription, price_cat.PriceCatCatID, price_addition.*, history_.HistValue
          FROM (price, category, price_cat, price_addition) LEFT JOIN history_ ON history_.HistPriceID=price.priceID AND history_.HistOrderID='".$HistOrderIDTemp."' WHERE price_addition.priceAddPriceID=price.priceID AND price.priceID=price_cat.PriceCatPriceID AND category.CatID=price_cat.PriceCatCatID AND priceHistory=1 AND CatHistory=1 AND PriceCatHistory=1 AND price_addition.PriceAddTemp='9999' ".$notforbasket." ". $andnotin . "
          ORDER BY priceDesc";

          //echo $query_hot;
          //exit;


    $ostlist = new OstList;
    /*$ostlist->LoadFromDatabase();
    $ostcount = $ostlist->GetTotalCount();*/
    $product = new Product; 

      $this->LoadFromSQL($query_hot);
      for ($i=0;$i<count($this->items);$i++) {
        if (isset($this->items[$i]))
        {
            //$this->items[$i]['PricePrice'] = $product->GetUserPrice($this->items[$i]['PricePrice'],$this->items[$i]['PriceEntry']);
            $this->items[$i]['PricePrice'] = $product->GetUserPrice(array("vip" => $this->items[$i]['PricePrice'],"entry" => $this->items[$i]['PriceEntry'],"tork" => $this->items[$i]['PriceTork'])); //Customer Discount
          /*if ($this->items[$i]['PriceCatCatID'] == $_SESSION['ozid1'] || $this->items[$i]['PriceCatCatID'] == $_SESSION['ozid2'] || $this->items[$i]['PriceCatCatID'] == $_SESSION['ozid3'])
          {
            $this->items[$i]['PriceCatCatID'] = null;
            $this->items[$i]['PriceOstat'] = "-";
          }
          for ($j=0;$j<$ostcount;$j++) {
            if (intval($this->items[$i]['PricePrice']) < intval($ostlist->items[$j]['ostPrice']))
            {
              if (intval($this->items[$i]['PriceOstat']) < intval($ostlist->items[$j]['ostValue']) || intval($this->items[$i]['PriceOstat']) > intval($ostlist->items[$j]['ostMaxValue'])) {
                $this->items[$i]['PriceOstat'] = "-";
              }
              break;
            }
          }*/
        }
      }
  }

  function GetTotalCount()
  {
    $query = "SELECT count(*) AS total FROM price WHERE priceHistory=1".$this->SearchString();
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }

  function GetTotalCountForCustomer()
  {
    $query = "SELECT count(*) AS total FROM price, category, price_cat WHERE price.priceID=price_cat.PriceCatPriceID AND category.CatID=price_cat.PriceCatCatID AND priceHistory=1 AND CatHistory=1 AND PriceCatHistory=1".$this->SearchString(); //.$this->SearchString_filtr();
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }
   function SearchString(){
        if (!empty($_SESSION['searchProd']))
        {
            //return " AND MATCH (priceDesc, priceVendor) AGAINST ('" . Connection::GetSQLToSearch($_SESSION['searchProd']). "' IN BOOLEAN MODE)";
            $tempsearch = "";
            $temparr = explode(" ", mysql_escape_string($_SESSION['searchProd']));
            for ( $i=0; $i<count($temparr);$i++) {
              $tempsearch = $tempsearch . " AND ( priceDesc LIKE '" . Connection::GetSQLLike($temparr[$i]). "%'  OR priceVendor LIKE '" . Connection::GetSQLLike($temparr[$i]). "%' OR priceDesc LIKE '% " . Connection::GetSQLLike($temparr[$i]). "%'  OR priceVendor LIKE '% " . Connection::GetSQLLike($temparr[$i]). "%' OR priceAddDesc LIKE '%" . Connection::GetSQLLike($temparr[$i]). "%')";

              //$tempsearch = $tempsearch . " AND ( priceDesc LIKE '%" . $temparr[$i]. "%'  OR priceVendor LIKE '%" . $temparr[$i]. "%')";
            }
            return $tempsearch;
        }else{
            return "";
        }
    }

   function SearchString_filtr(){
     $str1="";
     $str2="";
     $str3="";
     $str="";

       if ($_SESSION['userCheck1'])
       {
         $str1="(PriceOstat!='0' AND PriceOstat!='' AND PriceOstat!='-' AND PriceOstat IS NOT NULL)";  
       } else {
         //$str1="(PriceOstat='0' OR PriceOstat='' OR PriceOstat='-' OR PriceOstat IS NULL)";       
       }
       if ($_SESSION['userCheck2'])
       {
         $str2="((PriceOstat='0' OR PriceOstat='' OR PriceOstat='-' OR PriceOstat IS NULL) AND (PriceDate!='' AND PriceDate!=' ' AND PriceDate!='-' AND PriceDate IS NOT NULL))";
       } 
       if ($_SESSION['userCheck3'])
       {
         $str3="((PriceOstat='0' OR PriceOstat='' OR PriceOstat='-' OR PriceOstat IS NULL) AND (PriceDate='' OR PriceDate=' ' OR PriceDate='-' OR PriceDate IS NULL))";
       } 
     if ($str2!="") { 
        if ($str1!="") { 
          $str1 = $str1 . " OR " . $str2; 
        } else {
          $str1 = $str2; 
        }
     }
     if ($str3!="") { 
        if ($str1!="") { 
          $str1 = $str1 . " OR " . $str3; 
        } else {
          $str1 = $str3; 
        }
     }
     if ($str1!="") { 
       $str1 = " AND (" . $str1 . ")"; 
     } else { 
       $str1=" AND PriceID='12345'"; 
     }
     return $str1;


    }

   function SearchString_filtr_add($filter){
     $str1="";
     $str2="";
     $str3="";
     $str="";
     if ($filter == '0') {
       if ($_SESSION['userCheck1'])
       {
         $str1="(PriceOstat!='0' AND PriceOstat!='' AND PriceOstat!='-' AND PriceOstat IS NOT NULL)";  
       } else {
         //$str1="(PriceOstat='0' OR PriceOstat='' OR PriceOstat='-' OR PriceOstat IS NULL)";       
       }
       if ($_SESSION['userCheck2'])
       {
         $str2="((PriceOstat='0' OR PriceOstat='' OR PriceOstat='-' OR PriceOstat IS NULL) AND (PriceDate!='' AND PriceDate!=' ' AND PriceDate!='-' AND PriceDate IS NOT NULL))";
       } 
       if ($_SESSION['userCheck3'])
       {
         $str3="((PriceOstat='0' OR PriceOstat='' OR PriceOstat='-' OR PriceOstat IS NULL) AND (PriceDate='' OR PriceDate=' ' OR PriceDate='-' OR PriceDate IS NULL))";
       } 
     } else {
       if ($_SESSION['userCheck1'])
       {
       } else {
         $str1="(PriceOstat!='0' AND PriceOstat!='' AND PriceOstat!='-' AND PriceOstat IS NOT NULL)";       
       }
       if ($_SESSION['userCheck2'])
       {
       } else {
         $str2="((PriceOstat='0' OR PriceOstat='' OR PriceOstat='-' OR PriceOstat IS NULL) AND (PriceDate!='' AND PriceDate!=' ' AND PriceDate!='-' AND PriceDate IS NOT NULL))";
       } 
       if ($_SESSION['userCheck3'])
       {
       } else {
         $str3="((PriceOstat='0' OR PriceOstat='' OR PriceOstat='-' OR PriceOstat IS NULL) AND (PriceDate='' OR PriceDate=' ' OR PriceDate='-' OR PriceDate IS NULL))";
       }
     }
     if ($str2!="") { 
        if ($str1!="") { 
          $str1 = $str1 . " OR " . $str2; 
        } else {
          $str1 = $str2; 
        }
     }
     if ($str3!="") { 
        if ($str1!="") { 
          $str1 = $str1 . " OR " . $str3; 
        } else {
          $str1 = $str3; 
        }
     }
     if ($str1!="") { 
       $str1 = " AND (" . $str1 . ")"; 
     } else { 
       $str1=" AND PriceID='12345'"; 
     }
     //echo $str1 . "<br>";
     return $str1;


    }

}
?>