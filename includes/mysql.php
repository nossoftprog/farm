<?php

class Mysql {
  // variables
  var $DbHandler;
  var $Stmt;
  var $Error;
  var $Result;

  // Connect to MySQL database
  function Mysql() {
    $this->DbHandler = mysql_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD);
    if (!$this->DbHandler) {
      echo "database connection error...";
      exit;
    }
    if (!mysql_select_db(DB_NAME)) {
      echo "can't select database...";
      exit;
    }
  }

  // Close connection to Mysql
  function closeConnection() {
    if ($this->DbHandler) {
      mysql_close($this->DbHandler);
    }
  }

  // Execute query
  function executeQuery(&$Query) {
    $this->Result = array();
    if ($this->DbHandler) {
      $this->Stmt = mysql_query($Query, $this->DbHandler);
      $this->Error = mysql_error();
      if (empty($this->Error)) {
        return true;
      } else {
        if (DEBUG == 1) {
          $QueryArr = explode("\n", $Query);
          echo "<b>Can not execute query:</b><br>";
          for ($i = 1; $i <= count($QueryArr); $i++) {
            echo "Line ".$i.": ".str_replace("\r", "", $QueryArr[$i-1])."<br>";
          }
          echo "<b>".$this->Error."</b>";
          exit;
        }
      }
    }
    return false;
  }

  // Fetch result
  function fetchResult() {
    if ($this->Stmt) {
      $NumRows = mysql_num_rows($this->Stmt);
      $this->Result = array();
      if ($NumRows > 0) {
        for ($i = 0; $i < $NumRows; $i++) {
          $this->Result[] = mysql_fetch_array($this->Stmt, MYSQL_ASSOC);
        }
        mysql_free_result($this->Stmt);
        return true;
      }
    }
    return false;
  }
}

?>