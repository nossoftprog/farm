<?php
require_once("template.php");

class PageFooter extends Template
{

	// Constructor
	function PageFooter($user = "", $page = "")
	{
    $request = new LocalObject($_REQUEST);
		if ($user)
		{
			$this->Template("_a_footer.html");
			$this->SetVar("ImagesDir", HTTP_ADMIN_IMAGES);
			$this->SetVar("SiteDir", HTTP_URL);
		}
		else
		{
			$this->Template("_footer1".$request->GetProperty('lang').".html");
			$this->SetVar("ImagesDir", HTTP_URL_IMAGES);
			$this->SetVar("SiteDir", HTTP_URL);


      $vacancylist = new VacancyList();
      $vacancylist->LoadFromDataBase(1, 1);
      $this->LoadFromObjectsList("vacancylistright", $vacancylist);

			if ($page == "main")
			{
				$this->setVar("Main", 1);
			}
		}
	}

}
?>