<?
class Users {

	var $LevelName = array(
		"0" => "�������������",
		"1" => "��������",
		"2" => "������������",
		"3" => "��������",
	);


	function logout() {
		authenticate::logout();
		header("Location: ".$_SERVER['HTTP_REFERER']."");
		exit;
	}	// end logout()
	

	function login($error="") {
		$page = new Template(THEME_DIR);
		$page->set_file(array(
				"login" => "users_login.html",
		));
		
		$page->set_var(array(
				"THEMEPATH"		=> THEME_URL,
				"FORM_ERROR"		=> $error,
				"FORM_BEGIN"		=> '<form action="users.php" method="post">',
				"FORM_NAME"		=> '<input type="text" maxlength="15" name="name" value="'.authenticate::getName().'" style="width: 80px" class="LoginForm">',
				"FORM_PASSWORD"		=> '<input type="password" maxlength="20" name="password" style="width: 80px" class="LoginForm">',
				"FORM_SUBMIT"		=> '<input type="submit" value="�����" style="width: 50px" class="LoginButton">',
				"FORM_END"		=> '<input type="hidden" name="t" value="check"><input type="hidden" name="url" value="'.$_SERVER['HTTP_REFERER'].'"></form>',
		));
		
		$page->parse("OUT", "login");
		
		return $page->get("OUT");
	}	//end login()


	function register($name="", $email="", $lastname="", $firstname="", $middlename="", $company="", $address="", $phone="", $error="") {
		if (ALLOW_REGISTER) {
			$page = new Template(THEME_DIR);
			$page->set_file(array(
					"register" => "users_register.html",
			));
	
			
			$page->set_var(array(
					"THEMEPATH"			=> THEME_URL,
					"FORM_ERROR"		=> $error,
					"FORM_BEGIN"		=> '<form name="registration" action="users.php" method="post">',
					"FORM_EMAIL"		=> '<input type="text" name="email" value="'.$email.'" style="width: 100px" class="registerForm">',
					"FORM_FIRSTNAME"	=> '<input type="text" name="firstname" value="'.$firstname.'" style="width: 100px" class="registerForm">',
					"FORM_MIDDLENAME"	=> '<input type="text" name="middlename" value="'.$middlename.'" style="width: 100px" class="registerForm">',
					"FORM_LASTNAME"		=> '<input type="text" name="lastname" value="'.$lastname.'" style="width: 100px" class="registerForm">',
					"FORM_COMPANY"		=> '<input type="text" name="company" value="'.$company.'" style="width: 100px" class="registerForm">',
					"FORM_ADDRESS"		=> '<input type="text" name="address" value="'.$address.'" style="width: 100px" class="registerForm">',
					"FORM_PHONE"		=> '<input type="text" name="phone" value="'.$phone.'" style="width: 100px" class="registerForm">',
					"FORM_NAME"			=> '<input type="text" maxlength="15" name="name" value="'.$name.'" style="width: 100px" class="registerForm">',
					"FORM_PASSWORD1"	=> '<input type="password" maxlength="20" name="password1" style="width: 100px" class="registerForm">',
					"FORM_PASSWORD2"	=> '<input type="password" maxlength="20" name="password2" style="width: 100px" class="registerForm">',
					"FORM_SUBMIT"		=> '<input type="submit" value="������" style="width: 100px" onClick="javascript:validate_register()" class="registerButton">',
					"FORM_END"			=> '<input type="hidden" name="t" value="send"></form>',
			));
			
			$page->parse("OUT", "register");
			
			return $page->get("OUT");
		} else {
			$page = new Template(THEME_DIR);
			$page->set_file(array(
					"register" => "users_noregister.html",
			));
			
			$page->set_var(array(
					"THEMEPATH"		=> THEME_URL,
					"ADMIN_MAIL"		=> '<a href="mailto:'.ADMIN_MAIL.'">'.ADMIN_MAIL.'</a>',
			));
			
			$page->parse("OUT", "register");
			
			return $page->get("OUT");		
		}
	}	// end register()
	
	
	function send($email, $name, $lastname, $firstname, $middlename, $company, $address, $phone, $password1, $password2) {
		if (ALLOW_REGISTER) {
			if ($password1!=$password2) {
				$error = "������ �� ���������";
				return $this->register($name, $email, $lastname, $firstname, $middlename, $company, $address, $phone, $error);
				exit;
			}
			if (strlen($password1)<6 or strlen($password1)>20) {
				$error = "������ �� ����� ���� ������ 6-� �������� � ������� 20-��!". " " .$password1;
				return $this->register($name, $email, $lastname, $firstname, $middlename, $company, $address, $phone, $error);
				exit;
			}
			if ($lastname=="") {
				$error = "��������� ������� !";
				return $this->register($name, $email, $lastname, $firstname, $middlename, $company, $address, $phone, $error);
				exit;
			}
			if ($firstname=="") {
				$error = "��������� ��� !";
				return $this->register($name, $email, $lastname, $firstname, $middlename, $company, $address, $phone, $error);
				exit;
			}
			if ($company=="") {
				$error = "��������� ����������� !";
				return $this->register($name, $email, $lastname, $firstname, $middlename, $company, $address, $phone, $error);
				exit;
			}
			if ($address=="") {
				$error = "��������� ����� !";
				return $this->register($name, $email, $lastname, $firstname, $middlename, $company, $address, $phone, $error);
				exit;
			}
			if ($phone=="") {
				$error = "��������� ������� !";
				return $this->register($name, $email, $lastname, $firstname, $middlename, $company, $address, $phone, $error);
				exit;
			}
			$u = new Authenticate();
			if (!$u->createUser($name,$password1,$email, $lastname, $firstname, $middlename, $company, $address, $phone)) {
				$message  = '������ ��� �����������! ������������ � ����� ������ ��� ����������, ����������� ������ ���<br><br>'; 
				$message .= '<a href="javascript:history.back(-1)">��������� �����</a>';
			} else {
				if (!NEWUSER_CONFIRM) {
					$message  = '����� ������������ ��������������� ����� ������ ������� �� �����������, �� �������� ����� ��������� ���� ����� ������� ������ � �������������� �����������.<br><br>';
					$message .= '<a href="users.php?t=login">��������� �� �������� �����</a>';
				} else {
					$message  = '����������� ���������. ��� ��������� ������, �� �������� �����, ��������� ����, ����� ������� ������. �������� ����������� � ������ ��� ������������� ����� �����������<br><br>'; 
					$message .= '<a href="users.php?t=login">��������� �� �������� �����</a>';
				}
			}
			
			$page = new Template(THEME_DIR);
			$page->set_file(array(
					"send" => "users_send.html",
			));
			
			$page->set_var(array(
					"THEMEPATH"		=> THEME_URL,
					"MESSAGE"		=> $message,
			));
			
			$page->parse("OUT", "send");
			
			return $page->get("OUT");
		 } else {
			$page = new Template(THEME_DIR);
			$page->set_file(array(
					"register" => "users_noregister.html",
			));
			
			$page->set_var(array(
					"THEMEPATH"		=> THEME_URL,
					"ADMIN_MAIL"		=> '<a href="mailto:'.ADMIN_MAIL.'">'.ADMIN_MAIL.'</a>',
			));
			
			$page->parse("OUT", "register");
			
			return $page->get("OUT");		
		}
	} // end send()
	
	
	function check($name, $password, $url) {
		$u = new Authenticate();
		if ($u->Login($name, $password)) {
			if (preg_match("/users.php/i", $url)) {
			header("Location: ./");
			} else {
			//header("Location: ".$url."");
			header("Location: catalog.php");
			//header("Location: freepage.php?content=uc");
			}
			exit;
		}
		return $this->login("������������ ��������� ����� � ������.");
	} // end check()
	
	
	function confirm($email, $id) {
		$q = new DB_Sql("select confirm from authorize where email='".$email."' and id='".$id."'");
		if ($q->num_rows()!=0) {
			$q->next_record();
			if (!NEWUSER_CONFIRM and $q->f("confirm")) {
				$u = new Authenticate();
				$u->validateUser($email,$id);
				$message = '���� ����������� ������������ ���������������...<br> ��������� �� <a href="/users.php?t=login">�������� �����';
			}
			elseif (!NEWUSER_CONFIRM and !$q->f("confirm")) {
				$message = '���� ����������� ���� �� ������������ ���������������...';
			}
			else {
				$u = new Authenticate();
				$u->validateUser($email,$id);
				$message = '���� ����������� ������������...<br> ��������� �� <a href="/users.php?t=login">�������� �����';
			}
		} else {
			$message = '���� ������ �� ����������� ���������� ��� ���������� ���������������...';
		}
		$q->free();
		return $message;
	} // end confirm()
	
	
	function profile($message="") {
			$page = new Template(THEME_DIR);
			$page->set_file(array(
					"profile" => "users_profile.html",
			));
			
			$u = new Authenticate();
//			return "Logged as <b>".$u->getName()."</b> and has access level is <b>".$u->getLevel()."</b>";

			$names = $u->getFullName();
			
			$page->set_var(array(
					"THEMEPATH"			=> THEME_URL,
					"FORM_MESSAGE"		=> $message,
					"USERNAME"			=> $u->getName(),
					"ACCESSLEVEL" 		=> $this->LevelName[$u->getLevel()],
					"LAST_ACCESS" 		=> formatDate($u->getLastAccess()),
					"FORM_BEGIN"		=> '<form name="registration" action="users.php" method="post"  onsubmit="validate_profile()" >',
					"FORM_EMAIL"		=> '<input type="text" name="email" value="'.$u->getEmail().'" style="width: 150px" class="profileForm">&nbsp;&nbsp;<input type="submit" name="submit" value="�������� ������" style="width: 150px" class="profileButton">',
					"FORM_LASTNAME"		=> '<input type="text" name="lastname" value="'.$names["lastname"].'" style="width: 150px" class="profileForm" title="�������">',
					"FORM_FIRSTNAME"	=> '<input type="text" name="firstname" value="'.$names["firstname"].'" style="width: 150px" class="profileForm" title="���">',
					"FORM_MIDDLENAME"	=> '<input type="text" name="middlename" value="'.$names["middlename"].'" style="width: 150px" class="profileForm" title="��������">',
					"FORM_COMPANY"		=> '<input type="text" name="company" value="'.$u->getCompany().'" style="width: 150px" class="profileForm">',
					"FORM_ADDRESS"		=> '<input type="text" name="address" value="'.$u->getAddress().'" style="width: 150px" class="profileForm">',
					"FORM_PHONE"		=> '<input type="text" name="phone" value="'.$u->getPhone().'" style="width: 150px" class="profileForm">',
					"FORM_PASSWORD"		=> '<input type="password" maxlength="20" name="password" style="width: 150px" class="profileForm">&nbsp;&nbsp;<input type="submit" name="submit" value="������� ������" style="width: 150px" class="profileButton">',
					"FORM_END"			=> '<input type="hidden" name="t" value="change_profile"></form>',
			));
			
			$page->parse("OUT", "profile");
			
			return $page->get("OUT");

	} // end profile()
	
	function statistic() {
			//$page->set_block("statistic", "STATISTIC", "statistic");
			//$q = new DB_Sql("select * from history where user_id='".$id."' order by date");
			$page = new Template(THEME_DIR);
            $page->set_file(array(
                    "history" => "statistic.html",
            ));
			$u = new Authenticate();
			$current_user_id = $u->getID();
			//$names = $u->getFullName();
			//$q = new DB_Sql("select * from history where user_id='".$current_user_id."' order by date AND time DESC");
			//user delete history > 30 day's from current date
			$w = new DB_Sql("delete from history where user_id='".$current_user_id."' and TO_DAYS(NOW()) - TO_DAYS(date) > 30");
			$w->free();
			//show last 30 day's
			$q = new DB_Sql("select * from history where user_id='".$current_user_id."' and TO_DAYS(NOW()) - TO_DAYS(date) <= 30
 order by date DESC, time ASC");
			if ($q->num_rows()!=0) {
				//$q = new DB_Sql("select * from history where user_id=1 order by date");
				$page->set_var(array(
					"THEMEPATH"			=> THEME_URL,
					"PRICELIST_DATE"	=> $current_date,
				));
				$page->set_block("history", "HISTORY_ITEMS1", "items");
				$page->set_block("history", "HISTORY_ITEMS2", "items");
				$page->set_block("history", "HISTORY_ITEMS3", "items");
				$page->set_block("history", "HISTORY", "statistic");
				$row_bgcolor="statisticTableRowBgColor1";
				$current_date="";
				$current_time="";
				while($q->next_record()){
					if ($q->f("date")!=$current_date) {
						$page->set_var(array(
						"STAT_DATE"	=> formatDate($q->f("date")),
						));
						$page->parse("statistic", "HISTORY_ITEMS1", true);
						$current_date = $q->f("date");
						$row_bgcolor="statisticTableRowBgColor2";
					}
					if ($q->f("time")!=$current_time) {
						$page->set_var(array(
						"STAT_TIME"	=> $q->f("time"),
						));
					$page->parse("statistic", "HISTORY_ITEMS2", true);
					$current_time = $q->f("time");
					$row_bgcolor="statisticTableRowBgColor2";
					}
					$page->set_var(array(
						"USER"				=> $u->getName(),
						"USER_NAME"			=> join(' ', $u->getFullName()),
						"COMPANY_NAME"		=> $u->getCompany(),
						"COMPANY_ADDRESS"	=> $u->getAddress(),
						"COMPANY_PHONE"		=> $u->getPhone(),
						"COMPANY_MAIL"		=> $u->getEmail(),
						"STAT_PRODUCT"		=> $q->f("product"),
						"STAT_PRICE_ONE"	=> $q->f("price_one"),
						"STAT_PRICE_SUM"	=> $q->f("price_sum"),
						"STAT_NUM"		=> $q->f("num_items"),
						"STAT_COMMENT"		=> $q->f("comment"),
						"ROW_BGCOLOR"		=> $row_bgcolor,
					));
					if ($row_bgcolor=="statisticTableRowBgColor1") {
						$row_bgcolor="statisticTableRowBgColor2";
					} else {
						$row_bgcolor="statisticTableRowBgColor1";
					}
					$page->parse("statistic", "HISTORY_ITEMS3", true);
				} // while
				// compiling page
				$page->parse("OUT", "history");
				// return page
				return $page->get("OUT");
				$q->free();
				return $out;
			} else {
				$q->free();
				return "&nbsp;&nbsp;&nbsp;&nbsp;�� ������� ������������ ��� ����������";
			}	
	} // end statistic()

	function print_statistic() {
		$page = new Template(THEME_DIR);
            $page->set_file(array(
                    "history" => "print_statistic.html",
            ));
			$u = new Authenticate();
			$current_user_id = $u->getID();
			//$names = $u->getFullName();
			//$q = new DB_Sql("select * from history where user_id='".$current_user_id."' order by date AND time DESC");
			//user delete history > 30 day's from current date
			$w = new DB_Sql("delete from history where user_id='".$current_user_id."' and TO_DAYS(NOW()) - TO_DAYS(date) > 30");
			$w->free();
			//show last 30 day's
			$q = new DB_Sql("select * from history where user_id='".$current_user_id."' and TO_DAYS(NOW()) - TO_DAYS(date) <= 30
 order by date DESC, time ASC");
			if ($q->num_rows()!=0) {
				//$q = new DB_Sql("select * from history where user_id=1 order by date");
				$page->set_var(array(
					"THEMEPATH"			=> THEME_URL,
					"PRICELIST_DATE"	=> $current_date,
				));
				$page->set_block("history", "HISTORY_ITEMS1", "items");
				$page->set_block("history", "HISTORY_ITEMS2", "items");
				$page->set_block("history", "HISTORY_ITEMS3", "items");
				$page->set_block("history", "HISTORY", "statistic");
				$row_bgcolor="statisticTableRowBgColor1";
				$current_date="";
				$current_time="";
				while($q->next_record()){
					if ($q->f("date")!=$current_date) {
						$page->set_var(array(
						"STAT_DATE"	=> formatDate($q->f("date")),
						));
						$page->parse("statistic", "HISTORY_ITEMS1", true);
						$current_date = $q->f("date");
						$row_bgcolor="statisticTableRowBgColor2";
					}
					if ($q->f("time")!=$current_time) {
						$page->set_var(array(
						"STAT_TIME"	=> $q->f("time"),
						));
					$page->parse("statistic", "HISTORY_ITEMS2", true);
					$current_time = $q->f("time");
					$row_bgcolor="statisticTableRowBgColor2";
					}
					$page->set_var(array(
						"USER"				=> $u->getName(),
						"USER_NAME"			=> join(' ', $u->getFullName()),
						"COMPANY_NAME"		=> $u->getCompany(),
						"COMPANY_ADDRESS"	=> $u->getAddress(),
						"COMPANY_PHONE"		=> $u->getPhone(),
						"COMPANY_MAIL"		=> $u->getEmail(),
						"STAT_PRODUCT"		=> $q->f("product"),
						"STAT_PRICE_ONE"	=> $q->f("price_one"),
						"STAT_PRICE_SUM"	=> $q->f("price_sum"),
						"STAT_NUM"		=> $q->f("num_items"),
						"STAT_COMMENT"		=> $q->f("comment"),
						"ROW_BGCOLOR"		=> $row_bgcolor,
					));
					if ($row_bgcolor=="statisticTableRowBgColor1") {
						$row_bgcolor="statisticTableRowBgColor2";
					} else {
						$row_bgcolor="statisticTableRowBgColor1";
					}
					$page->parse("statistic", "HISTORY_ITEMS3", true);
				} // while
				// compiling page
				$page->parse("OUT", "history");
				// return page
				return $page->get("OUT");
				$q->free();
				return $out;
			} else {
				$q->free();
				return "&nbsp;&nbsp;&nbsp;&nbsp;�� ������� ���짮��⥫� ��� ���ଠ樨";
			}	
	} // end print_statistic()
	
	function manager_client() {
			$u = new Authenticate();
			//$manager = getID();
			$page = new Template(THEME_DIR);
            $page->set_file(array(
                    "manager" => "manager_client.html",
            ));
			$q = new DB_Sql("select user_id, lastname from users where manager='".$u->getID()."' order by lastname ASC");
			if ($q->num_rows()!=0) {
				$page->set_var(array(
					"THEMEPATH"			=> THEME_URL,
					"MAN_NAME"			=> join(' ', $u->getFullName()),
					"USER"				=>$u->getName(),
				));
				$page->set_block("manager", "CLIENT_ITEMS", "items");
				$page->set_block("manager", "CLIENT", "clients");
				while($q->next_record()){
					$page->set_var(array(
						"CLIENT_ID" 		=> $q->f("user_id"),
						"CLIENT_NAME" 		=> $q->f("lastname"),	
						));
					$page->parse("clients", "CLIENT_ITEMS", true);
				}
				$page->parse("OUT", "manager");
				// return page
				return $page->get("OUT");
				$q->free();
				return $out;
			}
			$q->free();
			return "&nbsp;&nbsp;&nbsp;&nbsp;�� ������� ������������ ��� ����������";
	}
	function manager_statistic($client) {
			//echo $client;
			//$page->set_block("statistic", "STATISTIC", "statistic");
			//$q = new DB_Sql("select * from history where user_id='".$id."' order by date");
			$page = new Template(THEME_DIR);
            $page->set_file(array(
                    "history" => "statistic.html",
            ));
			$u = new Authenticate();
			//$q = new DB_Sql("select * from history where user_id='".$client."' order by date DESC, time ASC");
			$q = new DB_Sql("select history.product, history.price_one, history.price_sum, history.num_items, history.date, history.time, users.username, users.company, users.firstname, users.middlename, users.lastname, users.address, users.phone, users.email from history, users where users.user_id=history.user_id and history.user_id='".$client."' order by date DESC, time ASC");
			//$t = new DB_Sql("select * from users where user_id='".$client."'");
			//echo $t->num_rows();
			//echo $t->f(user_id);
			//$t->free();
			if ($q->num_rows()!=0) {
				//$q = new DB_Sql("select * from history where user_id=1 order by date");
				$page->set_var(array(
					"THEMEPATH"			=> THEME_URL,
					"PRICELIST_DATE"	=> $current_date,
				));
				$page->set_block("history", "HISTORY_ITEMS1", "items");
				$page->set_block("history", "HISTORY_ITEMS2", "items");
				$page->set_block("history", "HISTORY_ITEMS3", "items");
				$page->set_block("history", "HISTORY", "statistic");
				$row_bgcolor="statisticTableRowBgColor1";
				$current_date="";
				$current_time="";
				while($q->next_record()){
					if ($q->f("date")!=$current_date) {
						$page->set_var(array(
						"STAT_DATE"	=> formatDate($q->f("date")),
						));
						$page->parse("statistic", "HISTORY_ITEMS1", true);
						$current_date = $q->f("date");
						$row_bgcolor="statisticTableRowBgColor2";
					}
					if ($q->f("time")!=$current_time) {
						$page->set_var(array(
						"STAT_TIME"	=> $q->f("time"),
						));
					$page->parse("statistic", "HISTORY_ITEMS2", true);
					$current_time = $q->f("time");
					$row_bgcolor="statisticTableRowBgColor2";
					}
					$page->set_var(array(
						//"USER"				=> $u->getName(),
						//"USER_NAME"			=> join(' ', $u->getFullName()),
						//"COMPANY_NAME"		=> $u->getCompany(),
						//"COMPANY_ADDRESS"	=> $u->getAddress(),
						//"COMPANY_PHONE"		=> $u->getPhone(),
						//"COMPANY_MAIL"		=> $u->getEmail(),
						"USER"				=> $q->f("username"),
						"USER_NAME"			=> $q->f("lastname").' '.$q->f("firstname").' '.$q->f("middlename"),
						"COMPANY_NAME"		=> $q->f("company"),
						"STAT_COMMENT"		=> $q->f("comment"),
						"COMPANY_ADDRESS"	=> $q->f("address"),
						"COMPANY_PHONE"		=> $q->f("phone"),
						"COMPANY_MAIL"		=> $q->f("email"),
						"STAT_PRODUCT"		=> $q->f("product"),
						"STAT_PRICE_ONE"	=> $q->f("price_one"),
						"STAT_PRICE_SUM"	=> $q->f("price_sum"),
						"STAT_NUM"		=> $q->f("num_items"),
						"ROW_BGCOLOR"		=> $row_bgcolor,
					));
					if ($row_bgcolor=="statisticTableRowBgColor1") {
						$row_bgcolor="statisticTableRowBgColor2";
					} else {
						$row_bgcolor="statisticTableRowBgColor1";
					}
					$page->parse("statistic", "HISTORY_ITEMS3", true);
				} // while
				// compiling page
				$page->parse("OUT", "history");
				// return page
				return $page->get("OUT");
				$q->free();
				return $out;
			} else {
				$q->free();
				return "&nbsp;&nbsp;&nbsp;&nbsp;�� ������� ������������ ��� ����������";
			}	
	} // end manager_statistic()

	function select_client() {
			$u = new Authenticate();
			//$manager = getID();
			$page = new Template(THEME_DIR);
            $page->set_file(array(
                    "manager" => "select_client.html",
            ));
			$q = new DB_Sql("select user_id, lastname from users where manager='".$u->getID()."' order by lastname ASC");
			if ($q->num_rows()!=0) {
				$page->set_var(array(
					"THEMEPATH"			=> THEME_URL,
					"MAN_NAME"			=> join(' ', $u->getFullName()),
					"USER"				=>$u->getName(),
				));
				$page->set_block("manager", "CLIENT_ITEMS", "items");
				$page->set_block("manager", "CLIENT", "clients");
				while($q->next_record()){
					$page->set_var(array(
						"CLIENT_ID" 		=> $q->f("user_id"),
						"CLIENT_NAME" 		=> $q->f("lastname"),	
						));
					$page->parse("clients", "CLIENT_ITEMS", true);
				}
				$page->parse("OUT", "manager");
				// return page
				return $page->get("OUT");
				$q->free();
				return $out;
			}
			$q->free();
			return "&nbsp;&nbsp;&nbsp;&nbsp;����������� ���������� �� ��������";
	} //End select_client
	
	function client_editor($client, $message="") {
			$page = new Template(THEME_DIR);
            $page->set_file(array(
                    "editor" => "client_editor.html",
            ));
			$u = new Authenticate();
			$q = new DB_Sql("select users.username, users.company, users.firstname, users.middlename, users.lastname, users.address, users.phone, users.email, users.price_procent, users.last_access from users where users.user_id='".$client."'");
			if ($q->num_rows()!=0) {
				$page->set_var(array(
					"THEMEPATH"			=> THEME_URL,
				));
				while($q->next_record()){
					$page->set_var(array(
						"FORM_MESSAGE"			=> $message,
						"LAST_CLIENT_ACCESS" 	=> formatDate($q->f("last_access")),
						"CLIENTNAME"			=> $q->f("username"),
						"FORM_LASTNAME"			=> $q->f("lastname"),
						"FORM_FIRSTNAME"		=> $q->f("firstname"),
						"FORM_MIDDLENAME"		=> $q->f("middlename"),
						"FORM_COMPANY"			=> $q->f("company"),
						"FORM_ADDRESS"			=> $q->f("address"),
						"FORM_PHONE"			=> $q->f("phone"),
						"FORM_EMAIL"			=> $q->f("email"),
						"FORM_CLIENT"			=> $client,
						"FORM_PROCENT"			=> '<input type="text" name="price_procent" value="'.$q->f("price_procent").'" style="width: 40px" class="profileForm">',
					));
				} // while
				// compiling page
				$page->parse("OUT", "editor");
				// return page
				return $page->get("OUT");
				$q->free();
				return $out;
			} else {
				$q->free();
				return "&nbsp;&nbsp;&nbsp;&nbsp;������! $client, $price_procent";
			}	
	} // end client_editor()	
	
	function changeClientProcent($client, $price_procent) {
		//echo $client;
		$price_procent=str_replace(",", ".", $price_procent);
		$z = new DB_Sql("select * from users where user_id='".$client."'");
		if ($z->num_rows()==1) {
			$w = new DB_Sql("update users set price_procent='".$price_procent."' where user_id='".$client."'");
			$w->free();
			$message.="������� ��� ������� �������!<br>";
			return $this->client_editor($client, $message);
			//return 1;
		} else {
			return 0;
		}
		$z->free();
		return $this->client_editor($client, $message);
	}
	
	function not_auth() {
			$page = new Template(THEME_DIR);
			$page->set_file(array(
					"notauth" => "users_notauth.html",
			));
			
			$page->set_var(array(
					"THEMEPATH"		=> THEME_URL,
					"ADMIN_MAIL"	=> '<a href="mailto:'.ADMIN_MAIL.'">'.ADMIN_MAIL.'</a>',
			));
			
			$page->parse("OUT", "notauth");
			
			return $page->get("OUT");		
	} // end not_auth()
	
	function changeProfile($email, $name, $lastname, $firstname, $middlename, $company, $address, $phone, $pword, $submit) {
		$u = new Authenticate();
		if ($submit=="�������� ������") {
			if ($u->changeFullName($u->getID(), $lastname, $firstname, $middlename)) {
//				$message.="�.�.�. ���� ������� ��������<br>";
			} else {
				$message.="������ ��� ��������� �.�.�.<br>";
			}
			if ($u->changeCompany($u->getID(), $company)) {
//				$message.="�������� �������� ���� ������� ��������<br>";
			} else {
				$message.="������ ��� ��������� �������� ��������<br>";
			}
			if ($u->changeAddress($u->getID(), $address)) {
//				$message.="����� ��� ������� �������<br>";
			} else {
				$message.="������ ��� ��������� ������<br>";
			}
			if ($u->changePhone($u->getID(), $phone)) {
//				$message.="������� ��� ������� �������<br>";
			} else {
				$message.="������ ��� ��������� ��������<br>";
			}
			if ($u->changeEmail($u->getID(), $email)) {
//				$message.="E-mail ��� ������� �������<br>";
			} else {        
				$message.="������ ��� ��������� E-mail<br>";
			}
		} elseif ($submit=="������� ������") {
			if ($u->setPassword($u->getID(), $pword)) {
				$message="������ ��� ������� ����������";
			} else {
				$message="������ ��� ��������� ������";
			}
		} else {
			$message="������������ ������, ������� �� ���-�� �� �� ������";
		}
		return $this->profile($message);
	} //end changeProfile()
	
}  //end class users

?>