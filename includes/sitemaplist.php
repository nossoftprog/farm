<?php
require_once("localobjectlist.php");
require_once("translation.php");
require_once("sitemaptranslist.php");
require_once("sitemap.php");

class SitemapList extends LocalObjectList
{
	var $message = "";

	function GetAllPages($selected, $pageID = "")
	{
		$where = "";
		if ($pageID)
		{
			$where = " WHERE PageID<>".intval($pageID);
		}
		$rows = array();
		$trans = new Translation();
		$query = "SELECT *, IF(PageID = '".mysql_escape_string($selected)."', 1, 0) AS selected
  				FROM sitemap
  				".$where."
  				ORDER BY SortOrder";
		$stmt = GetStatement();
		$rs = $stmt->Execute($query);
		$i = 0;
		if ($stmt->GetNumRows())
		{
			while ($row = $rs->NextRow())
			{
				$acTrans = new SitemapTransList($row["PageID"], $trans->language);
				$acTrans->LoadFromDataBase();
				$it = $acTrans->GetItemsArray();
				$rows[$i] = array_merge($row, isset($it[0]) ? $it[0] : array());
				$i++;
			}
		}
		$this->LoadFromArray($rows);
	}

	function GetTopLevelPages()
	{
		$lang = Translation::GetCurrentLanguage();
		$query = "SELECT *
  				FROM sitemap
  				WHERE ParentID = 0
  				ORDER BY SortOrder";
		$stmt = GetStatement();
		$rs = $stmt->Execute($query);
		$i = 0;
		$rows = array();
		if ($stmt->GetNumRows())
		{
			while ($row = $rs->NextRow())
			{
				$acTrans = new SitemapTransList($row["PageID"], $lang);
				$acTrans->LoadFromDataBase();
				$it = $acTrans->GetItemsArray();
				$rows[$i] = array_merge($row, isset($it[0]) ? $it[0] : array());
				$i++;
			}
		}
		//pre_print_r($rows);
		$this->LoadFromArray($rows);
	}

	function GetSitemapTreeFck()
	{
		$rows = array();
		$lang = Translation::GetCurrentLanguage();
		$query = "SELECT *
  				FROM sitemap
  				WHERE ParentID = 0
  				ORDER BY SortOrder";
		$stmt = GetStatement();
		$rs = $stmt->Execute($query);
		$i = 0;
		$html = '';
		if ($stmt->GetNumRows())
		{
			while ($row = $rs->NextRow())
			{
				$html .= '<tr><td>'.Sitemap::GetPageHTMLFck($row["PageID"], $lang).'</td></tr>';
			}
		}
		$html = '<table border="0" width="100%">'.$html.'</table>';
		return $html;
	}

	function GetSitemapTree()
	{
		$rows = array();
		$trans = new Translation();
		$query = "SELECT *
  				FROM sitemap
  				WHERE ParentID = 0
  				ORDER BY SortOrder";
		$stmt = GetStatement();
		$rs = $stmt->Execute($query);
		$i = 0;
		$html = '';
		if ($stmt->GetNumRows())
		{
			while ($row = $rs->NextRow())
			{
				$html .= '<tr><td>'.Sitemap::GetPageHTML($row["PageID"], $trans->language).'</td></tr>';
			}
		}
		//echo '<pre>'.htmlspecialchars($html).'</pre>';
		return $html;
	}

	function Delete($pageID)
	{
		if ($pageID)
		{
			$stmt = GetStatement();

			$query = "DELETE FROM sitemap WHERE PageID=".intval($pageID);
			$stmt->Execute($query);

			$query = "DELETE FROM sitemap_trans WHERE PageID=".intval($pageID);
			$stmt->Execute($query);

			$query = "SELECT PageID FROM sitemap WHERE ParentID=".intval($pageID);
			$rows = $stmt->FetchList($query);
			if (!$rows)
			{
				return;
			}
			else
			{
				foreach ($rows as $k => $v)
				{
					SitemapList::Delete($v["PageID"]);
				}
			}
		}
	}

	function MovePage($pageID, $updown)
	{
		$sitemap = new Sitemap();
		$sitemap->SetProperty('PageID', $pageID);
		$sitemap->LoadFromDataBase();
		$row = null;
		if (is_numeric($updown) && is_numeric($pageID))
		{
			$stmt = GetStatement();
			if ($updown == 1)
			{
				$query = "SELECT * FROM sitemap
						WHERE SortOrder >= ".intval($sitemap->GetProperty('SortOrder'))."
						AND PageID<>".intval($pageID)."
						AND ParentID=".intval($sitemap->GetProperty('ParentID'))."
						ORDER BY SortOrder ASC
						LIMIT 1";
				$row = $stmt->FetchRow($query);
			}
			elseif ($updown == -1)
			{
				$query = "SELECT * FROM sitemap
						WHERE SortOrder <= ".intval($sitemap->GetProperty('SortOrder'))."
						AND PageID<>".intval($pageID)."
						AND ParentID=".intval($sitemap->GetProperty('ParentID'))."
						ORDER BY SortOrder DESC
						LIMIT 1";
				$row = $stmt->FetchRow($query);
			}
			if ($row)
			{
				$query = "UPDATE sitemap SET
					SortOrder=".intval($row['SortOrder'])."
					WHERE PageID=".intval($pageID);
				$stmt->Execute($query);

				$query = "UPDATE sitemap SET
					SortOrder=".intval($sitemap->GetProperty('SortOrder'))."
					WHERE PageID=".intval($row['PageID']);
				$stmt->Execute($query);
			}
		}
	}

}
?>