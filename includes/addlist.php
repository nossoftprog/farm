<?php
require_once("localobjectlist.php");
require_once("functions.php");
require_once("addf.php");


class AddList extends LocalObjectList
{
  var $message = "";
  function LoadFromDataBase($page = 1)
  {
    $start = 0;
    if ($page > 0)
    {
      $start = ($page - 1)*ITEMS_PER_PAGE;
    }
    $rows = array();
    global $corporate;
    $query = "SELECT *
          FROM addfields
          ORDER BY addName
          LIMIT ".$start.", ".ITEMS_PER_PAGE;
    $this->LoadFromSQL($query);
    for ($i=0;$i<$this->GetTotalCount();$i++) {
      if (isset($this->items[$i]) && isset($this->items[$i]['AddDescr']))
      {
        $this->items[$i]["Preview"] = CreatePreview($this->items[$i]['AddDescr']);
      }
    }
  }

  function LoadAddList($selected = 0)
  {
    $rows = array();
    $query = "SELECT * FROM addfields
          ORDER BY addName";
    $this->LoadFromSQL($query);
    for ($i=0;$i<$this->GetTotalCount();$i++) {
      if (isset($this->items[$i]) && isset($this->items[$i]['AddDescr']))
      {
        $this->items[$i]["Preview"] = CreatePreview($this->items[$i]['AddDescr']);
      }
    }
  }



  function GetTotalCount()
  {
    global $corporate;
    $query = "SELECT count(*) AS total FROM addfields";
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }

  function Delete($data)
  {
    if (is_array($data) && $data)
    {
      $ids = implode(",", Connection::GetSQLArray($data));
      if ($ids)
      {
        $query = "DELETE FROM addfields WHERE addID IN (".$ids.")";
        $stmt = GetStatement();
        $stmt->Execute($query);

      }
    }
  }

}
?>