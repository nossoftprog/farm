<?php
require_once("localobjectlist.php");
require_once("functions.php");

class OstList extends LocalObjectList
{
  var $message = "";
  function LoadFromDataBase($page = 1)
  {
    $start = 0;
    if ($page > 0)
    {
      $start = ($page - 1)*ITEMS_PER_PAGE;
    }
    $rows = array();
    $query = "SELECT *
          FROM ostat
          ORDER BY ostPrice";
    $this->LoadFromSQL($query);

        $counter = $this->GetTotalCount();

        for ($i=0;$i<$counter;$i++) {
        if (isset($this->items[$i]))
        {
          if ($i == 0)
          {
            $this->items[$i]['ostPrice1'] = "0";
          } else {
            $this->items[$i]['ostPrice1'] = $this->items[$i-1]['ostPrice'];
          }
        }
        }

  }

  function LoadOstList($selected = 0)
  {
    $rows = array();
    $query = "SELECT * FROM ostat
          ORDER BY ostPrice";
    $this->LoadFromSQL($query);
  }

  function GetTotalCount()
  {
    global $corporate;
    $query = "SELECT count(*) AS total FROM ostat";
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }

  function Delete($data)
  {
    if (is_array($data) && $data)
    {
      $ids = implode(",", Connection::GetSQLArray($data));
      if ($ids)
      {
        $query = "DELETE FROM ostat WHERE ostID IN (".$ids.")";
        $stmt = GetStatement();
        $stmt->Execute($query);

      }
    }
  }

}
?>