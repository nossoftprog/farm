<?php
require_once("localobject.php");
require_once("localobjectlist.php");


class Companies extends LocalObjectList
{
  function	LoadFromDataBaseCompany($CoflexID)
	{
		if ($CoflexID)
		{
		//�������� �� ������� �������
	      $query = "SELECT count(*) as total FROM users_address WHERE UserCoflexID='".$CoflexID."'";
			$stmt = GetStatement();
			$n = $stmt->FetchField($query, "total");
			if ($n)
			{
				$query = "SELECT *
						FROM users_address
						WHERE UserCoflexID = '".$CoflexID . "'";
				$this->LoadFromSQL($query);
			}else{
				$query = "SELECT UserCompany as CompanyName, UserAddress as Address
						FROM users_
						WHERE UserCoflexID = '".$CoflexID . "'";
				$this->LoadFromSQL($query);
			}
//			echo $query;
		}
	}
} 

class Company extends LocalObject
{
  function	LoadFromDataBase($CoflexID)
	{
		if ($CoflexID)
		{
		//�������� �� ������� �������
	      $query = "SELECT count(*) as total FROM users_address WHERE UserCoflexID='".$CoflexID."'";
			$stmt = GetStatement();
			$n = $stmt->FetchField($query, "total");
			if ($n)
			{
				$query = "SELECT *
						FROM users_address
						WHERE UserCoflexID = '".$CoflexID . "'";
				$this->LoadFromSQL($query);
			}else{
				$query = "SELECT UserCompany as CompanyName, UserAddress as Address
						FROM users_
						WHERE UserCoflexID = '".$CoflexID . "'";
				$this->LoadFromSQL($query);
			}

		}
	}
} 



class User extends LocalObject
{
	function LoadFromDataBase()
	{
		if ($this->GetProperty('UserID'))
		{
			$query = "SELECT *
					FROM users_
					WHERE UserID = ".$this->GetPropertyForSQL('UserID');
			$this->LoadFromSQL($query);
		}
	}

  function LoadFromDataBaseForEmail()
	{
		if ($this->GetProperty('UserID'))
		{
//			$query = "SELECT *
//					FROM users_ LEFT JOIN admins ON AdminID=UserAdminID
//					WHERE UserID = ".$this->GetPropertyForSQL('UserID');
			$query = "SELECT *
					FROM users_ LEFT JOIN admins ON (AdminCoflexID=UserAdminID) 
					WHERE UserID = ".$this->GetPropertyForSQL('UserID');

			$this->LoadFromSQL($query);
		}
	}


  function LoadFromDataBaseForXML()
	{
		if ($this->GetProperty('UserID'))
		{

			if ($this->GetProperty('AddressCoflexID'))
			{
			   $str = " LEFT JOIN users_address ON AddressCoflexID=".$this->GetPropertyForSQL('AddressCoflexID');
			}elseif($this->GetProperty('CompanyCoflexID')){
			   $str = " LEFT JOIN users_address ON CompanyCoflexID=".$this->GetPropertyForSQL('CompanyCoflexID');
			}


			$query = "SELECT *
					FROM users_ LEFT JOIN admins ON (AdminCoflexID=UserAdminID)".$str."
					WHERE UserID = ".$this->GetPropertyForSQL('UserID');
			$this->LoadFromSQL($query);
		}

	}


	function CheckInformation()
	{
		if ($this->GetProperty('UserEmail') == null)
		{
			return "������� ����� E-mail!";
		}
//		elseif (strlen($this->GetProperty('UserINN')) < 10)
		elseif ((strlen($this->GetProperty('UserINN')) != 10) && (strlen($this->GetProperty('UserINN')) != 12))
		{
			return "��� ����� ��������� ������ 10 ��� 12 ����!";
		}
		elseif ($this->GetProperty('UserCompany') == null)
		{
			return "������� �������� ��������!";
		}
/*		elseif ($this->LoginExists())
		{
			return "����� ������������ ��� ����������";
		}
		elseif (!$this->GetProperty('UserID') && !$this->GetProperty('UserPassword'))
		{
			return "������� ������";
		}
		elseif (strlen($this->GetProperty('UserPassword')) !== 0 && (strlen($this->GetProperty('UserPassword')) < 6 ||  strlen($this->GetProperty('UserPassword')) > 20))
		{
			return "������ �� ����� ���� ������ 6-� �������� � ������� 20-��.";
		}
		elseif (!is_null($this->GetProperty('UserPassword')) && $this->GetProperty('UserPassword') !== $this->GetProperty('Password1'))
		{
			return "������ �� ���������";
		}*/
		return null;
	}

	function LoginExists()
	{
		/* $query = "SELECT count(*) as total FROM users_
				WHERE UserName=".$this->GetPropertyForSQL('UserName')."
				AND UserPassword='" . crypt($this->GetProperty('UserPassword'),'a552avf1ss') . "' AND UserID<>".$this->GetPropertyForSQL('UserID'); */
    $query = "SELECT count(*) as total FROM users_ WHERE UserName=".$this->GetPropertyForSQL('UserName')." AND UserID<>".$this->GetPropertyForSQL('UserID');
		$stmt = GetStatement();
		$n = $stmt->FetchField($query, "total");
		if ($n)
		{
			return true;
		}
		return false;
	}


	function Update()
	{
		$str = "UserName=".$this->GetPropertyForSQL('UserName');
		if ($this->GetProperty('CoflexID') != 0)
		{
			$str .= ", CoflexID=".$this->GetPropertyForSQL('CoflexID');
		}
		$str .= ", UserEmail=" . $this->GetPropertyForSQL('UserEmail') . ", UserFirstName=" . $this->GetPropertyForSQL('UserFirstName') . ", UserMiddleName=" . $this->GetPropertyForSQL('UserMiddleName') . ", UserLastName=" . $this->GetPropertyForSQL('UserLastName') . ", UserCompany=" . $this->GetPropertyForSQL('UserCompany') . ", UserAddress=" . $this->GetPropertyForSQL('UserAddress') . ", UserPhone=" . $this->GetPropertyForSQL('UserPhone') . ", UserPricePercent=0";

		if ($this->GetProperty('UserPassword'))
		{
			$str .= ", UserPassword='" . crypt($this->GetProperty('UserPassword'),'a552avf1ss') . "'";
		}
		if ($this->GetProperty('UserID'))
		{
			$query = "UPDATE users_ SET ".$str." WHERE UserID=".$this->GetPropertyForSQL('UserID');
		}
		else
		{
			$query = "INSERT INTO users_ SET ".$str. ", UserDate=now()";
		
		}
		$stmt = GetStatement();
		$stmt->Execute($query);
	}

	function UpdateAdmin()
	{
		$str = "UserName=".$this->GetPropertyForSQL('UserName');
		if ($this->GetProperty('CoflexID') != 0)
		{
			$str .= ", CoflexID=".$this->GetPropertyForSQL('CoflexID');
		}
		$str .= ", UserEmail=" . $this->GetPropertyForSQL('UserEmail') . ", UserFirstName=" . $this->GetPropertyForSQL('UserFirstName') . ", UserMiddleName=" . $this->GetPropertyForSQL('UserMiddleName') . ", UserLastName=" . $this->GetPropertyForSQL('UserLastName') . ", UserCompany=" . $this->GetPropertyForSQL('UserCompany') . ", UserAddress=" . $this->GetPropertyForSQL('UserAddress') . ", UserPhone=" . $this->GetPropertyForSQL('UserPhone') . ", OrderUpload=" . $this->GetPropertyForSQL('OrderUpload') . ", UserPricePercent=0, UserAdminID=" . $this->GetPropertyForSQL('UserAdminID');

		if ($this->GetProperty('UserPassword'))
		{
			$str .= ", UserPassword='" . crypt($this->GetProperty('UserPassword'),'a552avf1ss') . "'";
		}
		if ($this->GetProperty('UserID'))
		{
			$query = "UPDATE users_ SET ".$str." WHERE UserID=".$this->GetPropertyForSQL('UserID');
		}
		else
		{
			$query = "INSERT INTO users_ SET ".$str. ", UserDate=now()";
		}
		$stmt = GetStatement();
		$stmt->Execute($query);
	}

	function UpdateCheck()
	{
		$query = "UPDATE users_ SET UserCheck1=".$this->GetPropertyForSQL('UserCheck1').", UserCheck2=".$this->GetPropertyForSQL('UserCheck2').", UserCheck3=".$this->GetPropertyForSQL('UserCheck3')."  WHERE UserID=".$this->GetPropertyForSQL('UserID');
		$stmt = GetStatement();
		$stmt->Execute($query);
	}


}


?>