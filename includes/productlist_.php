<?php
require_once("localobjectlist.php");
require_once("functions.php");
require_once("product.php");
require_once("ostlist.php");


class ProductList extends LocalObjectList
{
  var $message = "";

  function LoadFromDataBase($page = 1)
  {
    $start = 0;
    if ($page > 0)
    {
      $start = ($page - 1)*ITEMS_PER_PAGE;
    }
    $str = "";
    if (isset($_SESSION['searchProd'])) 
    {
		  $str = Connection::GetSQLLike($_SESSION['searchProd']);
    }
    $query = "SELECT *
          FROM price WHERE priceHistory=1 AND priceDesc LIKE '%" . $str . "%'
          ORDER BY priceDesc
          LIMIT ".$start.", ".ITEMS_PER_PAGE;
    $this->LoadFromSQL($query);
  }

  function LoadFromDataBaseForCustomer($page = 1)
  {
    $start = 0;
    $counter = ITEMS_PER_PAGE;
    if (WHOLE_PRICE == 1) {
      $counter = $this->GetTotalCountForCustomer();
      //echo($counter);
    }
    if ($page > 0 && WHOLE_PRICE != 1)
    {
      $start = ($page - 1)*ITEMS_PER_PAGE;
    }
    $str = "";
    if (isset($_SESSION['searchProd'])) 
    {
		  $str = Connection::GetSQLLike($_SESSION['searchProd']);
    }
    $query = "SELECT price.*, category.CatDescription, price_addition.*
          FROM ( price, category, price_cat ) LEFT JOIN price_addition ON price_addition.priceAddPriceID=price.priceID WHERE price.priceID=price_cat.PriceCatPriceID AND category.CatID=price_cat.PriceCatCatID AND category.CatID IN ". $_SESSION['CatIN']." AND priceHistory=1 AND priceDesc LIKE '%" . $str . "%'
          ORDER BY CatID".$_SESSION['CatDESC'].", priceDesc
          LIMIT ".$start.", ".$counter;

    $query_allprice = "SELECT price.*, category.CatDescription, price_addition.*
          FROM ( price, category, price_cat ) LEFT JOIN price_addition ON price_addition.priceAddPriceID=price.priceID WHERE price.priceID=price_cat.PriceCatPriceID AND category.CatID=price_cat.PriceCatCatID AND category.CatID IN ". $_SESSION['CatIN']." AND priceHistory=1 AND priceDesc LIKE '%" . $str . "%'
          ORDER BY priceDesc
          LIMIT ".$start.", ".$counter;

    $ostlist = new OstList;
    $ostlist->LoadFromDatabase();
    $ostcount = $ostlist->GetTotalCount();

    if ($_SESSION['CatType'] == 0) {

      $this->LoadFromSQL($query_allprice);
      for ($i=0;$i<$counter;$i++) {
        if (isset($this->items[$i]))
        {
          for ($j=0;$j<$ostcount;$j++) {
            if (intval($this->items[$i]['PricePrice']) < intval($ostlist->items[$j]['ostPrice']))
            {
              if (intval($this->items[$i]['PriceOstat']) < intval($ostlist->items[$j]['ostValue']) || intval($this->items[$i]['PriceOstat']) > intval($ostlist->items[$j]['ostMaxValue'])) {
                $this->items[$i]['PriceOstat'] = "-";
              }
              break;
            }
          }
        }
      }

    } else {
      $tempcat = "";
      $templist = new LocalObjectList();
      $templist->LoadFromSQL($query);
      $temparray = Array();
      $temparrayproduct = Array();
      $this->LoadFromSQL($query);

      for ($i=0;$i<$counter;$i++) {
        if (isset($this->items[$i]))
        {
          for ($j=0;$j<$ostcount;$j++) {
            if (intval($this->items[$i]['PricePrice']) < intval($ostlist->items[$j]['ostPrice']))
            {
              if (intval($this->items[$i]['PriceOstat']) < intval($ostlist->items[$j]['ostValue']) || intval($this->items[$i]['PriceOstat']) > intval($ostlist->items[$j]['ostMaxValue'])) {
                $this->items[$i]['PriceOstat'] = "-";
              }
              break;
            }
          }
        }
      }

      $k = 0;
      $j = 1;

      for ($i=0;$i<$counter;$i++) {
        if (isset($this->items[$i]) && isset($this->items[$i]['CatDescription']) && $this->items[$i]['CatDescription'] != $tempcat)
        {
          if ($i != 0) {
            $temparray[$j]["productlistcat"] = $temparrayproduct;
            $j = $j + 1;
          }
          
          $temparray[$j]["CatDescription"] = $this->items[$i]['CatDescription'];
          $tempcat = $this->items[$i]['CatDescription'];
          $temparrayproduct = Array();
          $k = 0;
        }
        if (isset($this->items[$i])) {
          $temparrayproduct[$k] = $this->items[$i];
          $k = $k + 1;
        }
      }
      $temparray[$j]["productlistcat"] = $temparrayproduct;
      $this->LoadFromArray($temparray); 

      //$tempproducts = new LocalObjectList();
    }
  }

  function GetTotalCount()
  {
    $str = "";
    if (isset($_SESSION['searchProd'])) 
    {
		  $str = Connection::GetSQLLike($_SESSION['searchProd']);
    }
    $query = "SELECT count(*) AS total FROM price WHERE priceHistory=1 AND priceDesc LIKE '%" . $str . "%'";
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }

  function GetTotalCountForCustomer()
  {
    $str = "";
    if (isset($_SESSION['searchProd'])) 
    {
		  $str = Connection::GetSQLLike($_SESSION['searchProd']);
    }
    $query = "SELECT count(*) AS total           FROM price, category, price_cat WHERE price.priceID=price_cat.PriceCatPriceID AND category.CatID=price_cat.PriceCatCatID AND category.CatID IN ". $_SESSION['CatIN']." AND priceHistory=1 AND priceDesc LIKE '%" . $str . "%'";
    //echo($query);
    //exit;
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }

}
?>