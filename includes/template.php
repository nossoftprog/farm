<?php
require_once("vlibtemplate.php");

class Template extends vlibTemplate //Cache
{
	// Members
	var $filename;

	// Constructor
	function Template($file, $options = null)
	{
		//$this->vlibTemplateCache();
		$this->vlibTemplate($file, $options);
		$this->LoadFromArray($_REQUEST);
		$this->filename = $file;
	}

	// LoadFromArray
	function LoadFromArray($data)
	{
		if (is_array($data))
		{
			foreach ($data as $k => $v)
			{
				$this->setVar($k, $v);
			}
		}
	}

	// LoadFromArray with loop
	function LoadFromArrayWithLoop($data)
	{
		if (is_array($data))
		{
			foreach ($data as $k => $v)
			{
				if (is_array($v))
				{
					$this->SetLoop($k, $v);
				}
				else
				{
					$this->setVar($k, $v);
				}
			}
		}
	}

	// LoadFromArray with loops
	function LoadFromArrayWithLoops($data, $setVars = true)
	{
		if (is_array($data))
		{
			foreach ($data as $k => $v)
			{
				if (is_array($v))
				{
					$this->SetLoop($k, $v);
					$this->LoadFromArrayWithLoops($v, false);
				}
				elseif ($setVars)
				{
					$this->setVar($k, $v);
				}
			}
		}
	}

	// LoadFromObject
	function LoadFromObjectWithLoops($object)
	{
		$this->LoadFromArrayWithLoops($object->GetAllProperties());
	}

	// LoadFromObject
	function LoadFromObject($object)
	{
		$this->LoadFromArray($object->GetAllProperties());
	}

	// LoadFromObjectsList
	function LoadFromObjectsList($name, $objects)
	{
		$this->setLoop($name, $objects->GetItemsArray());
	}

	function LoadFromObjectListWithLoops($name, $objects)
	{
		$arr = $objects->GetItemsArray();
		$this->setLoop($name, $arr);
		foreach ($arr as $k => $v)
		{
			if (is_array($v))
			{
				$this->SetLoop($k, $v);	
			}
		}

	}

	// SetHasLoop
	function SetHasLoop($name, $objects)
	{
		$this->setVar($name, $objects->CountItems() > 0);
	}

	// SetFileName
	function SetFileName($filename)
	{
		return $this->filename = $filename;
	}

	// GetFileName
	function GetFileName()
	{
		return $this->filename;
	}

	function _arrayBuild ($arr)
	{
		if (is_array($arr) && !empty($arr))
		{
			$arr = array_values($arr); // to prevent problems w/ non sequential arrays

			for ($i = 0; $i < count($arr); $i++)
			{
				if(!is_array($arr[$i]))
				{
					if(is_object($arr[$i]))
					{
						$object = $arr[$i];
						unset($arr[$i]);
						$arr[$i] = $object->GetAllProperties();
					}
					else
					{
						return false;
					}
				}
				foreach ($arr[$i] as $k => $v)
				{
					unset($arr[$i][$k]);
					if ($this->OPTIONS['CASELESS']) $k = strtolower($k);
					if (preg_match('/^[0-9]+$/', $k)) $k = '_'.$k;


					if (is_array($v))
					{
						if (($arr[$i][$k] = $this->_arrayBuild($v)) === false) return false;
					}
					else
					{ // reinsert the var
					$arr[$i][$k] = $v;
					}
				}
				if ($this->OPTIONS['LOOP_CONTEXT_VARS'])
				{
					$_first = ($this->OPTIONS['CASELESS'])? '__first__' : '__FIRST__';
					$_last  = ($this->OPTIONS['CASELESS'])? '__last__' : '__LAST__';
					$_inner = ($this->OPTIONS['CASELESS'])? '__inner__' : '__INNER__';
					$_even  = ($this->OPTIONS['CASELESS'])? '__even__' : '__EVEN__';
					$_odd   = ($this->OPTIONS['CASELESS'])? '__odd__' : '__ODD__';
					$_rownum = ($this->OPTIONS['CASELESS'])? '__rownum__' : '__ROWNUM__';

					if ($i == 0) $arr[$i][$_first] = true;
					if (($i + 1) == count($arr)) $arr[$i][$_last] = true;
					if ($i != 0 && (($i + 1) < count($arr))) $arr[$i][$_inner] = true;
					if (is_int(($i+1) / 2))  $arr[$i][$_even] = true;
					if (!is_int(($i+1) / 2))  $arr[$i][$_odd] = true;
					$arr[$i][$_rownum] = ($i + 1);
				}
			}
			return $arr;
		}
		elseif (empty($arr))
		{
			return array();
		}
	}

}
?>