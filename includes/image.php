<?php
require_once("localobject.php");

class Image extends LocalObject
{
	var $error = null;
	var $filesDir;

	function Image($filesDir = '', $data = array())
	{
		if ($filesDir && is_dir($filesDir))
		{
			$this->filesDir = $filesDir;
		}
		else
		{
			$this->filesDir = FILES_DIR;
		}
		/* if CreateIconsMode == byHeight - cut image by height
		** if CreateIconsMode == byWidth - cut image by width
		** otherwise - cut to by both height & width */
		if (defined('CREATE_ICONS_MODE'))
		{
			$this->SetProperty('CreateIconsMode', CREATE_ICONS_MODE);
		}

		$this->LoadFromArray($data);
	}

	function CheckInformation($required = true)
	{
		if ($required)
		{
			if ($this->GetProperty('error') == 4)
			{
				return "Browse the file";
			}
			elseif ($this->GetProperty('size') == 0)
			{
				return "File is empty";
			}
		}
		if ($this->GetProperty('error') == 1 || $this->GetProperty('error') == 2)
		{
			return "The uploaded file exceeds the upload_max_filesize";
		}
		elseif ($this->GetProperty('error') == 3)
		{
			return "The uploaded file was only partially uploaded";
		}
		return null;
	}

	/************************************************************************
	$icons = array([0] => array("iconw" => ICON_WIDTH, "iconh" => ICON_HEIGHT, "pref" => "icon_"),
				   [1] => array(...))
	return created fileName or false;
	************************************************************************/
	function Add($filePref = '', $icons = array())
	{
		$isIconMaked = true;
		if ($this->GenerateName($filePref) && @move_uploaded_file($this->GetProperty('tmp_name'), JoinPath($this->filesDir, $this->GetProperty('name'))))
		{
			if ($icons && is_array($icons))
			{
				foreach ($icons as $row)
				{
					if ($row["pref"])
					{
						$isIconMaked = $this->CreateIcon($this->filesDir, $this->GetProperty('name'), $row["iconw"], $row["iconh"], $row["pref"]);
					}
					elseif ($row["suff"])
					{
						$isIconMaked = $this->CreateIcon($this->filesDir, $this->GetProperty('name'), $row["iconw"], $row["iconh"], "", $row["suff"]);
					}
					else
					{
						$isIconMaked = $this->CreateIcon($this->filesDir, $this->GetProperty('name'), $row["iconw"], $row["iconh"]);
					}
					if (!$isIconMaked)
					{
						$fileName = JoinPath($this->filesDir, $this->GetProperty("name"));
						if (file_exists($fileName) && is_file($fileName))
						{
							unlink($fileName);
						}
						return false;
					}
				}
			}
			return $this->GetProperty('name');
		}
		else
		{
			$this->error = "Error when move uploaded file or file name was not generated";
			return false;
		}
	}

	function GenerateName($pref = '')
	{
		$this->LoadFromArray(pathinfo($this->GetProperty('name')));
		if ($this->GetProperty('extension'))
		{
			if ($pref) $pref = $pref."_";
			$fileName = $pref.md5(uniqid(time())).".". $this->GetProperty('extension');
			$this->SetProperty('name', $fileName);
			return true;
		}
		return false;
	}

	function Delete($f, $mIcons = array())
	{
		global $icons;
		$tmpIcons = array();

        //delete file
		$fileName = JoinPath($this->filesDir, $f);
		if (file_exists($fileName) && is_file($fileName))
		{
			unlink($fileName);
		}

		if ($mIcons && is_array($mIcons))
		{
			$tmpIcons = $mIcons;
		}
		elseif ($icons && is_array($icons))
		{
			$tmpIcons = $icons;
		}

		//delete icons
		if ($tmpIcons && is_array($tmpIcons))
		{
			foreach ($tmpIcons as $icon)
			{
				$iconFileName = JoinPath($this->filesDir, $icon["pref"].$f);
				if (file_exists($iconFileName) && is_file($iconFileName))
				{
					unlink($iconFileName);
				}

			}
		}
	}

	function CreateIcon($dir, $fileName, $newWidth, $newHeight, $prefix = "icon_", $suffix = "")
	{
		$mode = $this->GetProperty('CreateIconsMode');

		list($name, $extension) = explode('.', $fileName);
		$imgInfo = @getimagesize($dir.$fileName);

		$GIF_IMAGE = 1;
		$JPEG_IMAGE = 2;

		$imageInitialWidth = $imgInfo[0];
		$imageInitialHeight = $imgInfo[1];
		$imageType = $imgInfo[2];

		if ($imgInfo && ($imageType == $JPEG_IMAGE || $imageType == $GIF_IMAGE))
		{
			$quality = "";
			if ($imageType == $JPEG_IMAGE)
			{
				if (!function_exists("imagejpeg"))
				{
					$this->error = "Your server doesn't support creation of JPEG files";
					return false;
				}
				$create_image_function = "imagecreatefromjpeg"; // $func1
				$output_image_function = "imagejpeg"; // func2
				$quality = "90";
			}
			elseif ($imageType == $GIF_IMAGE)
			{
				if (!function_exists("imagegif"))
				{
					$this->error = "Your server doesn't support creation of GIF files";
					return false;
				}
				$create_image_function = "imagecreatefromgif";
				$output_image_function = "imagegif";
			}
			else
			{
				$this->error = "Invalig image file. Only jpeg and gif are allowed";
				return false;
			}

			$coefficient_H = $imageInitialHeight / $newHeight;
			$coefficient_W = $imageInitialWidth / $newWidth;
			$coefficient_Original = $imageInitialWidth / $imageInitialHeight;
			$coefficient_Required = $newWidth / $newHeight;
			switch($mode)
			{
				case "byHeight":
					$iconNewWidth = round($imageInitialWidth / $coefficient_H);
					$iconNewHeigth = $newHeight;
					break;
				case "byWidth":
					$iconNewWidth = $newWidth;
					$iconNewHeigth = round($imageInitialHeight / $coefficient_W);
					break;
				default: // both
					if ($coefficient_Original > $coefficient_Required)
					{
						$iconNewWidth = $newWidth;
						$iconNewHeigth = round($imageInitialHeight / $coefficient_W);
					}
					else
					{
						$iconNewWidth = round($imageInitialWidth / $coefficient_H);
						$iconNewHeigth = $newHeight;
					}
			}
			$img1 = $create_image_function($dir.$fileName);
			$img2 = imagecreatetruecolor($iconNewWidth, $iconNewHeigth);
			$img3 = imagecreatetruecolor($newWidth, $newHeight);
			imagecopyresampled($img2, $img1, 0, 0, 0, 0, $iconNewWidth, $iconNewHeigth, $imageInitialWidth, $imageInitialHeight);

			switch($mode)
			{
				case "byHeight":
					if ($iconNewWidth > $newWidth)
					{
					    imagecopy($img3, $img2, 0, 0, 0, 0, $newWidth, $newHeight);
						$imageSource = $img3;
					}
					else
					{
						$imageSource = $img2;
					}
					break;
				case "byWidth":
					if ($iconNewHeigth > $newHeight)
					{
					    imagecopy($img3, $img2, 0, 0, 0, 0, $newWidth, $newHeight);
						$imageSource = $img3;
					}
					else
					{
						$imageSource = $img2;
					}
					break;
				default:
					$imageSource = $img2;
			}

			if ($quality)
			{
				$output_image_function($imageSource, $dir.$prefix.$name.$suffix.".".$extension, $quality);
			}
			else
			{
				$output_image_function($imageSource, $dir.$prefix.$name.$suffix.".".$extension);
			}
			imagedestroy($img3);
			imagedestroy($img2);
			imagedestroy($img1);
			@chmod($dir.$prefix.$name.$suffix.".".$extension, 0644);
			return true;
		}
		else
		{
			$this->error = "Invalig image file. Class does not support this type";
			return false;
		}
	}



	function UploadFile( $basePath = FILES_DIR, $filePref = '')
	{
		if ($this->GenerateName($filePref) &&
			move_uploaded_file($this->GetProperty('tmp_name'), JoinPath($basePath, $this->GetProperty('name'))))
		{

			return $this->GetProperty('name');
		}
		else
		{
			$this->error = "Error when move uploaded file or file name was not generated";
		}
	}


	function ResizeImage($dir, $fileName, $newWidth, $newHeight, $mode='')
	{
		list($name, $extension) = explode('.', $fileName);
		$imgInfo = getimagesize($dir.$fileName);

		$GIF_IMAGE = 1;
		$JPEG_IMAGE = 2;

		$imageInitialWidth = $imgInfo[0];
		$imageInitialHeight = $imgInfo[1];
		$imageType = $imgInfo[2];

		if ($imgInfo && ($imageType == $JPEG_IMAGE || $imageType == $GIF_IMAGE))
		{
			$quality = "";
			if ($imageType == $JPEG_IMAGE)
			{
				if (!function_exists("imagejpeg"))
				{
					$this->error = "Your server doesn't support creation of JPEG files";
					return false;
				}
				$create_image_function = "imagecreatefromjpeg"; // $func1
				$output_image_function = "imagejpeg"; // func2
				$quality = "90";
			}
			elseif ($imageType == $GIF_IMAGE)
			{
				/*if (!function_exists("imagegif"))
				{
					$this->error = "Your server doesn't support creation of GIF files";
					return false;
				}*/
				$create_image_function = "imagecreatefromgif";
				//$output_image_function = "imagegif";
				$output_image_function = "imagejpeg"; // func2
			}
			else
			{
				$this->error = "Invalig image file. Only jpeg and gif are allowed";
				return false;
			}

			$coefficient_H = $imageInitialHeight / $newHeight;
			$coefficient_W = $imageInitialWidth / $newWidth;
			$coefficient_Original = $imageInitialWidth / $imageInitialHeight;
			$coefficient_Required = $newWidth / $newHeight;
			switch($mode)
			{
				case "byHeight":
					$iconNewWidth = round($imageInitialWidth / $coefficient_H);
					$iconNewHeigth = $newHeight;
					break;
				case "byWidth":
					$iconNewWidth = $newWidth;
					$iconNewHeigth = round($imageInitialHeight / $coefficient_W);
					break;
				default: // both
					if ($coefficient_Original > $coefficient_Required)
					{
						$iconNewWidth = $newWidth;
						$iconNewHeigth = round($imageInitialHeight / $coefficient_W);
					}
					else
					{
						$iconNewWidth = round($imageInitialWidth / $coefficient_H);
						$iconNewHeigth = $newHeight;
					}
			}
			$img1 = $create_image_function($dir.$fileName);

			//$black = imagecolorallocate($img1, 0, 0, 0);
			//imagecolortransparent($img1, $black);

			$img2 = imagecreatetruecolor($iconNewWidth, $iconNewHeigth);
			$img3 = imagecreatetruecolor($newWidth, $newHeight);
			imagecopyresampled($img2, $img1, 0, 0, 0, 0, $iconNewWidth, $iconNewHeigth, $imageInitialWidth, $imageInitialHeight);

			switch($mode)
			{
				case "byHeight":
					if ($iconNewWidth > $newWidth)
					{
				    imagecopy($img3, $img2, 0, 0, 0, 0, $newWidth, $newHeight);
						$imageSource = $img3;
					}
					else
					{
						$imageSource = $img2;
					}
					break;
				case "byWidth":
					if ($iconNewHeigth > $newHeight)
					{
				    imagecopy($img3, $img2, 0, 0, 0, 0, $newWidth, $newHeight);
						$imageSource = $img3;
					}
					else
					{
						$imageSource = $img2;
					}
					break;
				default:
					$imageSource = $img2;
			}

			if ($quality)
			{
				$output_image_function($imageSource, "", $quality);
			}
			else
			{
				$output_image_function($imageSource);
			}
			imagedestroy($img3);
			imagedestroy($img2);
			imagedestroy($img1);
			return ;
		}
		else
		{
			$this->error = "Invalig image file. Class does not support this type";
			return false;
		}
	}

  function WatermarkImage($waterm) 
  {
    $path = $_SERVER['DOCUMENT_ROOT'].$_SERVER['REQUEST_URI'];

    $image = imagecreatefromstring(file_get_contents($path));
    $w = imagesx($image);
    $h = imagesy($image);

      $watermark = imagecreatefrompng($waterm);
      $ww = imagesx($watermark);
      $wh = imagesy($watermark);
      
      if ($w > $ww && $h > $wh ) 
      {
        imagealphablending($image, true);
        imagecopy($image, $watermark, $w-$ww, $h-$wh, 0, 0, $ww, $wh);
        //$ww1 = $w/2;
        //$wh1 = $wh*$w/(2*$ww);
        //$watermark_p = imagecreatetruecolor($ww1, $wh1);
        //imagecopyresampled($watermark_p, $watermark, 0, 0, 0, 0, $ww1, $wh1, $ww, $wh); 
        //$transparency=50;
        //imagecopymerge($image, $watermark, $w-$ww, $h-$wh, 0, 0, $ww, $wh, $transparency);
      }

      eregi('\.(gif|jpeg|jpg|png)$',$path,$regs);
      switch( $regs[1] )
      {
        case 'gif':
          header('Content-type: image/gif');
          imagegif($image);
          break;
        case 'jpg':
          header('Content-type: image/jpeg');
          imagejpeg($image);
          break;
        case 'jpeg':
          header('Content-type: image/jpeg');
          imagejpeg($image);
          break;
        case 'png':
          header('Content-type: image/png');
          imagepng($image);
          break;
      }
      exit();

  }
}
?>