<?php
include_once(dirname(__FILE__)."/phpmailer.php");

class SendEmail extends LocalObject
{
  function Send()
  {
    /*$phpmailer = new PHPMailer();
    $phpmailer->IsSendmail();
    $phpmailer->From = ($this->GetProperty("From")) ? $this->GetProperty("From") : ADMIN_EMAIL;
    $phpmailer->ContentType = "text/html";
    $phpmailer->CharSet = "windows-1251";
    $phpmailer->FromName = FROM_NAME;
    $phpmailer->Subject = $this->GetProperty("Subject");
    $phpmailer->Body = $this->GetProperty("Text");*/

    $phpmailer = new PHPMailer();
    $phpmailer->IsSMTP();
    $phpmailer->Host = '192.168.10.2';
    $phpmailer->Port = 25;
    $phpmailer->SMTPAuth = false;
    $phpmailer->SetFrom($this->GetProperty("From") ? $this->GetProperty("From") : ADMIN_EMAIL, FROM_NAME);
    $phpmailer->ContentType = "text/html";
    $phpmailer->CharSet = "windows-1251";
    $phpmailer->Subject = $this->GetProperty("Subject");
    $body = $this->GetProperty("Text");
    $phpmailer->MsgHTML($body);

   if($this->GetProperty("SendCopyTo")){
       if(!is_array($this->GetProperty("SendCopyTo"))){
            $phpmailer->AddAddress($this->GetProperty("SendCopyTo"));
       }else{ //� ����� ������ �� ���������� ����������
           foreach($this->GetProperty("SendCopyTo") as $mailCopy){
               $phpmailer->AddAddress($mailCopy);
           }
       }
   }
   if($this->GetProperty("SendCopyTo2")){
    $phpmailer->AddAddress($this->GetProperty("SendCopyTo2"));
   }
    //$phpmailer->AddAddress("ascheulov@shaklin.ru");
    $phpmailer->AddAddress($this->GetProperty("SendTo"));
    $phpmailer->Send();
    $phpmailer->ClearAllRecipients();
  }
}

?>
