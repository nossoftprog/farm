<?php
require_once("user.php");

class Product extends LocalObject
{
  var $error = null;
  var $message = null;

  function Product()
  {
  }

  function LoadFromDataBase()
  {
    if ($this->GetProperty("PriceID"))
    {
      $query = "SELECT *
        FROM price 
        WHERE priceHistory=1
        AND priceID = ".$this->GetPropertyForSQL("PriceID");
      $this->LoadFromSQL($query);
      $arPrices = array("vip" => $this->GetProperty("PricePrice"), "entry" => $this->GetProperty("PriceEntry"),"tork" => $this->GetProperty("PriceTork"));
      $this->SetProperty("PricePrice",$this->GetUserPrice($arPrices));

    }
  }

  function LoadFromDataBaseAdd()
  {
    $HistOrderIDTemp=0;
    if (isset($_SESSION['OrderID']) && $_SESSION['OrderID'])
    {
      $HistOrderIDTemp = $_SESSION['OrderID'];
    }

    if ($this->GetProperty("priceID"))
    {
      $query = "SELECT *
        FROM price p LEFT JOIN price_addition pa ON pa.PriceAddPriceID=p.PriceID  
	  LEFT JOIN history_ ON history_.HistPriceID=p.priceID AND history_.HistOrderID='".$HistOrderIDTemp."' 
       WHERE priceHistory=1
        AND priceID = ".$this->GetPropertyForSQL("priceID");
      $this->LoadFromSQL($query);
      $arPrices = array("vip" => $this->GetProperty("PricePrice"), "entry" => $this->GetProperty("PriceEntry"),"tork" => $this->GetProperty("PriceTork"));
      $this->SetProperty("PricePrice",$this->GetUserPrice($this->GetUserPrice($arPrices)));
    }
  }

  function CheckAddInformation()
  {
    return $this->CheckInformation();
  }

  function CheckUpdateInformation()
  {
    return $this->CheckInformation();
  }

  function CheckInformation()
  {
    return null;
  }

  /*function GetUserPrice($price){
        if (isset($_SESSION['userID'])) {
          $user = new User();
          $user->SetProperty('UserID',$_SESSION['userID']);
          $user->LoadFromDataBase();
          $discount = $user->GetProperty("UserPricePercent") ? $user->GetProperty("UserPricePercent") : 1;
          $price = round($discount*$price,2, PHP_ROUND_HALF_DOWN);
       }
        return $price;
  }*/
    function GetUserPrice($arPrices){
            if (isset($_SESSION['userID'])) {
              $user = new User();
              $user->SetProperty('UserID',$_SESSION['userID']);
              $user->LoadFromDataBase();
                //���������� ������� �������
              $userPrice = $arPrices[$user->GetProperty("price_type")] ? $arPrices[$user->GetProperty("price_type")] : $arPrices["vip"];
              $discount = $user->GetProperty("UserPricePercent") ? $user->GetProperty("UserPricePercent") : 1;
              $price = round($discount*$userPrice,2, PHP_ROUND_HALF_DOWN);
           }
            return $price;
     }

  //add/edit
  function GetTotalCountAdd($id = '0')
  {
    $query = "SELECT count(*) AS total FROM price_addition where priceAddPriceID='" . $id . "'";
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }

  function Update()
  {
    global $post;
    $str = "priceAddDesc=".$this->GetPropertyForSQL('priceAddDesc').", priceAddPriceID =".$this->GetPropertyForSQL('priceID').", priceAddmetID=".$post->GetPropertyForSQL('priceAddmetID').", priceAddTemp=".$post->GetPropertyForSQL('PriceStatID').", PriceAddKeywords=".$post->GetPropertyForSQL('PriceAddKeywords').", PriceAddFiles=".$this->GetPropertyForSQL('PriceAddFiles');
    if ($this->GetProperty('priceAddID'))
    {
      $query = "UPDATE price_addition SET ".$str." WHERE priceAddID=".$this->GetPropertyForSQL('priceAddID');
    }
    else
    {
      $query = "INSERT INTO price_addition SET ".$str;
    }
    //echo $query;
    //exit;
    $stmt = GetStatement();
    $stmt->Execute($query);
  }

  function UpdateDesc($add='0')
  {
    $str = "priceAddPriceID =".$this->GetPropertyForSQL('priceAddPriceID');
    if ($this->GetPropertyForSQL('priceAddDesc') !="'#empty#'") {
      $str = $str . ", priceAddDesc=".$this->GetPropertyForSQL('priceAddDesc');
    }
    if ($this->GetPropertyForSQL('priceAddTech') !="'#empty#'") {
      $str = $str . ", priceAddTech=".$this->GetPropertyForSQL('priceAddTech');
    }
    if ($this->GetPropertyForSQL('priceAddImages') !="'#empty#'") {
      $str = $str . ", priceAddImages=".$this->GetPropertyForSQL('priceAddImages');
    }
    if ($add != '0' && $add != 0)
    {
      $query = "UPDATE price_addition SET ".$str." WHERE priceAddPriceID=".$this->GetPropertyForSQL('priceAddPriceID');
    }
    else
    {
      $query = "INSERT INTO price_addition SET ".$str;
    }

    $stmt = GetStatement();
    $stmt->Execute($query);
    //echo $query."<br>";
  }

  function UpdateFiles()
  {
    global $post;
    $str = "priceAddDesc=".$this->GetPropertyForSQL('priceAddDesc').", priceAddPriceID =".$this->GetPropertyForSQL('priceID').",    PriceAddFiles=".$this->GetPropertyForSQL('PriceAddFiles');
    if ($this->GetProperty('priceAddID'))
    {
      $query = "UPDATE price_addition SET ".$str." WHERE priceAddID=".$this->GetPropertyForSQL('priceAddID');
    }
    else
    {
      $query = "INSERT INTO price_addition SET ".$str;
    }
    //echo $query;
    //exit;
    $stmt = GetStatement();
    $stmt->Execute($query);
  }

}
?>