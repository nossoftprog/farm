<?php
require_once("objectslist.php");

class LocalObjectList extends ObjectsList
{
	var $totalCount	;

	// Constructor
	function LocalObjectList($data = array())
	{
		if (is_array($data))
		{
			$this->ObjectsList($data);
		}
		else
		{
			$this->ObjectsList($data, GetStatement());
		}
		
		$this->totalCount = 0;
	}

	// LoadFromSQL
	function LoadFromSQL($query)
	{
		ObjectsList::LoadFromSQL($query, GetStatement());
	}
	
	function GetTotalCount()
	{
		return $this->totalCount;
	}

}
?>