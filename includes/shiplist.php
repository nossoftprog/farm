<?php
require_once("localobjectlist.php");
//require_once("invoice.php");

class ShipList extends LocalObjectList
{

	function LoadFromDataBase($user = '0')
	{
		$query = "SELECT ship.*, v_invoices.InvoiceID1 FROM ship LEFT JOIN v_invoices ON InvoiceGUID=ShipGUID WHERE ShipUserGUID='" . $user ."' ORDER BY ShipDate DESC, ShipDocDate DESC";
    //$query = "SELECT ship.*, invoices.InvoiceID FROM ship LEFT JOIN invoices ON InvoiceGUID=ShipGUID WHERE ShipUserGUID='" . $user ."' AND InvoiceDate = (select max(InvoiceDate) from invoices) ORDER BY ShipDate DESC, ShipDocDate DESC";
    //echo $query;
		$this->LoadFromSQL($query);
  }

	function LoadFromDataBase1($selected = 0)
	{
		$query = "SELECT *, IF(PriceStatID = '".mysql_escape_string($selected)."', 1, 0) AS selected_p
  				FROM price_status where PriceStatTemp != 1 ORDER BY PriceStatTemp, PriceStatID";
		$this->LoadFromSQL($query);
  }

	function Delete($data)
	{
		if (is_array($data) && $data)
		{
			$ids = implode(",", Connection::GetSQLArray($data));
			if ($ids)
			{

				$query = "DELETE FROM price_status WHERE PriceStatID IN (".$ids.")";
        $stmt = GetStatement();
				$stmt->Execute($query);

			}
		}
	}

  function GetTotalCount($front = 0)
  {
    $str = "";
    $query = "SELECT count(*) AS total FROM price_status";
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }

  function GetTotalCountNew($front = 0)
  {
    $str = "";
    $query = "SELECT count(*) AS total FROM invoices WHERE InvoiceShowed='1' AND InvoiceUserName='".$_SESSION['userName']."'";
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }
}
?>