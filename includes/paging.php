<?php
class Paging
{
	function Paging($page, $totalCount, &$tpl)
	{
		$pagesCount = ceil($totalCount/ITEMS_PER_PAGE);
		if($pagesCount > 1)
		{
			$left = 5;
			$right = 5;
			$pages = array();
			for ($i = 1; $i <= $pagesCount; $i++)
			{
				if (($i == $page - $left - 1) || ($i == $page + $right + 1))
				{
					$pages[$i] = array('more'=>true);
				}
				else if ($i >= $page - $left && $i <= $page + $right)
				{
					$pages[$i] = array('current'=>($i == $page), 'page'=>$i);
				}
			}
			$tpl->SetLoop('pages', $pages);
		}
		$tpl->SetVar("CurrentPage", $page);
	}
}

?>