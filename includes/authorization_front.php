<?php

require_once("localobject.php");
require_once("user.php");

class Authorization {

    /**
     * Current web site user
     *
     * @var User
     */
    var $user;
    var $message;

    //constructor
    function Authorization() {
        $this->user = new User();
        $this->message = null;
    }

    //if the User's account and password are compatible
    function Validate($type) {
        $get = new LocalObject($_GET);
        if ($get->GetProperty('logout') != 1) {
            $conn = GetConnection();
            if ($type) {
                if (isset($_POST['Login'])){
                    $_POST['Login'] = trim($_POST['Login']);
                    $this->user->SetProperty('login', $_POST['Login']);
                    /* 				$query = "SELECT *
                      FROM users_ LEFT JOIN admins on users_.UserAdminID=admins.AdminID
                      WHERE UserName = ".Connection::GetSQLString($_POST['Login'])."
                      AND UserPassword = ".Connection::GetSQLString(crypt($_POST['Password'],'a552avf1ss'))."";
                     */
                    //���������� ����������� �� ������ � ����� ���������� ��� ����� ��� ��� ���� ������������		
                    $query = "SELECT * FROM users_ LEFT JOIN admins on users_.UserAdminID=admins.AdminID WHERE UserName = " . Connection::GetSQLString($_POST['Login']) . "";
                    $stmt = $conn->CreateStatement();
                    /* @var $stmt Statement */

                    $rs = $stmt->Execute($query);
                    /* @var $rs RecordSet */
                    if ($stmt->GetNumRows()) {
                        $row = $rs->NextRow();
                        $_SESSION['userID'] = $row['UserID'];
                        $_SESSION['guid'] = $row['CoflexID'];
                        $_SESSION['userName'] = $row['UserName'];
                        $_SESSION['userType'] = 6;
                        $_SESSION['userAdminID'] = $row['UserAdminID'];
                        $_SESSION['userCheck1'] = $row['UserCheck1'];
                        $_SESSION['userCheck2'] = $row['UserCheck2'];
                        $_SESSION['userCheck3'] = $row['UserCheck3'];
                        $orderEmail = explode(";", $row['UserAdminEmail']);
                        if (isset($row['AdminEmail']) && $row['AdminEmail']) {
                            $orderEmail = explode(";", $row['AdminEmail']);
                            //����� ���������� ����������
                            $_SESSION['OrderEmail'] = array_shift($orderEmail);
                            $_SESSION['OrderEmailCopy'] = count($orderEmail) ? $orderEmail : false;
                        } elseif (isset($row['UserAdminEmail']) && $row['UserAdminEmail']) {
                            //����� ���������� ����������
                            $_SESSION['OrderEmail'] = array_shift($orderEmail);
                            $_SESSION['OrderEmailCopy'] = count($orderEmail) ? $orderEmail : false;
                        } else {
                            $_SESSION['OrderEmail'] = ORDER_EMAIL;
                        }
                        $_SESSION['FromEmail'] = ADMIN_EMAIL;
                        $this->user->SetProperty('userID', $row['UserID']);
                        $this->user->SetProperty('userName', $row['UserName']);
                        $this->user->SetProperty('userType', 6);
                        $this->user->SetProperty('userAdminID', $_SESSION['userAdminID']);
                        //������ ���� � ID ������������
                        setcookie("userID", $row['UserID'], time() + COOKIE_LIFE_TIME, "/");
                        $stmt = GetStatement();
                        $query = "SELECT * FROM motivated_users WHERE guid = '".$_SESSION['guid']."' AND aborted_at IS NOT NULL LIMIT 1";
                        $motUser = $stmt->FetchRow($query);
                        if(empty($motUser)){
                            header("Location: /".ROUTE_MOT_MAIN);
                        }
                        
                        return $this->GetUser();
                    } else {
                        $this->message = "�������� ��� ��� ������";
                        return false;
                    }
                } elseif ((isset($_SESSION['userID']) && $_SESSION['userID']) || (isset($_COOKIE['userID']) && $_COOKIE['userID'])) {//��������� ������� cookie
                    $userID = $_SESSION['userID'] ? Connection::GetSQLString($_SESSION['userID']) : Connection::GetSQLString($_COOKIE['userID']);
//				$query = "SELECT UserID, UserName
//						FROM users_
//            WHERE UserID=". $userID;
                    $query = "SELECT *
						FROM users_ LEFT JOIN admins on users_.UserAdminID=admins.AdminID
						WHERE UserID=" . $userID;
                    $stmt = $conn->CreateStatement();
                    $rs = $stmt->Execute($query);

                    if ($stmt->GetNumRows()) {
                        if (!isset($_SESSION['userID']) || !$_SESSION['userID']) {
                            $row = $rs->NextRow();
                            $_SESSION['userID'] = $row['UserID'];
                            $_SESSION['guid'] = $row['CoflexID'];
                            $_SESSION['userName'] = $row['UserName'];
                            $_SESSION['userType'] = 6;
                            $_SESSION['userAdminID'] = $row['UserAdminID'];
                            $orderEmail = explode(";", $row['UserAdminEmail']);
                            $_SESSION['userCheck1'] = $row['UserCheck1'];
                            $_SESSION['userCheck2'] = $row['UserCheck2'];
                            $_SESSION['userCheck3'] = $row['UserCheck3'];

                            if (isset($row['AdminEmail']) && $row['AdminEmail']) {
                                $orderEmail = explode(";", $row['AdminEmail']);
                                //����� ���������� ����������
                                $_SESSION['OrderEmail'] = array_shift($orderEmail);
                                $_SESSION['OrderEmailCopy'] = count($orderEmail) ? $orderEmail : false;
                            } elseif (isset($row['UserAdminEmail']) && $row['UserAdminEmail']) {
                                //����� ���������� ����������
                                $_SESSION['OrderEmail'] = array_shift($orderEmail);
                                $_SESSION['OrderEmailCopy'] = count($orderEmail) ? $orderEmail : false;
                            } else {
                                $_SESSION['OrderEmail'] = ORDER_EMAIL;
                            }

                            $_SESSION['FromEmail'] = ADMIN_EMAIL;
                        }
                        $this->user->SetProperty('userID', $_SESSION['userID']);
                        $this->user->SetProperty('userName', $_SESSION['userName']);
                        $this->user->SetProperty('userType', $_SESSION['userType']);
                        $this->user->SetProperty('userAdminID', $_SESSION['userAdminID']);
                        //return true;

                        return $this->GetUser();
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } elseif ($type == 99) {
                return false;
            }
        }
    }

    function GetUser() {
        return $this->user;
    }

    function GetErrorMessage() {
        return $this->message;
    }

    function Logout() {
        // Load BasketID
        if (isset($_SESSION['userID']) && $_SESSION['userID'] != '0' && $_SESSION['userID'] != 0)
            ; // && !isset($_SESSION['OrderID']))
        {
            $conn = GetConnection();
            $query = "SELECT OrderID
						FROM orders_
						WHERE OrderUserID = " . Connection::GetSQLString($_SESSION['userID']) . " 
						AND OrderStatus = '0'";


            $stmt = $conn->CreateStatement();
            /* @var $stmt Statement */

            $rs = $stmt->Execute($query);
            /* @var $rs RecordSet */
            if ($stmt->GetNumRows()) {
                $row = $rs->NextRow();
                $_SESSION['OrderID'] = $row['OrderID'];
            } else {
                $query = "INSERT INTO orders_ SET OrderAdminID=" . Connection::GetSQLString($_SESSION['userAdminID']) . ", OrderUserID=" . Connection::GetSQLString($_SESSION['userID']) . ", OrderStatus='0'";
                $stmt = $conn->CreateStatement();
                $stmt->Execute($query);
                $_SESSION['OrderID'] = $stmt->GetLastInsertID();
            }
        }

        /* if (isset($_SESSION['OrderID']) && isset($_SESSION['userID']) && !isset($_SESSION['userAdminIDchanged']))
          {
          $conn = GetConnection();
          $query = "UPDATE orders_ SET OrderAdminID=".Connection::GetSQLString($_SESSION['userAdminID'])."
          WHERE OrderUserID = ".Connection::GetSQLString($_SESSION['userID'])."
          AND OrderStatus = '0'";
          $stmt = $conn->CreateStatement();
          $stmt->Execute($query);
          $_SESSION['userAdminIDchanged'] = 1;
          } */

        $get = new LocalObject($_GET);
        if ($get->GetProperty('logout') == 1) {
            if (isset($_SESSION['userID'])) {
                $_SESSION = array();
            }
            setcookie("userID", $_SESSION['userID'], time() - 3600, "/");
            return true;
        } else {
            setcookie("userID", $_SESSION['userID'], time() - 3600, "/");
            return false;
        }
    }

}

ini_set('session.save_path', $_SERVER['DOCUMENT_ROOT'] . '/sessions/');
session_name(PROJECT_NAME_SESS_USER);
?>
