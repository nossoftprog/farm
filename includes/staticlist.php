<?php
require_once("localobjectlist.php");
require_once("functions.php");
require_once("static.php");


class StaticList extends LocalObjectList
{
  var $message = "";
  function LoadFromDataBase($page = 1)
  {
    $start = 0;
    if ($page > 0)
    {
      $start = ($page - 1)*ITEMS_PER_PAGE;
    }
    $rows = array();
    global $corporate;
    $query = "SELECT *
          FROM static_ 
          ORDER BY NewsID
          LIMIT ".$start.", ".ITEMS_PER_PAGE;
    $this->LoadFromSQL($query);
    for ($i=0;$i<$this->GetTotalCount();$i++) {
      if (isset($this->items[$i]) && isset($this->items[$i]['NewsContent']))
      {
        $this->items[$i]["Preview"] = CreatePreview($this->items[$i]['NewsContent']);
      }
    }
  }

  function LoadNewsList($selected = 0)
  {
    global $corporate;
    $rows = array();
    $query = "SELECT *,
        IF (NewsID= '".mysql_escape_string($selected)."', 1, 0) AS selected
          FROM static_
          ORDER BY NewsID";
    $this->LoadFromSQL($query);
    for ($i=0;$i<$this->GetTotalCount();$i++) {
      if (isset($this->items[$i]) && isset($this->items[$i]['NewsContent']))
      {
        $this->items[$i]["Preview"] = CreatePreview($this->items[$i]['NewsContent']);
      }
    }

  }


  function GetTotalCount()
  {
    global $corporate;
    $query = "SELECT count(*) AS total FROM static_";
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }

  function Delete($data)
  {
    if (is_array($data) && $data)
    {
      $ids = implode(",", Connection::GetSQLArray($data));
      if ($ids)
      {
        $query = "DELETE FROM static_ WHERE NewsID IN (".$ids.")";
        $stmt = GetStatement();
        $stmt->Execute($query);

      }
    }
  }

	function MoveField($newsID, $updown)
	{
    global $corporate;
		$news = new News();
		$news->SetProperty('NewsID', $newsID);
		$news->LoadFromDataBase();
		$row = null;
		if (is_numeric($updown) && is_numeric($newsID))
		{
			$stmt = GetStatement();
			if ($updown == 1)
			{
				$query = "SELECT * FROM static_
						WHERE NewsSortOrder >= ".intval($news->GetProperty('NewsSortOrder'))."
						AND NewsID<>".intval($newsID)." AND NewsCorporate=".$corporate."
						ORDER BY NewsSortOrder ASC
						LIMIT 1";
				$row = $stmt->FetchRow($query);
			}
			elseif ($updown == -1)
			{
				$query = "SELECT * FROM static_
						WHERE NewsSortOrder <= ".intval($news->GetProperty('NewsSortOrder'))."
						AND NewsID<>".intval($newsID)." AND NewsCorporate=".$corporate."
						ORDER BY NewsSortOrder DESC
						LIMIT 1";
				$row = $stmt->FetchRow($query);
			}
			if ($row)
			{
				$query = "UPDATE static_ SET
					NewsSortOrder=".intval($row['NewsSortOrder'])."
					WHERE NewsID=".intval($newsID);
				$stmt->Execute($query);

				$query = "UPDATE static_ SET
					NewsSortOrder=".intval($news->GetProperty('NewsSortOrder'))."
					WHERE NewsID=".intval($row['NewsID']);
				$stmt->Execute($query);
			}
		}
	}

}
?>