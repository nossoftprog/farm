<?php

class Quest extends LocalObject
{
  var $error = null;
  var $message = null;

  function Quest()
  {
  }

  function LoadFromDataBase()
  {
    if ($this->GetProperty("questID"))
    {
      $query = "SELECT *, DATE_FORMAT(questDate, '".USER_DATE_FORMAT."') AS questDate1
        FROM questions
        WHERE questID = ".$this->GetPropertyForSQL("questID");
      $this->LoadFromSQL($query);
    }
  }

  function CheckAddInformation()
  {
    return $this->CheckInformation();
  }

  function CheckUpdateInformation()
  {
    return $this->CheckInformation();
  }

  function CheckInformation()
  {
    if ($this->GetProperty("questQuest") == null)
    {
      return "������ �� ����� ���� ������";
    }
    elseif ($this->GetProperty("questAnswer") == null)
    {
      return "����� �� ����� ���� ������";
    }
    return null;
  }

  function CheckInformationUser()
  {
    if ($this->GetProperty("questQuest") == null)
    {
      return "������ �� ����� ���� ������";
    }
    elseif ($this->GetProperty("questUser") == null)
    {
      return "��� �� ����� ���� ������";
    }
    return null;
  }

  //add/edit
  function Update()
  {
    global $post;
    $str = "questAnswered=".$this->GetPropertyForSQL('questAnswered').", questAnswer =".$this->GetPropertyForSQL('questAnswer').", questQuest =".$this->GetPropertyForSQL('questQuest').", questActive=".$post->GetPropertyForSQL('questActive').", questUser=".$this->GetPropertyForSQL('questUser').", questEmail=".$this->GetPropertyForSQL('questEmail');
    if ($this->GetProperty('questID'))
    {
      $query = "UPDATE questions SET ".$str." WHERE questID=".$this->GetPropertyForSQL('questID');
    }
    else
    {
      $query = "INSERT INTO questions SET ".$str.", questDate=NOW()";;
    }
    $stmt = GetStatement();
    $stmt->Execute($query);
		if ($questID = $stmt->GetLastInsertID() && !$this->GetProperty('questID'))
		{
			$this->SetProperty('questID', $questID);
			$query = "UPDATE questions SET questSortOrder=".$questID." WHERE questID=".$questID;
			$stmt->Execute($query);
		}

  }
}
?>