<?php

require_once(dirname(__FILE__).'/statement.php');

class Connection
{
	var $_dbLink;

	function Connection($server = '', $databaseName = '', $userName = '', $password = '')
	{
		$this->_dbLink = mysql_connect($server, $userName, $password);
		if ($this->_dbLink === false)
		{
			trigger_error('Can\'t connect to database server.', E_USER_ERROR);
		}
		else
		{
			if (mysql_select_db($databaseName, $this->_dbLink) === false)
				trigger_error('Can\'t select database.', E_USER_ERROR);
		}
	}

	function &CreateStatement($resultType = MYSQL_ASSOC)
	{
		$stmt =& new Statement($this->_dbLink, $resultType);
		return $stmt;
	}

	function GetSQLString($str)
	{
		return "'".mysql_escape_string($str)."'";
	}

	function GetSQLLike($str)
	{
		return addcslashes(mysql_escape_string($str), "\\_%'");
	}
    function GetSQLToSearch($str)
    {
        return "+".implode("* +",explode(" ",mysql_escape_string($str)))."*";
    }

	function GetSQLArray($arr)
	{
		if (is_array($arr))
		{
			foreach ($arr as $key => $value)
				$arr[$key] = Connection::GetSQLString($value);
		}
		return $arr;
	}
}

?>