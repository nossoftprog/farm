<?php
require_once("localobjectlist.php");
require_once("functions.php");

class QuestList extends LocalObjectList
{
  var $message = "";
  function LoadFromDataBase($page = 1, $front = 0)
  {
    $start = 0;
    if ($page > 0)
    {
      $start = ($page - 1)*ITEMS_PER_PAGE;
    }
    $rows = array();
    $str = "";
    $top = ITEMS_PER_PAGE;
    if ($front == 1)
    {
      $str = " AND questActive = 1 AND questAnswered = 1";
    }
    $query = "SELECT *, DATE_FORMAT(questDate, '".USER_DATE_FORMAT."') AS questDate1
          FROM questions WHERE questID>0 ".$str."
          ORDER BY questAnswered, questDate DESC
          LIMIT ".$start.", ".$top;
    $this->LoadFromSQL($query);

    for ($i=0;$i<$this->GetTotalCount($front);$i++) {
      $answered = "";
      if (isset($this->items[$i]) && isset($this->items[$i]['questAnswered']) && $this->items[$i]['questAnswered'] == 1)
      {
        $answered = "X";
      }
      if (isset($this->items[$i]) && isset($this->items[$i]['questQuest']))
      {
        $this->items[$i]["Preview"] = CreatePreview($this->items[$i]['questQuest']);
        $this->items[$i]["questQuest"] = str_replace("\n", '<br>', $this->items[$i]["questQuest"]);
      }
      if (isset($this->items[$i]) && isset($this->items[$i]['questAnswer']))
      {
        $this->items[$i]["Preview1"] = CreatePreview($this->items[$i]['questAnswer']);
        $this->items[$i]["questAnswer"] = str_replace("\n", '<br>', $this->items[$i]["questAnswer"]);
      }
      $this->items[$i]["questAnswered"] = $answered;
    }
  }


  function GetTotalCount($front = 0)
  {
    $str = "";
    if ($front == 1)
    {
      $str = " AND questActive = 1 AND questAnswered = 1";
    }
    $query = "SELECT count(*) AS total FROM questions WHERE questID>1".$str;
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }

  function Delete($data)
  {
    if (is_array($data) && $data)
    {
      $ids = implode(",", Connection::GetSQLArray($data));
      if ($ids)
      {
        $query = "DELETE FROM questions WHERE questID IN (".$ids.")";
        $stmt = GetStatement();
        $stmt->Execute($query);

      }
    }
  }
}
?>