<?php
require_once("localobjectlist.php");
require_once("functions.php");
require_once('mysql.php'); 

class CatList extends LocalObjectList
{
  var $message = "";
  function LoadFromDataBase($page = 1)
  {
    $start = 0;
    if ($page > 0)
    {
      $start = ($page - 1)*ITEMS_PER_PAGE;
    }
    $rows = array();
    $query = "SELECT *
          FROM Category WHERE CatHistory=1";
    $this->LoadFromSQL($query);
  }

  function LoadCategoryTopForAdmin()
  {
    $query = "SELECT * FROM category WHERE CatParent='0' AND CatHistory=1 ";
    
    if (!isset($_SESSION['CatID_']) || strlen($_SESSION['CatID_']) == 0 || $_SESSION['CatID_'] == '0') 
    {
      $in_ = "";
      $queryone = "SELECT * FROM category WHERE CatParent='0'" . $and_ . " AND CatHistory=1  LIMIT 0, 1";
      $stmt = GetStatement();
      $rows = $stmt->FetchList($queryone);
      $_SESSION['CatID_'] = $rows[0]['CatID']; 
      $_SESSION['CatName_'] = $rows[0]['CatDescription']; 
    }

    if (isset($_REQUEST['CatID'])) 
    {
      $in_ = "";
      $queryone = "SELECT * FROM category WHERE CatHistory=1 AND CatID='".$_REQUEST['CatID']."'";
      $stmt = GetStatement();
      $_SESSION['CatID_'] = $stmt->FetchField($queryone, "CatID");
      $_SESSION['CatName_'] = $stmt->FetchField($queryone, "CatDescription");
    }

    $this->LoadFromSQL($query);
  }

  function LoadCategoryTop()
  {
    $rows = array();
    $and_ = "";
    /*if ( SHOW_STOMAT == 0 ) {
      $and_ = $and_  . " AND CatID <> '12764092'";
      if (isset($_SESSION['CatID']) && $_SESSION['CatID'] == '12764092')
      {
        $_SESSION['CatID'] = '0';
      }
    }
    if ( SHOW_OPT == 0 ) {
      $and_ = $and_  . " AND CatID <> '12845022'";
      if (isset($_SESSION['CatID']) && $_SESSION['CatID'] == '12845022')
      {
        $_SESSION['CatID'] = '0';
      }
    }
    if ( SHOW_ALL == 0 ) {
      $and_ = $and_  . " AND CatID <> '32612542'";
      if (isset($_SESSION['CatID']) && $_SESSION['CatID'] == '32612542')
      {
        $_SESSION['CatID'] = '0';
      }
    }*/
    $query = "SELECT * FROM category WHERE CatParent='0'" . $and_ . " AND CatHistory=1  ORDER BY CatName ";
    
    if (!isset($_SESSION['CatType']))
    {
        $_SESSION['CatType'] = 1;
        //������ �� ��������� ����� ���������, ���� ���������� ����� ������ ��������� - �������� ��� �����.
        if(isset($_COOKIE['CatType']) && $_COOKIE['CatType'] == 1)
            $_SESSION['CatType'] = 1;
        else
            $_SESSION['CatType'] = 0;
    }
    if (isset($_REQUEST['CatType']))
    {
      $_SESSION['CatType'] = $_REQUEST['CatType'];
      //��������� ����� ����������
      setcookie ("CatType", $_SESSION['CatType'], time()+COOKIE_LIFE_TIME, "/");

    }

    //$_SESSION['CatType']=0; //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    $_SESSION['CatID'] = '0';

    if (!isset($_SESSION['CatID']) || strlen($_SESSION['CatID']) == 0 || $_SESSION['CatID'] == '0') 
    {
      include_once('mysql.php'); 
      $queryone = "UPDATE category SET CatIndex='1' WHERE CatParent='0'";
      $dbc = new Mysql;
      //$dbc->executeQuery($queryone);
      $in_ = "";
      $queryone = "SELECT * FROM category WHERE CatParent='0'" . $and_ . " AND CatHistory=1  ORDER BY CatName LIMIT 0, 1";
      //echo $queryone;
      //exit;
      $stmt = GetStatement();
      $rows = $stmt->FetchList($queryone);
      $_SESSION['CatID'] = $rows[0]['CatID']; //$stmt->FetchField($queryone, "CatID");
      $_SESSION['CatName'] = $rows[0]['CatDescription']; //$stmt->FetchField($queryone, "CatDescription");
      $queryone = "SELECT CatID FROM category WHERE CatParent='".$_SESSION['CatID']."' AND CatHistory=1";
      $rows = $stmt->FetchList($queryone);

      for ($i=0; $i<count($rows); $i++) {
        if ($i == 0) {
				  $in_ .= "'".$rows[$i]['CatID']."'";
        } else {
          $in_ .= ",'".$rows[$i]['CatID']."'"; 
        }
			}
      $_SESSION['CatIN'] = "(".$in_.")";
    }

    /*if (isset($_REQUEST['CatID'])) 
    {
      $in_ = "";
      $queryone = "SELECT * FROM category WHERE CatHistory=1 AND CatID=".$_REQUEST['CatID'] . $and_;
      $stmt = GetStatement();
      $_SESSION['CatID'] = $stmt->FetchField($queryone, "CatID");
      $_SESSION['CatName'] = $stmt->FetchField($queryone, "CatDescription");
      $queryone = "SELECT CatID FROM category WHERE CatParent='".$_SESSION['CatID']."' AND CatHistory=1";
      $rows = $stmt->FetchList($queryone);

      for ($i=0; $i<count($rows); $i++) {
        if ($i == 0) {
				  $in_ .= $rows[$i]['CatID'];
        } else {
          $in_ .= ",".$rows[$i]['CatID'];
        }
			}
      $_SESSION['CatIN'] = "(".$in_.")";
    }*/

    $_SESSION['CatDESC'] = "";
    if ($_SESSION['CatID'] == '12764092') {
      $_SESSION['CatDESC'] = " DESC ";
    }

    $this->LoadFromSQL($query);
  }

  function GetTotalCount()
  {
    global $corporate;
    $query = "SELECT count(*) AS total FROM category WHERE CatHistory=1";
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }

}