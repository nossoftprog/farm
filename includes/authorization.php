<?php
require_once("localobject.php");
require_once("admin.php");

define("SUPERADMIN", 1);
define("ADMIN", 2);
define("CUSTOMER", 3);

if (array_key_exists('HTTP_USER_AGENT', $_SERVER) && !$name = strstr ($_SERVER['HTTP_USER_AGENT'], "MSIE")) { 
  //Header ("Location: browser_msie.html"); 
} 



class Authorization
{
	/**
	 * Current web site user
	 *
	 * @var User
	 */
	var $user;
	var $message;
  var $authfile;

  
	//constructor
	function Authorization()
	{
		$this->user = new Admin();
		$this->message = null;
    $this->authfile = Array();
    $this->authfile[1] = SUPERADMIN_FIRST_FILE;
    $this->authfile[2] = ADMIN_FIRST_FILE;
    $this->authfile[3] = MANAGER_FIRST_FILE;
    $this->authfile[4] = EDITOR_FIRST_FILE;
    $this->authfile[5] = PERSON_FIRST_FILE;
	}

	//if the User's account and password are compatible
	function Validate($type)
	{
		$conn = GetConnection();
		/* @var $conn Connection */

		if ($type)
		{
			if (isset($_POST['Login']) && isset($_POST['Password']))
			{
				$this->user->SetProperty('login', $_POST['Login']);
				$query = "SELECT AdminID, AdminNick, AdminLevel, AdminEmail
						FROM admins
						WHERE AdminActive=1 AND AdminNick = ".Connection::GetSQLString($_POST['Login'])." 
						AND AdminPassword = ".Connection::GetSQLString($_POST['Password'])."";
						

				$stmt = $conn->CreateStatement();
				/* @var $stmt Statement */

				$rs = $stmt->Execute($query);
				/* @var $rs RecordSet */
				if ($stmt->GetNumRows())
				{
					$row = $rs->NextRow();
					$_SESSION['userId'] = $row['AdminID'];
					$_SESSION['userName'] = $row['AdminNick'];
					$_SESSION['userType'] = $row['AdminLevel'];
					$_SESSION['FromEmail'] = $row['AdminEmail'];
          $_SESSION['OrderSort'] = 'OrderDateCreated DESC';
          $_SESSION['ZakazSort'] = 'UserLastName';
					$this->user->SetProperty('admin_id', $row['AdminID']);
					$this->user->SetProperty('login', $row['AdminNick']);
					$this->user->SetProperty('type', $row['AdminLevel']);
					//return false;
					return $this->GetUser();
				}
				else
				{
					$this->message = "�������� ��� ��� ������";
					return false;
				}
			}
			elseif (isset($_SESSION['userId']) && $_SESSION['userId'] && isset($_SESSION['userType']))
			{
 				$query = "SELECT AdminID, AdminNick, AdminPassword
 						FROM admins
 						WHERE AdminActive=1 AND AdminID = ". Connection::GetSQLString($_SESSION['userId']);
				$stmt = $conn->CreateStatement();
				$rs = $stmt->Execute($query);

 				if ($stmt->GetNumRows())
				{
					$this->user->SetProperty('admin_id', $_SESSION['userId']);
					$this->user->SetProperty('login', $_SESSION['userName']);
					$this->user->SetProperty('type', $_SESSION['userType']);
          if ($type[$_SESSION['userType']] == 0) 
          {
            header("Location: ".$this->authfile[$_SESSION["userType"]]);
            exit;
          }
					//return true;
                    setcookie('isAdmin',$this->user->GetProperty('type'),0,'/');
					return $this->GetUser();
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		elseif ($type == CUSTOMER)
		{
			return false;
		}
	}

	function GetUser()
	{
		return $this->user;
	}

	function GetErrorMessage()
	{
		return $this->message;
	}

	function Logout()
	{
		$get = new LocalObject($_GET);
		if ($get->GetProperty('logout') == 1)
		{
            setcookie('isAdmin','false',time()-100,'/');
			if (isset($_SESSION['userId']))
			{
				$_SESSION = array();
			}
			return true;
		}
		else
		{
			return false;
		}
	}
}
session_name(PROJECT_NAME_SESS);
session_start();
?>