<?php
require_once("localobjectlist.php");
require_once("functions.php");
require_once("faq.php");

class FaqList extends LocalObjectList
{
  var $message = "";
  function LoadFromDataBase($page = 1, $front = 0)
  {
    $start = 0;
    if ($page > 0)
    {
      $start = ($page - 1)*ITEMS_PER_PAGE;
    }
    $rows = array();
    $str = "";
    if ($front == 1)
    {
      $str = " AND faqActive = 1 ";
    }
    $query = "SELECT *
          FROM faq WHERE faqID > 0".$str." 
          ORDER BY faqSortOrder
          LIMIT ".$start.", ".ITEMS_PER_PAGE;
    $this->LoadFromSQL($query);
    for ($i=0;$i<$this->GetTotalCount($front);$i++) {
      if (isset($this->items[$i]) && isset($this->items[$i]['faqQuest']))
      {
        $this->items[$i]["Preview"] = CreatePreview($this->items[$i]['faqQuest']);
        $this->items[$i]["faqQuest"] = str_replace("\n", '<br>', $this->items[$i]["faqQuest"]);
      }
      if (isset($this->items[$i]) && isset($this->items[$i]['faqtAnswer']))
      {
        $this->items[$i]["Preview1"] = CreatePreview($this->items[$i]['questAnswer']);
        $this->items[$i]["faqAnswer"] = str_replace("\n", '<br>', $this->items[$i]["faqAnswer"]);
      }
    }
  }


  function LoadFromDataBaseForCustomer($front=1)
  {
    $query = "SELECT *
          FROM faq WHERE faqID > 0 AND faqActive = 1
          ORDER BY faqSortOrder";
    $this->LoadFromSQL($query);
    for ($i=0;$i<$this->GetTotalCount($front);$i++) {
      if (isset($this->items[$i]) && isset($this->items[$i]['faqQuest']))
      {
        $this->items[$i]["Preview"] = CreatePreview($this->items[$i]['faqQuest']);
      }
    }
  }

  function GetTotalCount($front = 0)
  {
    $str = "";
    if ($front == 1)
    {
      $str = " AND faqActive = 1 ";
    }
    $query = "SELECT count(*) AS total FROM faq WHERE faqID > 0".$str;
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }

  function Delete($data)
  {
    if (is_array($data) && $data)
    {
      $ids = implode(",", Connection::GetSQLArray($data));
      if ($ids)
      {
        $query = "DELETE FROM faq WHERE faqID IN (".$ids.")";
        $stmt = GetStatement();
        $stmt->Execute($query);

      }
    }
  }

  function MoveField($faqID, $updown)
  {
    $faq = new Faq();
    $faq->SetProperty('faqID', $faqID);
    $faq->LoadFromDataBase();
    $row = null;
    if (is_numeric($updown) && is_numeric($faqID))
    {
      $stmt = GetStatement();
      if ($updown == 1)
      {
        $query = "SELECT * FROM faq
            WHERE faqSortOrder >= ".intval($faq->GetProperty('faqSortOrder'))."
            AND faqID<>".intval($faqID)."
            ORDER BY faqSortOrder ASC
            LIMIT 1";
        $row = $stmt->FetchRow($query);
      }
      elseif ($updown == -1)
      {
        $query = "SELECT * FROM faq
            WHERE faqSortOrder <= ".intval($faq->GetProperty('faqSortOrder'))."
            AND faqID<>".intval($faqID)."
            ORDER BY faqSortOrder DESC
            LIMIT 1";
        $row = $stmt->FetchRow($query);
      }
      if ($row)
      {
        $query = "UPDATE faq SET
          faqSortOrder=".intval($row['faqSortOrder'])."
          WHERE faqID=".intval($faqID);
        $stmt->Execute($query);

        $query = "UPDATE faq SET
          faqSortOrder=".intval($faq->GetProperty('faqSortOrder'))."
          WHERE faqID=".intval($row['faqID']);
        $stmt->Execute($query);
      }
    }
  }

}
?>