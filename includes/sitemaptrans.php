<?php
require_once("functions.php");
class SitemapTrans extends LocalObject
{
	var $error = null;
	var $message = null;
	var $languageID;
	var $pageID;

	function SitemapTrans($pageID, $langID)
	{
		$this->languageID = $langID;
		$this->pageID = $pageID;
	}

	function LoadFromDataBase()
	{
		if ($this->languageID && $this->pageID)
		{
			$query = "SELECT *, 1 AS Translated
				FROM sitemap_trans
				WHERE PageID = ".intval($this->pageID)."
				AND LanguageID=".intval($this->languageID);
			$this->LoadFromSQL($query);
			if ($this->CountProperties() == 0)
			{
				$query = "SELECT *, 0 AS Translated
						FROM language AS l
						LEFT  JOIN sitemap_trans AS t ON l.LanguageID = t.LanguageID
						WHERE t.PageID=".intval($this->pageID)."
						AND l.LanguageID<>".intval($this->languageID)."
						AND t.PageTransID IS NOT NULL
						ORDER  BY l.SortOrder
						LIMIT 1";
				$this->LoadFromSQL($query);
			}
		}
	}

	function GetPageName()
	{
		if ($this->languageID && $this->pageID)
		{
			$query = "SELECT PageName, 1 AS Translated
				FROM sitemap_trans
				WHERE PageID = ".intval($this->pageID)."
				AND LanguageID=".intval($this->languageID);
			$this->LoadFromSQL($query);
			if ($this->CountProperties() == 0)
			{
				$query = "SELECT PageName, 0 AS Translated
						FROM language AS l
						LEFT  JOIN sitemap_trans AS t ON l.LanguageID = t.LanguageID
						WHERE t.PageID=".intval($this->pageID)."
						AND l.LanguageID<>".intval($this->languageID)."
						AND t.PageTransID IS NOT NULL
						ORDER  BY l.SortOrder
						LIMIT 1";
				$this->LoadFromSQL($query);
			}
			return $this->GetProperty("PageName");
		}
	}


	function CheckAddInformation()
	{
		return $this->CheckInformation();
	}

	function CheckUpdateInformation()
	{
		return $this->CheckInformation();
	}

	function CheckInformation()
	{
		if ($this->GetProperty("PageName") == null)
		{
			return "Input Page Name ";
		}
		/*elseif ($this->GetProperty("PageContent") == null)
		{
			return "Input Page Content";
		} */
		return null;
	}

	//add/edit
	function Update()
	{
		$str = "LanguageID=".$this->languageID.",".
				"PageID=".$this->pageID.",".
				"PageName=".$this->GetPropertyForSQL("PageName").",".
				"PageContent=".$this->GetPropertyForSQL("PageContent");

		if ($this->GetProperty("PageID") && $this->GetProperty("LanguageID") == $this->languageID)
		{
			$query = "UPDATE sitemap_trans SET ".$str."
				WHERE PageID=".$this->GetPropertyForSQL("PageID")."
				AND LanguageID=".$this->languageID;
		}
		else
		{
			$query = "INSERT INTO sitemap_trans SET ".$str;
		}
		$stmt = GetStatement();
		$stmt->Execute($query);
	}
}
?>