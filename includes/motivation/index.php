<?php
if (!$user) {
    $tpl = new Template("motivation/login.html");
    $tpl->SetVar("url", parse_url($url, PHP_URL_PATH));
    break;
}
$stmt = GetStatement();
$query = "SELECT * FROM motivated_users WHERE guid = '".$_SESSION['guid']."' LIMIT 1";
$result = $stmt->FetchRow($query);

if($layout == 'otkaz'){
    if(empty($result['aborted_at'])){
        $tpl = new Template("motivation/otkaz.html");
        if(!empty($result)){
            $tpl->SetVar("logged", '1');
            $tpl = checkBonus($tpl);
        }
    }else{
        $tpl = new Template("motivation/otkaz-text.html");
    }
}elseif(empty($result)){
    $tpl = new Template("motivation/pravila.html");
}elseif(!empty($result['aborted_at'])){
    $tpl = new Template("motivation/otkaz-text.html");
}elseif(empty($result['approved_at'])){
    $tpl = new Template("motivation/moderation.html");
}elseif($layout == 'pravila'){
    $tpl = new Template("motivation/pravila.html");
    $tpl = checkBonus($tpl);
    $tpl->SetVar("logged", '1');
    $tpl->SetVar("fio_text", $result['fio']);
    $tpl->SetVar("filelink", HTTP_URL.'agreement/'.$result['file']);
}elseif($layout == 'default'){
    $logOld = array();
    $stmt = GetStatement();
    $query = "SELECT * FROM motivation_data WHERE guid = '".$_SESSION['guid']."' AND god_tek = '".(MOT_YEAR-1)."' AND prirost_fakt > '14' LIMIT 1";
    $logOld = $stmt->FetchRow($query);
    $stmt = GetStatement();
    if (MOT_CURRENT_DATE >= MOT_BONUS_DATE_BEGIN_YEAR && MOT_CURRENT_DATE < MOT_BONUS_DATE_BEGIN){
        $query = "SELECT * FROM motivation_data WHERE guid = '".$_SESSION['guid']."' AND god_tek = '".(MOT_YEAR-1)."' LIMIT 1";
    }else{
        $query = "SELECT * FROM motivation_data WHERE guid = '".$_SESSION['guid']."' AND god_tek = '".MOT_YEAR."' LIMIT 1";
    }
    $log = $stmt->FetchRow($query);
    error_reporting(E_ALL ^ E_DEPRECATED);
    //�������� ���� 
    $curDate = date_create(MOT_MONTH.'/'.MOT_DAY.'/'.MOT_YEAR);
    $tempDate = explode("/", MOT_END_DATE);
    $endDate = date_create($tempDate[1].'/'.$tempDate[0].'/'.$tempDate[2]);
    $interval = date_diff($curDate, $endDate);
    $dayToEndOfYear = str_split($interval->format('%a'));
    //!�������� ����
    require_once("templates/motivation/default.php");
    exit();
}elseif($layout == 'poluchit-bonus'){
    $stmt = GetStatement();
    $query = "SELECT * FROM motivation_data WHERE guid = '".$_SESSION['guid']."' AND god_tek = '".(MOT_YEAR-1)."' AND bonus_date IS NULL AND prirost_fakt > '14' LIMIT 1";
    $logOld = $stmt->FetchRow($query);
    $stmt = GetStatement();
    if((MOT_CURRENT_DATE >= MOT_BONUS_DATE_BEGIN) && (MOT_CURRENT_DATE <= MOT_BONUS_DATE_END) && !empty($logOld)){
        $query = "SELECT bonus_fakt FROM motivation_data WHERE guid = '".$_SESSION['guid']."' AND god_tek = '".(MOT_YEAR-1)."' LIMIT 1";
        $curYearLog = $stmt->FetchList($query);
        $tpl = new Template("motivation/poluchit-bonus.html");
        $tpl->SetVar("bonus_fakt", number_format($curYearLog[0]['bonus_fakt'], 0, ',', ' '));
        $tpl->SetVar("god_tek", MOT_YEAR);
    }else{
        $tpl = new Template("motivation/bonus-attempt-double.html");
    }
}elseif($layout == 'dostijeniya'){
    $stmt = GetStatement();
    $query = "SELECT * FROM motivation_data WHERE guid = '".$_SESSION['guid']."' AND god_tek = '".(MOT_YEAR-1)."' AND bonus_date IS NULL AND prirost_fakt > '14' LIMIT 1";
    $logOld = $stmt->FetchRow($query);
    $stmt = GetStatement();
    $query = "SELECT * FROM motivation_data WHERE guid = '".$_SESSION['guid']."' AND god_tek = '".(MOT_YEAR-1)."' AND data_vrucheniya IS NULL LIMIT 1";
    $curYearLog = $stmt->FetchRow($query);
    $stmt = GetStatement();
    $query = "SELECT * FROM motivation_data WHERE guid = '".$_SESSION['guid']."' AND data_vrucheniya IS NOT NULL AND bonus_date IS NOT NULL ORDER BY god_tek ASC";
    $bonusLog = $stmt->FetchList($query);
    require_once("templates/motivation/dostijeniya.php");
    exit();
}elseif($layout == 'moderation'){
    $tpl = new Template("motivation/pravila.html");
    $tpl->SetVar("fio_text", $result['fio']);
    $tpl->SetVar("filelink", HTTP_URL.'agreement/'.$result['file']);
}elseif (file_exists($_SERVER["DOCUMENT_ROOT"]."/templates/motivation/".$layout.".html")){
    $tpl = new Template("motivation/".$layout.".html");
} else {
    $tpl = new Template("motivation/404.html");
}
if(isset($_SESSION['errors'])){
    $errors = $_SESSION['errors'];
    $data = $_SESSION['data'];
    $tpl->LoadFromArrayWithLoop($errors);
    $tpl->LoadFromArray($data);
    unset($_SESSION['data']);
    unset($_SESSION['errors']);
    
}
$tpl->pparse();

exit;