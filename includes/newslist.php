<?php
require_once("localobjectlist.php");
require_once("functions.php");
//require_once("news.php");

class NewsList extends LocalObjectList
{
  var $message = "";
  function LoadFromDataBase($page = 1, $hot = 0, $left = 0, $front = 0)
  {
    $start = 0;
    if ($page > 0)
    {
      $start = ($page - 1)*ITEMS_PER_PAGE;
    }
    $rows = array();
    global $corporate;
    $str = "";
    $top = ITEMS_PER_PAGE;
    if ($hot == 1)
    {
      $top = 5;
      $str = " AND NewsActive = 1 AND NewsCenter = 1";
    }
    if ($left == 1)
    {
      $top = 5;
      $str = " AND NewsActive = 1 AND NewsRight = 1";
    }
    if ($front == 1)
    {
      $str = " AND NewsActive = 1 ";
    }
    $query = "SELECT *, DATE_FORMAT(NewsDate, '".USER_DATE_FORMAT."') AS NewsDate1
          FROM news WHERE NewsCorporate=".$corporate.$str."
          ORDER BY NewsDate DESC
          LIMIT ".$start.", ".$top;
    $this->LoadFromSQL($query);
    //echo($query);
    //exit;
    for ($i=0;$i<$this->GetTotalCount();$i++) {
      if (isset($this->items[$i]) && isset($this->items[$i]['NewsContent']))
      {
        $this->items[$i]["Preview"] = CreatePreview($this->items[$i]['NewsContent']);
      }
    }
  }

  function LoadNewsList($selected = 0)
  {
    global $corporate;
    $rows = array();
    $query = "SELECT *,
        IF (NewsID= '".mysql_escape_string($selected)."', 1, 0) AS selected, DATE_FORMAT(NewsDate, '".USER_DATE_FORMAT."') AS NewsDate1
          FROM news WHERE NewsCorporate=".$corporate."
          ORDER BY NewsDate DESC";
    $this->LoadFromSQL($query);
    for ($i=0;$i<$this->GetTotalCount();$i++) {
      if (isset($this->items[$i]) && isset($this->items[$i]['NewsContent']))
      {
        $this->items[$i]["Preview"] = CreatePreview($this->items[$i]['NewsContent']);
      }
    }

  }


  function GetTotalCount($front = 0)
  {
    global $corporate;
    $str = "";
    if ($front == 1)
    {
      $str = " AND NewsActive = 1 ";
    }
    $query = "SELECT count(*) AS total FROM news WHERE NewsCorporate=".$corporate.$str;;
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }

  function Delete($data)
  {
    if (is_array($data) && $data)
    {
      $ids = implode(",", Connection::GetSQLArray($data));
      if ($ids)
      {
        $query = "DELETE FROM news WHERE NewsID IN (".$ids.")";
        $stmt = GetStatement();
        $stmt->Execute($query);

      }
    }
  }

	function MoveField($newsID, $updown)
	{
    global $corporate;
		$news = new News();
		$news->SetProperty('NewsID', $newsID);
		$news->LoadFromDataBase();
		$row = null;
		if (is_numeric($updown) && is_numeric($newsID))
		{
			$stmt = GetStatement();
			if ($updown == 1)
			{
				$query = "SELECT * FROM news
						WHERE NewsSortOrder >= ".intval($news->GetProperty('NewsSortOrder'))."
						AND NewsID<>".intval($newsID)." AND NewsCorporate=".$corporate."
						ORDER BY NewsSortOrder ASC
						LIMIT 1";
				$row = $stmt->FetchRow($query);
			}
			elseif ($updown == -1)
			{
				$query = "SELECT * FROM news
						WHERE NewsSortOrder <= ".intval($news->GetProperty('NewsSortOrder'))."
						AND NewsID<>".intval($newsID)." AND NewsCorporate=".$corporate."
						ORDER BY NewsSortOrder DESC
						LIMIT 1";
				$row = $stmt->FetchRow($query);
			}
			if ($row)
			{
				$query = "UPDATE news SET
					NewsSortOrder=".intval($row['NewsSortOrder'])."
					WHERE NewsID=".intval($newsID);
				$stmt->Execute($query);

				$query = "UPDATE news SET
					NewsSortOrder=".intval($news->GetProperty('NewsSortOrder'))."
					WHERE NewsID=".intval($row['newsID']);
				$stmt->Execute($query);
			}
		}
	}

}
?>