<?php

require_once("object.php");

class LocalObject extends Object
{

	// Constructor
	function LocalObject($data = array())
	{
		if (is_array($data))
		{
			$this->Object($data);
		}
		else
		{
			$this->Object($data, GetStatement());
		}
	}

	// LoadFromSQL
	function LoadFromSQL($query)
	{
		Object::LoadFromSQL($query, GetStatement());
	}

	function ValidateInt($name)
	{
		return preg_match("/^[0-9]+$/", $this->GetProperty($name)) ;
	}

	function ValidateFloat($name)
	{
		if (preg_match("/^[0-9]+$/", $this->GetProperty($name)))
		{
			return true;
		}
		elseif (preg_match("/^[0-9]+\.[0-9]+$/", $this->GetProperty($name)))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function ValidateWord($name)
	{
		return preg_match("/^[A-Za-z\s0-9]+$/", $this->GetProperty($name));
	}

	function ValidateEmail($name)
	{
		return preg_match("/^([A-Za-z0-9_\-]+\.)*[A-Za-z0-9_\-]+@([A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9]\.)+[a-z]{2,4}$/", $this->GetProperty($name)) ;
	}

	function ValidateYear($name)
	{
		return preg_match("/^[1-2][0-9]{3}$/", $this->GetProperty($name));
	}

	function ValidateDate($name)
	{
		$data = explode("-", $this->GetProperty($name));
		if (is_array($data) && count($data) == 3 
		&& is_numeric($data[0]) && strlen($data[0]) == 4 
		&& is_numeric($data[1]) && strlen($data[1]) <= 2 
		&& is_numeric($data[2]) && strlen($data[2]) <= 2 )
		{
			return checkdate($data[1], $data[2], $data[0]);
		}
		return false;
	}

	function LoadFromArray($data)
	{
		if (is_array($this->properties) && is_array($data))
		{
			$this->properties = array_merge($this->properties, $data);
		}
		else
		{
			$this->properties = $data;
		}
	}

	function IsEmpty()
	{
		$properties = $this->GetAllProperties();
		return empty($properties);
	}

	// UnSetProperty
	function UnsetProperty($name)
	{
		unset($this->properties[$name]);
	}

}
?>