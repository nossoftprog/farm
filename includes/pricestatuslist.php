<?php
require_once("localobjectlist.php");
require_once("pricestatus.php");

class PriceStatusList extends LocalObjectList
{
	var $message = "";

	function LoadFromDataBase($selected = 0)
	{
		$query = "SELECT *, IF(PriceStatID = '".mysql_escape_string($selected)."', 1, 0) AS selected_p
  				FROM price_status ORDER BY PriceStatTemp, PriceStatID";
		$this->LoadFromSQL($query);
  }

	function LoadFromDataBase1($selected = 0)
	{
		$query = "SELECT *, IF(PriceStatID = '".mysql_escape_string($selected)."', 1, 0) AS selected_p
  				FROM price_status where PriceStatTemp != 1 ORDER BY PriceStatTemp, PriceStatID";
		$this->LoadFromSQL($query);
  }

	function Delete($data)
	{
		if (is_array($data) && $data)
		{
			$ids = implode(",", Connection::GetSQLArray($data));
			if ($ids)
			{

				$query = "DELETE FROM price_status WHERE PriceStatID IN (".$ids.")";
        $stmt = GetStatement();
				$stmt->Execute($query);

			}
		}
	}

  function GetTotalCount($front = 0)
  {
    $str = "";
    $query = "SELECT count(*) AS total FROM price_status";
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }
}
?>