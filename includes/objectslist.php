<?php
class ObjectsList
{

	// Members
	var $items, $counter;

	// Constructor
	function ObjectsList($data = array(), $statement = null)
	{
		if (is_array($data))
		{
			$this->LoadFromArray($data);
		}
		else
		{
			$this->LoadFromSQL($data, $statement);
		}
		$this->counter = 0;
	}

	// LoadFromArray
	function LoadFromArray($data)
	{
		$this->items = array_values($data);
	}

	// LoadFromSQL
	function LoadFromSQL($query, $statement)
	{
		$this->items = $statement->FetchList($query);
	}

	// GetItemsArray
	function GetItemsArray()
	{
		return $this->items;
	}

	// CountItems
	function CountItems()
	{
		return count($this->items);
	}

	// FirstItem
	function FirstItem()
	{
		$this->counter = 0;
	}

	// NextItem
	function NextItem()
	{
		if (isset($this->items[$this->counter]))
		{
			return $this->CreateItem($this->items[$this->counter++]);
		}
		return null;
	}

	// CreateItem
	function CreateItem($data)
	{
		echo("\nDefine CreateItem in child class to use iteration over items!\n");
		return null;
	}
	
	function Items($index)
	{
		if (isset($this->items[$index]))
		{
			return $this->CreateItem($this->items[$index]);
		}
		return null;
	}

}
?>