<?php
require_once("localobjectlist.php");
require_once("image.php");
require_once("metod.php");

class MetodList extends LocalObjectList
{
	var $message = "";

	function LoadFromDataBase($selected = 0)
	{
		$query = "SELECT *, IF(metID = '".mysql_escape_string($selected)."', 1, 0) AS selected
  				FROM metod ORDER BY metName";
		$this->LoadFromSQL($query);
    $count_ = $this->GetTotalCount();
    $count_ = floor($count_/2.8);
    $curr_head = "";
    $temp_head = "";
    for ($i=0;$i<$this->GetTotalCount();$i++) {
       $temp_head = $this->items[$i]["metName"];
       $temp_head = substr($temp_head, 0, 1);
       if ($temp_head == $curr_head) {
         $this->items[$i]["letter"] = 0;
         $this->items[$i]["letters"] = "";
       } else {
         $curr_head = $temp_head;
         $this->items[$i]["letter"] = 1;
         $this->items[$i]["letters"] = $curr_head;
       }
       if ($i == $count_+3 || $i == $count_*2-3) {
        $this->items[$i]["third"] = 1;
       } else {
        $this->items[$i]["third"] = 0;
       }
    }
  }

	function Delete($data)
	{
		if (is_array($data) && $data)
		{
			$ids = implode(",", Connection::GetSQLArray($data));
			if ($ids)
			{

				$query = "DELETE FROM metod WHERE metID IN (".$ids.")";
        $stmt = GetStatement();
				$stmt->Execute($query);

			}
		}
	}

  function GetTotalCount($front = 0)
  {
    $str = "";
    if ($front == 1)
    {
      $str = " AND metActive = 1 ";
    }
    $query = "SELECT count(*) AS total FROM metod";
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }
}
?>