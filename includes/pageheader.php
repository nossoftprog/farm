<?php
require_once("template.php");
require_once("newslist.php");
require_once("invoicelist.php");

class PageHeader extends Template
{
	// Constructor
  function PageHeader($title, $user = "", $page = "", $pageTitle = "", $keywords__ = "", $statName__ = "")
  {
	global $nameIcons;
  $request = new LocalObject($_REQUEST);
	if ($user)
	{
		$this->Template("_a_header.html");
		$this->setVar("Title", $title);
		$this->SetVar('UserName', $user->GetProperty('login'));
		$this->SetVar("ImagesDir", HTTP_ADMIN_IMAGES);
		$this->SetVar("JsDir", HTTP_ADMIN_JS);
		$this->SetVar("StylesDir", HTTP_ADMIN_STYLES);
		$this->SetVar("level".$_SESSION['userType'], 1);
	}
	else
	{
		$this->Template("_header".$request->GetProperty('lang').".html");
		if ($page == "main")
		{
			$this->setVar("Main", 1);
		}
		$this->setVar("Title", $title);
		$this->setVar("statName", $statName__);
		$this->setVar("PageTitle", $pageTitle);
		$this->SetVar("ImagesDir", HTTP_URL_IMAGES);
		$this->SetVar("JsDir", HTTP_URL_JS);
		$this->SetVar("StylesDir", HTTP_URL_STYLES);
    $this->SetVar("KeyStandard1", PROJECT_KEYWORDS);
    $this->SetVar("KeyStandard", PROJECT_NAME);
    $this->SetVar("KeyDynamic", $keywords__);

		$request = new LocalObject($_REQUEST);
    $newcount=0;

    if (isset($_SESSION['userName'])) {
      $invoicelist = new InvoiceList();
      $newcount = $invoicelist->GetTotalCountNew();
      $this->SetVar("user_", $_SESSION['userName']);
    } elseif(isset($_POST['loginf'])) {
      $this->SetVar("message_", "�������� ������");
    }

	}

    $newslist = new NewsList();
    $newslist->LoadFromDataBase(1, 0, 1, 0);
    $this->LoadFromObjectsList("newslistright", $newslist);

    $this->SetVar("SiteDir", HTTP_URL);
    $this->SetVar("ProjectName", PROJECT_NAME);
    $this->SetVar("UrlDezSred", HTTP_DEZ_SRED);
    $this->SetVar("newcount", $newcount);
  }
}
?>