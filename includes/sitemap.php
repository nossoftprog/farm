<?php
require_once("translation.php");
require_once("sitemaptrans.php");

class Sitemap extends LocalObject
{
	var $error = null;
	var $message = null;
	var $currLang;

	function Sitemap()
	{
		$trans = new Translation();
		$this->currLang = $trans->language;
	}

	function LoadFromDataBase()
	{
		if ($this->GetProperty("PageID"))
		{
			$query = "SELECT *
				FROM sitemap
				WHERE PageID = ".$this->GetPropertyForSQL("PageID");
			$this->LoadFromSQL($query);
			$transFields = new SitemapTrans($this->GetProperty("PageID"), $this->currLang);
			$transFields->LoadFromDataBase();
			$this->LoadFromArray($transFields->GetAllProperties());
		}
	}

	function HasParent($parentID)
	{
		$query = "SELECT ParentID FROM sitemap WHERE PageID=".intval($parentID);
		$stmt = GetStatement();
		$p = $stmt->FetchField($query, "ParentID");
		if ($p)
		{
			return true;
		}
		return false;
	}

	function IsLeftMenuLevel($parentID)
	{
		$query = "SELECT * FROM sitemap
			WHERE ParentID=".intval($parentID)." AND UseInLeftMenu='Y'
			ORDER BY SortOrder";
		$stmt = GetStatement();
		$rows = $stmt->FetchList($query);
		return $rows;
	}

	function GetLeftMenuItemsTranslated($data)
	{
		$menu = $this->GetLeftMenuItems($data);
		$i = 0;
		$rows = array();
		if ($menu)
		{
			foreach ($menu as $k => $v)
			{
				$transFields = new SitemapTrans(intval($v["PageID"]), $this->currLang);
				$rows[$i] = $v;
				$rows[$i]["PageName"] = $transFields->GetPageName();
				if ($data["PageID"] == $v["PageID"])
				{
					$rows[$i]["selected"] = 1;
				}
				$i++;
			}
		}
		return $rows;
	}

	function GetLeftMenuItems($data)
	{
		$stmt = GetStatement();
		if ($data['ParentID'] == 0)
		{
			$query = "SELECT * FROM sitemap
				WHERE ParentID=".intval($data["PageID"])." AND UseInLeftMenu='Y'
				ORDER BY SortOrder";
			$menu = $stmt->FetchList($query);
			return $menu;
		}
		else
		{
			if ($menu = $this->IsLeftMenuLevel($data['ParentID']))
			{
				return $menu;
			}
			else
			{
				$query = "SELECT * FROM sitemap
					WHERE PageID=".intval($data['ParentID'])."
					ORDER BY SortOrder";
				$newData = $stmt->FetchList($query);
				$menu = Sitemap::GetLeftMenuItems($newData[0]);
				return $menu;
			}
		}
	}

	function GetPageChildren($pageID)
	{
		$query = "SELECT * FROM sitemap WHERE ParentID=".intval($pageID)." ORDER BY SortOrder";
		$stmt = GetStatement();
		return $stmt->FetchList($query);
	}

	function GetPageProperties($pageID, $lang)
	{
		$rows = array();
		$query = "SELECT *
			FROM sitemap
			WHERE PageID = ".intval($pageID);
		$stmt = GetStatement();
		$rows = $stmt->FetchList($query);
		$transFields = new SitemapTrans(intval($pageID), intval($lang));
		$transFields->LoadFromDataBase();
		return array_merge(isset($rows[0]) ? $rows[0] : array(), $transFields->GetAllProperties());
	}

	function GetPageHTMLFck($pageID, $lang)
	{
		if (!$children = Sitemap::GetPageChildren($pageID))
		{
			$tpl = new Template("single_page_fck.html");
			$r = Sitemap::GetPageProperties($pageID, $lang);
			$tpl->LoadFromArray($r);
			$tpl->SetVar("ImagesDir", HTTP_ADMIN_IMAGES);
			return $tpl->grab();
		}
		else
		{
			$tmp_array = array();
			foreach($children as $page)
			{
				$h = Sitemap::GetPageHTMLFck($page["PageID"], $lang);
				array_push($tmp_array, $h);
			}
			$tpl = new Template("page_subpage_fck.html");
			$tpl->SetVar("SubPage", implode("\n", $tmp_array));
			$r = Sitemap::GetPageProperties($pageID, $lang);
			$tpl->LoadFromArray($r);

			$tpl->SetVar("ImagesDir", HTTP_ADMIN_IMAGES);
			return $tpl->grab();
		}
	}


	function GetPageHTML($pageID, $lang)
	{
		if (!$children = Sitemap::GetPageChildren($pageID))
		{
			$tpl = new Template("single_page.html");
			$r = Sitemap::GetPageProperties($pageID, $lang);
			$tpl->LoadFromArray($r);
			$tpl->SetVar("ImagesDir", HTTP_ADMIN_IMAGES);
			return $tpl->grab();
		}
		else
		{
			$tmp_array = array();
			foreach($children as $page)
			{
				$h = Sitemap::GetPageHTML($page["PageID"], $lang);
				array_push($tmp_array, $h);
			}
			$tpl = new Template("page_subpage.html");
			$tpl->SetVar("SubPage", implode("\n", $tmp_array));
			$r = Sitemap::GetPageProperties($pageID, $lang);
			$tpl->LoadFromArray($r);

			$tpl->SetVar("ImagesDir", HTTP_ADMIN_IMAGES);
			return $tpl->grab();
		}
	}

	function CheckAddInformation()
	{
		return $this->CheckInformation();
	}

	function CheckUpdateInformation()
	{
		return $this->CheckInformation();
	}

	function CheckInformation()
	{
		if ($this->GetProperty("PageType") == null)
		{
			return "Select Page Type";
		}
		$transFields = new SitemapTrans($this->GetProperty("PageID"), $this->currLang);
		$transFields->LoadFromArray($this->GetAllProperties());
		if ($error = $transFields->CheckInformation())
		{
			return $error;
		}
		return null;
	}

	//add/edit
	function Update()
	{
		$stmt = GetStatement();
		if ($this->GetProperty('IndexPage'))
		{
			$isIndex = 'Y';
			$query = "UPDATE sitemap SET IndexPage='N'";
			$stmt->Execute($query);
		}
		else
		{
			$isIndex = 'N';
		}

		$str = "UseInLeftMenu=".$this->GetPropertyForSQL("UseInLeftMenu").",".
				"UseInTopMenu=".$this->GetPropertyForSQL("UseInTopMenu").",".
				"ParentID=".$this->GetPropertyForSQL("ParentID").",".
				"IndexPage='".$isIndex."',".
				"PageType=".$this->GetPropertyForSQL("PageType");
		if ($this->GetProperty("PageID"))
		{
			$query = "UPDATE sitemap SET ".$str." WHERE PageID=".$this->GetPropertyForSQL("PageID");
		}
		else
		{
			$query = "INSERT INTO sitemap SET ".$str;
		}

		$stmt->Execute($query);
		if ($id = $stmt->GetLastInsertID())
		{
			$this->SetProperty('PageID', $id);
			$query = "UPDATE sitemap SET SortOrder=PageID WHERE PageID=".$this->GetPropertyForSQL("PageID");
			$stmt->Execute($query);
		}

		$transFields = new SitemapTrans($this->GetProperty("PageID"), $this->currLang);
		$transFields->LoadFromArray($this->GetAllProperties());
		$transFields->Update();
	}

	function GetIndexPage()
	{
		$query = "SELECT *
				FROM sitemap
				WHERE IndexPage = 'Y' LIMIT 1";
		$this->LoadFromSQL($query);
		$transFields = new SitemapTrans($this->GetProperty("PageID"), $this->currLang);
		$transFields->LoadFromDataBase();
		$this->LoadFromArray($transFields->GetAllProperties());
	}
}
?>