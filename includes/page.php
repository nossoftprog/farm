<?php
require_once("pageheader.php");
require_once("pagefooter.php");

class Page
{
	var	$content, $header, $footer;

	function Page($content,	$header, $footer)
	{

    $this->content = $content;
		$this->header =	$header;
		$this->footer =	$footer;
	}

	function Output()
	{
		$this->header ?	$this->header->pparse()	: "";
		$this->content->pparse();
		$this->footer ?	$this->footer->pparse()	: "";
	}

	function CreateForAdmin($content, $user, $title	= "")
	{
		$content->SetVar('UserName', $user->GetProperty('login'));
		$content->SetVar("ImagesDir", HTTP_ADMIN_IMAGES);
		$content->SetVar("JsDir", HTTP_ADMIN_JS);
		$content->SetVar("StylesDir", HTTP_ADMIN_STYLES);
		$content->SetVar("SiteDir",	HTTP_URL);
		$content->SetVar("ProjectName", PROJECT_NAME);
		$content->SetVar("FilesDir", FILES_DIR);
		$content->SetVar("FilesHttpDir", FILES_HTTP_DIR);
		if ($title)
		{
			return new Page($content, new PageHeader($title, $user), new PageFooter($user));
		}
		return new Page($content, null,	null);
	}

	function CreateForCustomer($content, $title, $pageType, $pageTitle, $keywords_="", $statName = "")
	{
		$content->SetVar("ImagesDir", HTTP_URL_IMAGES);
		$content->SetVar("JsDir", HTTP_URL_JS);
		$content->SetVar("StylesDir", HTTP_URL_STYLES);
		$content->SetVar("SiteDir",	HTTP_URL);
		$content->SetVar("FilesDir", FILES_DIR);
		$content->SetVar("FilesHttpDir", FILES_HTTP_DIR);

		if ($title)
		{
			return new Page($content, new PageHeader($title, "", $pageType, $pageTitle, $keywords_, $statName), new PageFooter("", $pageType));
		}
		return new Page($content, null,	null);
	}

	function GetPageTemplate($pageObj)
	{
		global $icons, $nameIcons;
		$request = new LocalObject($_REQUEST);
		$page = $request->GetProperty('page') ? $request->GetProperty('page') : 1;
		if ($type = $pageObj->GetProperty('PageType'))
		{
			switch ($type)
			{
				case "news":
					$tpl = new Template("news.html");
					$news = new NewsList();
					$news->LoadFromDataBase($page);
					$tpl->LoadFromObjectsList("newslist", $news);
					$tpl->SetVar("URL", "page".$pageObj->GetProperty('PageID'));
					//paging
					$paging  = new Paging($page, $news->GetTotalCount(), $tpl);
				break;
				case "advanced_search":
					$tpl = new Template("advanced_search.html");
					$regions = new RegionList();
					$regions->LoadRegionsList($request->GetProperty('RegionID'));
					$tpl->LoadFromObjectsList("regions", $regions);
					//accommodation types
					$accommodationtypes = new AccommodationTypeList();
					$accommodationtypes->LoadAccommodationTypes($request->GetProperty('AccommodationTypeID'));
					$tpl->LoadFromObjectsList("accommodationtypes", $accommodationtypes);
				break;

        default:
					$tpl = new Template("static_page.html");
				break;
			}
			$tpl->LoadFromObject($pageObj);
			return $tpl;
		}
		return null;
	}
}
?>