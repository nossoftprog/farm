<?php
require_once("localobjectlist.php");

class OrderList extends LocalObjectList
{
	var $message = "";

	function LoadFromDataBase($all = 1, $page = 1)
	{
    $start = 0;
    if ($page > 0)
    {
      $start = ($page - 1)*ITEMS_PER_PAGE;
    }
    $str = "";
    if ($all == 1)
    {
      $str = " AND OrderStatus <> 0 ";
    }
  
    if ($all == 0)
    {
      $str = " AND OrderStatus = 0 ";
    }

    if ($_SESSION['userType'] != 1)
    {
      $str .= " AND users_.UserAdminID=".$_SESSION['userId']." ";
    }


		$query = "SELECT *, DATE_FORMAT(orders_.OrderDateCreated, '".USER_DATE_FORMAT."') AS Date1, FORMAT(OrderSum,2) as OrderSum1, IF(orders_.OrderStatus = 1, '1', '') as stat1, 
          IF(orders_.OrderStatus = 2, '1', '') as stat2, IF(orders_.OrderStatus = 3, '1', '') as stat3
  				FROM ( orders_, users_ ) LEFT JOIN admins on admins.AdminCoflexID=users_.UserAdminID WHERE orders_.OrderUserID=users_.UserID ".$str." 
					ORDER BY "  . $_SESSION['OrderSort'] . " LIMIT ".$start.", ".ITEMS_PER_PAGE;
		$this->LoadFromSQL($query);
	}


  function LoadFromDataBaseForCustomer($page = 1)
	{
    $start = 0;
    if ($page > 0)
    {
      $start = ($page - 1)*ITEMS_PER_PAGE_USER;
    }

		$query = "SELECT DISTINCT orders_.*, users_.*, users_address.CompanyName, DATE_FORMAT(orders_.OrderDateCreated, '".USER_DATE_FORMAT."') AS Date1, FORMAT(OrderSum,2) as OrderSum1, IF(orders_.OrderStatus = 1, '1', '') as stat1, 
          IF(orders_.OrderStatus = 2, '1', '') as stat2, IF(orders_.OrderStatus = 3, '1', '') as stat3
  				FROM ( orders_, users_ ) LEFT JOIN users_address ON ( users_address.CompanyCoflexID=SUBSTRING_INDEX(orders_.OrderAddressCoflexID, '_', 1) AND users_address.AddressCoflexID=SUBSTRING_INDEX(orders_.OrderAddressCoflexID, '_', -1) ) WHERE orders_.OrderUserID=users_.UserID AND users_.UserName='" .$_SESSION['userName'] ."' AND OrderStatus <> 0 AND OrderDateCreated > '" . ORDERS_SHOW_DATE . "' ORDER BY orders_.OrderDateCreated DESC LIMIT ".$start.", 200"; //".ITEMS_PER_PAGE_USER;
          //echo $query;
          //exit;

		$this->LoadFromSQL($query);
	}


	function Delete($data)
	{
		if (is_array($data) && $data)
		{
			$ids = implode(",", Connection::GetSQLArray($data));
			if ($ids)
			{
				$stmt = GetStatement();
				$query = "DELETE FROM orders_ WHERE OrderID IN (".$ids.")";
				$stmt->Execute($query);
				$stmt = GetStatement();
				$query = "DELETE FROM history_ WHERE HistOrderID IN (".$ids.")";
				$stmt->Execute($query);
			}
		}
	}

  function GetTotalCount($all = 0)
  {
    $str = "";
    if ($all == 1)
    {
      $str = " AND OrderStatus <> 0 ";
    }
  
    if ($all == 0)
    {
      $str = " AND OrderStatus = 0 ";
    }

    if ($_SESSION['userType'] != 1)
    {
      $str .= " AND OrderAdminID=".$_SESSION['userId']." ";
    }


		$query = "SELECT count(*) AS total 
  				FROM ( orders_, users_ ) LEFT JOIN admins on admins.AdminCoflexID=orders_.OrderAdminID WHERE orders_.OrderUserID=users_.UserID ".$str;
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }

  function GetTotalCountForCustomer()
  {
		$query = "SELECT count(*) AS total 
  				FROM ( orders_, users_ ) WHERE orders_.OrderUserID=users_.UserID AND users_.UserName='" .$_SESSION['userName'] ."' AND OrderStatus <> 0 AND OrderDateCreated > '" . ORDERS_SHOW_DATE."'";
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }


}
?>