<?php
require_once("localobjectlist.php");
require_once("functions.php");
require_once("sort.php");


class SortList extends LocalObjectList
{
  var $message = "";
  function LoadFromDataBase()
  {
    $query = "SELECT *
          FROM category WHERE CatParent =" . $_SESSION['CatID_'] . "
          AND CatHistory=1 ORDER BY CatIndex, CatID";
    $this->LoadFromSQL($query);

  }

  function MoveField($sortID, $updown)
  {
    $sort = new Sort_();
    $sort->SetProperty('sortID', $sortID);
    $sort->LoadFromDataBase();
    $row = null;
    if (is_numeric($updown) && is_numeric($sortID))
    {
      $stmt = GetStatement();
      if ($updown == 1)
      {
        $query = "SELECT * FROM category
            WHERE CatIndex >= ".intval($sort->GetProperty('CatIndex'))."
            AND CatID<>".intval($sortID)."
            ORDER BY CatIndex ASC
            LIMIT 1";
        $row = $stmt->FetchRow($query);
      }
      elseif ($updown == -1)
      {
        $query = "SELECT * FROM category
            WHERE CatIndex <= ".intval($sort->GetProperty('CatIndex'))."
            AND CatID<>".intval($sortID)."
            ORDER BY CatIndex DESC
            LIMIT 1";
        $row = $stmt->FetchRow($query);
      }
      if ($row)
      {
        $query = "UPDATE category SET
          CatIndex=".intval($row['CatIndex'])."
          WHERE CatID=".intval($sortID);
        $stmt->Execute($query);

        $query = "UPDATE category SET
          catIndex=".intval($sort->GetProperty('CatIndex'))."
          WHERE CatID=".intval($row['CatID']);
        $stmt->Execute($query);
      }
    }
  }

}
?>