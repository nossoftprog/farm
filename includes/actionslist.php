<?php
require_once("localobjectlist.php");
require_once("functions.php");

class ActionsList extends LocalObjectList
{
  var $message = "";
  function LoadFromDataBase($page = 1, $front = 0)
  {
    $start = 0;
    if ($page > 0)
    {
      $start = ($page - 1)*ITEMS_PER_PAGE;
    }
    $rows = array();
    $str = "";
    $top = ITEMS_PER_PAGE;

    if ($front == 1)
    {
      $str = " AND ActionActive = 1 ";
    }

    $query = "SELECT *, DATE_FORMAT(ActionDate, '".USER_DATE_FORMAT."') AS ActionDate1, DATE_FORMAT(ActionDateEnd, '".USER_DATE_FORMAT."') AS ActionDate2
          FROM actions WHERE ActionID > 0".$str."
          ORDER BY ActionDate DESC
          LIMIT ".$start.", ".$top;
    $this->LoadFromSQL($query);
    //echo($query);
    //exit;
    for ($i=0;$i<$this->GetTotalCount($front);$i++) {
      if (isset($this->items[$i]) && isset($this->items[$i]['ActionDesc']))
      {
        $this->items[$i]["Preview"] = CreatePreview($this->items[$i]['ActionDesc']);
      }
    }
  }

  function GetTotalCount($front = 0)
  {
    $str = "";
    if ($front == 1)
    {
      $str = " AND ActionActive = 1 ";
    }
    $query = "SELECT count(*) AS total FROM actions WHERE ActionID > 0";
    $stmt = GetStatement();
    return $stmt->FetchField($query, "total");
  }

  function Delete($data)
  {
    if (is_array($data) && $data)
    {
      $ids = implode(",", Connection::GetSQLArray($data));
      if ($ids)
      {
        $query = "DELETE FROM actions WHERE ActionID IN (".$ids.")";
        $stmt = GetStatement();
        $stmt->Execute($query);

      }
    }
  }

}
?>