<?php
class Object
{

	// Members
	var $properties;

	// Constructor
	function Object($data = array(), $statement = null)
	{
		if (is_array($data))
		{
			$this->LoadFromArray($data);
		}
		else if (!is_null($statement))
		{
			$this->LoadFromSQL($data, $statement);
		}
		else
		{
			$this->LoadFromArray(array());
		}
	}

	// LoadFromArray
	function LoadFromArray($data)
	{
		$this->properties = $data;
	}

	// LoadFromSQL
	function LoadFromSQL($query, $statement)
	{
		/* @var $statement Statement */
		$data = $statement->FetchRow($query);
		$this->properties = is_array($data) ? $data : array();
	}

	// SetProperty
	function SetProperty($name, $value)
	{
		$this->properties[$name] = $value;
	}

	// GetProperty
	function GetProperty($name)
	{
		return isset($this->properties[$name]) ? $this->properties[$name] : null;
	}

	// GetPropertyForSQL
	function GetPropertyForSQL($name)
	{
		return Connection::GetSQLString($this->GetProperty($name));
	}

	// GetPropertyForURL
	function GetPropertyForURL($name)
	{
		return urlencode($this->GetProperty($name));
	}

	// GetIntProperty
	function GetIntProperty($name)
	{
		return intval($this->GetProperty($name));
	}

	// GetAllProperties
	function GetAllProperties()
	{
		return $this->properties;
	}

	// CountProperties
	function CountProperties()
	{
		return count($this->properties);
	}

	// ValidateNotEmpty
	function ValidateNotEmpty($name)
	{
		return strlen($this->GetProperty($name)) > 0;
	}

	// ValidateEmail
	function ValidateEmail($name)
	{
		return preg_match("/^.+@.+$/", $this->GetProperty($name));
	}

}
?>