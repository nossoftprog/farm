<?php

function CreatePreview($data)
{
	//$clean = strip_tags($data);

	/* strip nonbreaking space, strip php tags, strip html tags,
	** convert html entites, strip extra white space */
	$trans = array_flip(get_html_translation_table(HTML_ENTITIES));
	$search_clean = array("%&nbsp;%i", "%<\?.*\?>%Usi", "%<[\/]*[^<>]*>%Usi", "%(\&[a-zA-Z0-9\#]+;)%es", "%\s+%");
	$replace_clean = array(" ", "", "", "strtr('\\1',\$trans)", " ");
	$clean = preg_replace($search_clean, $replace_clean, $data);

	if (strlen($clean) > PREVIEW_LENGHT)
	{
		$res = substr($clean, 0, PREVIEW_LENGHT)."...";
	}
	else
	{
		$res = $clean;
	}
	return $res;
}

function CreateTextDescription($data)
{
	$trans = array_flip(get_html_translation_table(HTML_ENTITIES));
	$search_clean = array("%&nbsp;%i", "%<\?.*\?>%Usi", "%<[\/]*[^<>]*>%Usi", "%(\&[a-zA-Z0-9\#]+;)%es", "%\s+%");
	$replace_clean = array(" ", "", "", "strtr('\\1',\$trans)", " ");
	$clean = preg_replace($search_clean, $replace_clean, $data);

	return $clean;
}
//reverse date format (e.g. convert date from Y-m-d to d-m-Y)
function ConvertDate($date)
{
	$data = explode("-", $date);
	$data1 = $date;
	if (is_array($data) && count($data) == 3)
	{
		$data1 = $data[2]."-".$data[1]."-".$data[0];
	}
	return $data1;
}

function transliterate($str){
    $str = strtr($str,
        "����������������������������������������������",
        "abvgdegziyklmnoprstufieABVGDEGZIYKLMNOPRSTUFIE"
    );
    $str = strtr($str, array(
        '�'=>'N', '�'=>"yo", '�'=>"h", '�'=>"ts", '�'=>"ch", '�'=>"sh",
        '�'=>"shch", '�'=>'', '�'=>'', '�'=>"yu", '�'=>"ya",
        '�'=>"Yo", '�'=>"H", '�'=>"Ts", '�'=>"Ch", '�'=>"Sh",
        '�'=>"Shch", '�'=>'', '�'=>'', '�'=>"Yu", '�'=>"Ya",
    ));
    return $str;
}

function abortUser($post, $id = null){//$id is coflexid or guid
    if(empty($id)){
        $id = $_SESSION['guid'];
    }
    $errors = array();
    $agree = $post->GetProperty('agree');
    if(!$agree){
        $errors['agree'] = "�� ������ ������� �������";
    }
    if(empty($errors)){
        $stmt = GetStatement();
        $query = "SELECT fio FROM motivated_users WHERE guid = '".$_SESSION['guid']."'";
        $userData = $stmt->FetchRow($query);
        if (!empty($userData)){
            $stmt = GetStatement();
            $query = "UPDATE motivated_users SET aborted_at=NOW() WHERE guid = '".$id."'";
            $stmt->Execute($query);
            header("Location: /lk/motiv-prog/");
        } else {
            $stmt = GetStatement();
            $query = "INSERT motivated_users SET aborted_at=NOW(), guid='".$_SESSION['guid']."',fio='', file='',created_at=NOW()";
            $stmt->Execute($query);
            header("Location: /lk/motiv-prog/");
        }
        $stmt = GetStatement();
        $query = "SELECT * FROM users_ WHERE CoflexID = '".$_SESSION['guid']."'";
        $userDataEmail = $stmt->FetchRow($query);
        $tpl = new Template("motivation/mail/abort_user.html");
        $tpl->SetVar("fio", $fio);
        $message = $tpl->grab();
        $subject = '�� ���������� �� ������� � ��������� ������.���������';
        sendMotivationEmail($userDataEmail['UserEmail'],$subject,$message);
        $tpl = new Template("motivation/mail/abort_for_manager.html");
        $tpl->SetVar("fio", $fio);
        $message = $tpl->grab();
        $subject = '������ ���������: �������� ����� �� �������';
        sendMotivationEmail($userDataEmail['UserAdminEmail'],$subject,$message);
    }
    $data = array(
        'agree_text' => $agree
    );
    return array($errors,$data);
}

function motRegisterEdit($post){
    $errors = array();
    $fio = $post->GetProperty('fio');
    $agree = $post->GetProperty('agree');
    $file = $_FILES['file'];
    if(!$agree){
        $errors['agree'] = "�� ������ ������� �������";
    }
    if(!empty($fio)){
        if (preg_match('/[^�-�\s]/', $fio))
        {
          $errors['fio'][]['text'] = "������ ���� ������ ������� ����� � �������";
        }
        if (strlen($fio) < 3 || strlen($fio) > 255)
        {
          $errors['fio'][]['text'] = "������ ���� �� ������ 3 � �� ������ 255 ��������";
        }
    }
    if($file['error'] == '4'){
        $file = NULL;
    }
    if(empty($errors) && !empty($file)){
        list($errors, $filename) = uploadFile($file);
    }
    if(empty($errors)){
        $updateData = array();
        if(!empty($fio)){
            $updateData[] = "fio='".$fio."'";
        }
        if(!empty($file)){
            $updateData[] = "file='".$filename."'";
        }
        if(!empty($updateData)){
            $stmt = GetStatement();
            $query = "UPDATE motivated_users SET ".implode(',', $updateData)." WHERE guid = '".$_SESSION['guid']."'";
            $stmt->Execute($query);
            header("Location: /lk/motiv-prog/");
        }
    }
    $data = array(
        'fio_text' => $fio,
        'agree_text' => $agree
    );
    return array($errors,$data);
}

function motRegister($post){
    $errors = array();
    $fio = $post->GetProperty('fio');
    $agree = $post->GetProperty('agree');
    $file = $_FILES['file'];
    if(!$agree){
        $errors['agree'] = "�� ������ ������� �������";
    }
    if (preg_match('/[^�-�\s]/', $fio))
    {
      $errors['fio'][]['text'] = "������ ���� ������ ������� ����� � �������";
    }
    if (strlen($fio) < 3 || strlen($fio) > 255)
    {
      $errors['fio'][]['text'] = "������ ���� �� ������ 3 � �� ������ 255 ��������";
    }
    if($file['error'] == '4'){
        $errors['file'][]['text'] = "�� ������ ������� ����";
    }
    if(empty($errors)){
        list($errors, $filename) = uploadFile($file);
    }
    if(empty($errors)){
        $stmt = GetStatement();
        $query = "INSERT INTO motivated_users SET guid='".$_SESSION['guid']."',fio='".$fio."', file='".$filename."',created_at=NOW()";
        $stmt->Execute($query);
        if($stmt->GetLastInsertID()){
            $stmt = GetStatement();
            $query = "SELECT * FROM users_ WHERE CoflexID = '".$_SESSION['guid']."'";
            $userData = $stmt->FetchList($query);
            if(!empty($userData)){
                $tpl = new Template("motivation/mail/newuser.html");
                $tpl->SetVar("fio", $fio);
                $tpl->SetVar("filename", HTTP_URL."agreement/".$filename);
                $tpl->LoadFromArrayWithLoop(array("userData"=>$userData));
                $message = $tpl->grab();
                $subject = '����������� ������ ��������� ��������� ����������';
                sendMotivationEmail($userData[0]['UserAdminEmail'],$subject,$message);
                $tpl = new Template("motivation/mail/newuserclient.html");
                $tpl->SetVar("fio", $fio);
                $message = $tpl->grab();
                $subject = '������ ���������. ������ ��������� ���������� �� ���������';
                sendMotivationEmail($userData[0]['UserEmail'],$subject,$message);
            }
        }
    }
    $data = array(
        'fio_text' => $fio,
        'agree_text' => $agree
    );
    return array($errors,$data);
}

function motGetBonus($post){
    $errors = array();
    $bonus = $post->GetProperty('bonus');
    $agree = $post->GetProperty('agree');
    if(!$agree){
        $errors['agree'] = "�� ������ ������� �������";
    }
    if (empty((int)$bonus))
    {
      $errors['bonus'] = "�� �� ������� �����";
    }
    if(empty($errors)){
        $bonuses = array(
            "1" => "��������� �������������",
            "2" => "���������� ����������"
        );
        $stmt = GetStatement();
        $query = "UPDATE motivation_data SET bonus_type='".$bonuses[$bonus]."',bonus_date=NOW() WHERE guid = '".$_SESSION['guid']."' AND god_tek = '".(MOT_YEAR-1)."'";
        $stmt->Execute($query);
        $stmt = GetStatement();
        $query = "SELECT * FROM motivated_users WHERE guid = '".$_SESSION['guid']."' LIMIT 1";
        $motUserData = $stmt->FetchRow($query);
        $stmt = GetStatement();
        $query = "SELECT * FROM motivation_data WHERE guid = '".$_SESSION['guid']."' AND god_tek = '".(MOT_YEAR-1)."' LIMIT 1";
        $motData = $stmt->FetchRow($query);
        if(!empty($motUserData) && !empty($motData)){
            $stmt = GetStatement();
            $query = "SELECT * FROM users_ WHERE CoflexID = '".$_SESSION['guid']."'";
            $userData = $stmt->FetchRow($query);
            $tpl = new Template("motivation/mail/bonus-selected.html");
            $tpl->SetVar("fio", $motUserData['fio']);
            $tpl->SetVar("bonus", number_format($motData['bonus_fakt'], 0, ',', ' '));
            $tpl->SetVar("bonusType", $motData['bonus_type']);
            $message = $tpl->grab();
            $subject = '������ ���������: �����������! �� �������� ��������� ������';
            sendMotivationEmail($userData['UserEmail'],$subject,$message);
            $user = getUserInfo();
            $admin = getAdminInfo($user['UserAdminID']);
            $tpl = new Template("motivation/mail/bonus-selected-manager.html");
            $tpl->SetVar("company", $user['UserCompany']);
            $tpl->SetVar("admin", $admin['AdminName']);
            $tpl->SetVar("bonus", number_format($motData['bonus_fakt'], 0, ',', ' '));
            $tpl->SetVar("bonusType", $motData['bonus_type']);
            $message = $tpl->grab();
            $subject = '������ ���������: ���������� '.$user['UserCompany'].' ������� ��������� ������';
            sendMotivationEmail($user['UserAdminEmail'],$subject,$message);
        }
        header("Location: /lk/motiv-prog/");
        exit();
    }
    $data = array(
        'bonus_text' => $bonus,
        'agree_text' => $agree
    );
    return array($errors,$data);
}

function uploadFile($file){
    
    $ValidfileSize = 3145728;
    $errors = array();
    $fileName = '';
    $fileError = $file['error'];
    if ($fileError > 0) 
    {
        switch ($fileError) 
        {
        case 1:
        $errors['file'][]['text'] = '���� ������ ������ � PHP.INI';//���� ������ ������ � PHP.INI';

        case 2:
        $errors['file'][]['text'] = '���� ������ ������ �������������� ������ HTML';//'���� ������ ������ �������������� ������ HTML';

        case 3:
        $errors['file'][]['text'] = '������ ��������� ��������';//'������ ��������� ��������';
        }
    }

    //check for filesize
    $fileSize = $file['size'];
    if($fileSize > $ValidfileSize)
    { 
        $errors['file'][]['text'] = '���� ������ 3MB';//'���� ������ 3MB';
    }

    //check the file extension is ok
    $fileName = $file['name'];
    $uploadedFileNameParts = explode('.',$fileName);
    $uploadedFileExtension = array_pop($uploadedFileNameParts);

    $validFileExts = explode(',', 'jpeg,jpg,png,pdf');

    //assume the extension is false until we know its ok
    $extOk = false;

    //go through every ok extension, if the ok extension matches the file extension (case insensitive)
    //then the file extension is ok
    foreach($validFileExts as $key => $value)
    {
            if( preg_match("/".$value."/i", $uploadedFileExtension ) )
            {
                    $extOk = true;

            }
    }

    if ($extOk == false) 
    {
        $errors['file'][]['text'] = '�������� ����������';//'�������� ����������';
    }

    //the name of the file in PHP's temp directory that we are going to move to our folder
    $fileTemp = $file['tmp_name'];

    //for security purposes, we will also do a getimagesize on the temp file (before we have moved it 
    //to the folder) to check the MIME type of the file, and whether it has a width and height
    $imageinfo = getimagesize($fileTemp);

    //we are going to define what file extensions/MIMEs are ok, and only let these ones in (whitelisting), rather than try to scan for bad
    //types, where we might miss one (whitelisting is always better than blacklisting) 
    $okMIMETypes = 'image/jpeg,image/pjpeg,image/png,image/x-png,image/gif,application/pdf';
    if (function_exists('finfo_file')) {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $type = finfo_file($finfo, $fileTemp);
        finfo_close($finfo);
    }
    $validFileTypes = explode(",", $okMIMETypes);		

    //if the temp file does not have a width or a height, or it has a non ok MIME, return
    if(!empty($imageinfo['mime'])){
        if( !is_int($imageinfo[0]) || !is_int($imageinfo[1]) ||  !in_array($imageinfo['mime'], $validFileTypes) )
        {

                $errors['file'][]['text'] =  '��� ����� �� ��������������';//'��� ����� �� ��������������' ;
        }
    }elseif(!empty($type)){
        if(!in_array($type, $validFileTypes) )
        {

                $errors['file'][]['text'] =  '��� ����� �� ��������������';//'��� ����� �� ��������������' ;
        }
    }

    //lose any special characters in the filename
    $fileName = preg_replace("/[^A-Za-z0-9\.]/i", "-", $fileName);

    //always use constants when making file paths, to avoid the possibilty of remote file inclusion
    $fileName = strtolower($fileName);
    if (file_exists(UPLOAD_FOLDER.$fileName)){
        $i = 1;
        while (file_exists(UPLOAD_FOLDER.$i.'-'.$fileName)) {

            $i++;
        }
        $fileName = $i.'-'.$fileName;
    }

    $uploadPath = UPLOAD_FOLDER.$fileName;
    $uploadurl = '/agreement/'.$fileName;
    //print_r($uploadPath); die();
    if(empty($errors) && !move_uploaded_file($fileTemp, $uploadPath)) 
    {
        $errors['file'][]['text'] =  '������ ����������� �����';//'������ ����������� �����';
    }
    $data = array($errors,$fileName);
    return $data;
}
function sendMotivationEmail($address,$subject,$message){
    $address = str_replace(';', ', ', $address);
    // ��� �������� HTML-������ ������ ���� ���������� ��������� Content-type
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    // �������������� ���������
    $headers .= 'From: SHAKLIN.RU <nrein@shaklin.ru>' . "\r\n";
    // ����������
    $subject = iconv('CP1251','UTF8',$subject);
    $message = iconv('CP1251','UTF8',$message);
    $headers = iconv('CP1251','UTF8',$headers);
return (mail($address, $subject, $message, $headers));
    
}
function checkBonus($tpl){
    $logOld = array();
    $stmt = GetStatement();
    $query = "SELECT * FROM motivation_data WHERE guid = '".$_SESSION['guid']."' AND god_tek = '".(MOT_YEAR-1)."' AND prirost_fakt > '14' LIMIT 1";
    $logOld = $stmt->FetchRow($query);
    if ((MOT_CURRENT_DATE >= MOT_BONUS_DATE_BEGIN) && (MOT_CURRENT_DATE <= MOT_BONUS_DATE_END)){
        if(!empty($logOld)){
            $tpl->SetVar("logOld", '1');
            $tpl->SetVar("end", '1');
        }
    }else{
        if(!empty($logOld['bonus_date'])){
            $tpl->SetVar("end", '1');
        }
    }
    return $tpl;
}
function getUserInfo($guid = null){
    if(empty($guid)){
        $guid = $_SESSION['guid'];
    }
    $stmt = GetStatement();
    $query = "SELECT * FROM users_ WHERE CoflexID = '".$guid."' LIMIT 1";
    return $stmt->FetchRow($query);
}
function getMotUserInfo($guid = null){
    if(empty($guid)){
        $guid = $_SESSION['guid'];
    }
    $stmt = GetStatement();
    $query = "SELECT * FROM motivated_users WHERE guid = '".$guid."' LIMIT 1";
    return $stmt->FetchRow($query);
}
function getAdminInfo($adminId){
    $stmt = GetStatement();
    $query = "SELECT * FROM admins WHERE AdminCoflexID = '".$adminId."' LIMIT 1";
    return $stmt->FetchRow($query);
}


?>