<?php
class Action extends LocalObject
{
  var $error = null;
  var $message = null;


  function Action()
  {
  }

  function LoadFromDataBase()
  {
    if ($this->GetProperty("ActionID"))
    {
      $query = "SELECT *, DATE_FORMAT(ActionDate, '".USER_DATE_FORMAT."') AS ActionDate1, DATE_FORMAT(ActionDateEnd, '".USER_DATE_FORMAT."') AS ActionDate2
        FROM actions
        WHERE ActionID = ".$this->GetPropertyForSQL("ActionID");
      $this->LoadFromSQL($query);
    }
  }

  function CheckAddInformation()
  {
    return $this->CheckInformation();
  }

  function CheckUpdateInformation()
  {
    return $this->CheckInformation();
  }

  function CheckInformation()
  {
    if ($this->GetProperty("ActionHeader") == null)
    {
      return "������� ���������";
    }
    elseif ($this->GetProperty("ActionDesc") == null)
    {
      return "�������� ��������";
    }
    return null;
  }

  //add/edit
  function Update()
  {
    global $post;
    $dates1 = split("-", $post->GetProperty('ActionDate1'));
    $dates2 = split("-", $post->GetProperty('ActionDate2'));
    $str = "ActionHeader=".$this->GetPropertyForSQL('ActionHeader').", ActionDesc =".$this->GetPropertyForSQL('ActionDesc').",  ActionActive=".$post->GetPropertyForSQL('ActionActive').", ActionDate='".$dates1[2]."-".$dates1[1]."-".$dates1[0]."'".", ActionDateEnd='".$dates2[2]."-".$dates2[1]."-".$dates2[0]."'";
    if ($this->GetProperty('ActionID'))
    {
      $query = "UPDATE actions SET ".$str." WHERE ActionID=".$this->GetPropertyForSQL('ActionID');
    }
    else
    {
      $query = "INSERT INTO actions SET ".$str;
    }
    $stmt = GetStatement();
    $stmt->Execute($query);
		if ($actionID = $stmt->GetLastInsertID() && !$this->GetProperty('ActionID'))
		{
			$this->SetProperty('ActionID', $actionID);
			$query = "UPDATE actions SET ActionsSortOrder=".$actionID." WHERE ActionID=".$actionID;
			$stmt->Execute($query);
		}

  }
}
?>