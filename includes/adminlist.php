<?php
require_once("localobjectlist.php");
require_once("image.php");
require_once("admin.php");

class AdminList extends LocalObjectList
{
	var $message = "";

	function LoadFromDataBase($selected = 0)
	{
		$query = "SELECT *, IF(AdminLevel = 1, 1, 0) as selected
  				FROM admins 
					ORDER BY AdminLevel";
		$this->LoadFromSQL($query);
	}

	function LoadFromDataBaseManager($selected = '0')
	{
//		$query = "SELECT *, IF(AdminID = " . $selected . ", 1, 0) as selected
		/*$query = "SELECT *, IF(AdminID = " . $selected . ", 1 , IF(AdminCoflexID = '" . $selected . "', 1, 0)) as selected
  				FROM admins WHERE AdminLevel=3 AND AdminCoflexID <> '0'
					ORDER BY AdminName";*/
    $query = "SELECT *, IF(AdminCoflexID = " . $selected . ", 1, 0) as selected
  				FROM admins WHERE AdminLevel=3 AND AdminCoflexID <> '0'
					ORDER BY AdminName";
		$this->LoadFromSQL($query);
	}

	function Delete($data)
	{
		if (is_array($data) && $data)
		{
			$ids = implode(",", Connection::GetSQLArray($data));
			if ($ids)
			{
				$stmt = GetStatement();

				$query = "DELETE FROM admins WHERE AdminID IN (".$ids.")";
				$stmt->Execute($query);

			}
		}
	}



}
?>