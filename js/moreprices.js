/*
Author: Phong Nha
Email: letranphongnha@gmail.com
======================================================================== 
Copyright (c) 2013
======================================================================== 
*/
var activeLoading = 1; // disable loading dialog
var activePagination = 1;
var perPage;
var totalGames;
var loading = 0;

function pageResize() {
	var winWidth = $(window).width();
	var winHeight= $(window).height();

	// for height
	var gameWrapperHeight = winHeight;
	var gameRows = Math.ceil(gameWrapperHeight / gameItemHeight);
	var wrapperHeight = gameRows * gameItemHeight;

	while(wrapperHeight > winHeight) {
		gameRows -= 1;
		wrapperHeight = (gameRows * gameItemHeight);
	}
	if(gameRows < 1) { gameRows = 1; wrapperHeight =  gameItemHeight; } 

	// for width
	var gameCols = Math.ceil(winWidth / gameItemWidth);
	var wrapperWidth = gameCols * gameItemWidth;
	if (!$.browser.msie || $.browser.version >= 9) {
		while(wrapperWidth > winWidth) {
			gameCols -= 1;
			wrapperWidth = gameCols * gameItemWidth;
		}
	} else {
		while(wrapperWidth > winWidth - 30) {
			gameCols -= 1;
			wrapperWidth = gameCols * gameItemWidth;
		}
	}


	$(".games_wrapper, .games_wrapper ul").width(wrapperWidth);
	// $(".games_wrapper").height(wrapperHeight);
	perPage = gameCols * 3;
	currentGames = gameCols * gameRows + perPage;
	loadGames(1);
}
function loadGames(page,filter) {
	if(activePagination && loading == 0) {
		if(activeLoading) $("#loadingOverlay").css("display", "block");
		loading = 1;
		if(page == 1) {
      $("#showmore").css("display", "none");
			$.get(dataUrl+"&page="+page+"&filter="+filter, function(data) {
				if(data == "") {
				  if(activeLoading) $("#loadingOverlay").css("display", "none");          
          return;
        }
				$("#games_page_1").append(data);
				loading = 0;
				if(activeLoading) $("#loadingOverlay").css("display", "none");
			});
		} else {
      $("#showmore").css("display", "none");
      //alert("aaaaaaaa");
			$.get(dataUrl+"&page="+page+"&filter="+filter, function(data) {
				if(data == "") { 
				  if(activeLoading) $("#loadingOverlay").css("display", "none");
          return;
        }
        //alert(data);				
				$("#games_page_1").append(data);
				loading = 0;
				if(activeLoading) $("#loadingOverlay").css("display", "none");
			});		
		}
	}
}
/*$(document).ready(function() {
 	$(window).scroll(function(){
		if  ($(window).scrollTop() >= ($(document).height() - $(window).height() - 800)) {
      //alert($(window).scrollTop() + " " + $(document).height() + " " + $(window).height());
			if(activePagination && loading == 0) {
				//alert($(document).height());
				currentPage++;
				loadGames(currentPage);
			}
		}
	}); 
});*/
