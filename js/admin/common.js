function LinkWindow(Link) {
  if (window.event) {
    window.event.cancelBubble = true;
    window.event.returnValue = false;
  }
	var width, height;
	try {
		width = Math.ceil(screen.width/2);
		height = Math.ceil(screen.height*0.6);
	}
	catch (error) {
		width = 600;
		height = 400;
	}
  if (width < 600) { width = 600; }
  if (height < 400) { height = 400; }
	var left, top;
	try {
		left = Math.ceil(screen.width/4);
		top = Math.ceil(screen.height*0.1);
	}
	catch (error) {
		left = 100;
		top = 100;
	}
  if (typeof(oTarget) != "undefined") {
    oTarget.close();
  }
  var time = (new Date()).getTime();
  oTarget = window.open(Link, "Additional" + time, "height="+height+",width="+width+",scrollbars=1,left="+left+",top="+top);
  oTarget.focus();
  return false;
};

function CheckAll(elem)
{
  if (elem.checked)
  {
    for (i = 0; i < document.forms.list.elements.length; i++)
    {
      if (document.forms.list.elements[i].type == "checkbox")
      {
        document.forms.list.elements[i].checked = true;
      }
    }
  }
  else
  {
    for (i = 0; i < document.forms.list.elements.length; i++)
    {
      if (document.forms.list.elements[i].type == "checkbox")
      {
        document.forms.list.elements[i].checked = false;
      }
    }
  }
}

function IsAllChecked(elem)
{
  if (!elem.checked)
  {
    document.getElementById('select').checked = false;
  }
}

function UnCheckAll(elem)
{
  if (!elem.checked)
  {
    document.forms.list.checkall.checked = false;
  }
}

function Delete(whatDelete, newOp)
{
  if (confirm("�� ������������� ������� ������� "+whatDelete+"?"))
  {
    document.forms.list.op.value = newOp;
    document.forms.list.submit();
    return true;
  }
  else
  {
    return false;
  }
}

function CheckDate(form)
{
	if (form != "undefined")
	{
		if (form.StartDate.value == '')
		{
			alert('������� ����');
			return false;
		}
		else
		{
			if (form.EndDate.value == '')
			{
				alert('������� ����');
				return false;
			}
		}
	}
	return true;
}

function EnableBrowse(enabled)
{
	document.getElementById('newImage').disabled = !enabled;
};

function EnableBrowseNew(enabled, name)
{
	document.getElementById(name).disabled = !enabled;
};

function appendRow(table, htmlArray)
{
	insRow(table, document.getElementById(table).rows.length-1, htmlArray);
}

//insert in a table the row with N cells
function insRow(table, i, htmlArray)
{
	var x = document.getElementById(table).insertRow(i);
	var n = htmlArray.length;
	for (j = 0; j < n; j++)
	{
		var y = x.insertCell(j);
		y.innerHTML = htmlArray[j];
		y.align = "center";
		if (i%2 == 0)
		{
    			y.bgColor = "#dddddd";
		}

	}
}

function ResetRowsColor(tbl)
{
	var table = document.getElementById(tbl);
	var n = table.rows.length;
	for (i = 1; i < n - 1; i++)
	{
		var m = table.rows[i].cells.length;
		if (i%2 == 0)
		{
			for (j = 0; j < m; j++)
			{
				table.rows[i].cells[j].bgColor = "#dddddd";
			}
		}
		else
		{
			for (j = 0; j < m; j++)
			{
				table.rows[i].cells[j].bgColor = "#ffffff";
			}
		}
	}
}

function delRow(table, i)
{
	document.getElementById(table).deleteRow(i);
	ResetRowsColor(table);
}

//============!!!test function
function fnShowProps(obj, objName)
{
    var result = "";
    j = 0;
    for (var i in obj)
    { // 
        if (j > 60)
        result += objName + "." + i + " = " + obj[i] + "<br />\n";
        j++;
    }
    return result;
}
//============!!end test

