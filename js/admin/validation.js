
function Validation()
{
	var Err=function()
	{
		var oSource  = new Object;
		var sMessage = new String;

		this.clear=function ()
		{
			oSource  = new Object;
			sMessage = new String;
		}

		this.add=function (oSrc,sMsg)
		{
			oSource  = oSrc;
			sMessage = sMsg;
		}

		this.raise=function ()
		{
			var sName = oSource.NAME;
			var sMsg  = oSource.MSG;

			sMsg = sMsg ? sMsg : sMessage + (sName ? " in the "+sName+" field" : "");
			alert(sMsg);

			if(oSource.focus)
				oSource.focus();
			if(oSource.select)
				oSource.select();
			this.clear();
		}
	}

	var isDate=function (sDate,sFormat)
	{
		var aDaysInMonth=new Array(31,28,31,30,31,30,31,31,30,31,30,31);
		var sSepDate=sDate.charAt(sDate.search(/\D/));
		var sSepFormat=sFormat.charAt(sFormat.search(/[^MDY]/i));

		if (sSepDate!=sSepFormat)
			return false;

		var aValueMDY=sDate.split(sSepDate);
		var aFormatMDY=sFormat.split(sSepFormat);

		var iMonth,iDay,iYear;
		iMonth = aValueMDY[0];
		iDay   = aValueMDY[1];
		iYear  = aValueMDY[2];

		if (  !isNum( iMonth )
			||!isNum( iDay   )
			||!isNum( iYear  ) )
			return false;

		if(iYear.length!=4)
			return false;

		var iDaysInMonth=(iMonth!=2)?aDaysInMonth[iMonth-1]:
			((iYear%4==0 && iYear%100!=0 || iYear % 400==0)?29:28);

		return (iDay!=null && iMonth!=null && iYear!=null
				&& iMonth<13 && iMonth>0 && iDay>0 && iDay<=iDaysInMonth);
	}

	var isNum=function (v)
	{
		return (typeof v!="undefined" && v.toString() && !/\D/.test(v));
	}

	var formatNumber=function (i)
	{
		var sEnd="$";
		i = i.toString();
		if (/\./.test(i))
			sEnd="\\.";
		var re = new RegExp("(\\d)(\\d{3})(,|"+sEnd+")");
		if(re.test(i))
			i = formatNumber(i.replace(re, "$1,$2$3"));
		return i;
	}

	var getValueOf=function (oElement)
	{
		var sReturnValue = null;
		switch (oElement.type)
		{
			case "text" : case "textarea" : case "file" : case "password" : case "hidden" :
				sReturnValue = oElement.value;
				break;
			case "select-one" :
				sReturnValue = oElement.options[oElement.selectedIndex].value;
				break;
			case "select-multiple" :
				var i, iOptions = oElement.options.length;
				for(i=0; i<iOptions; i++)
					if(oElement.options[i].selected && oElement.options[i].value.toString().trim())
					{
						sReturnValue = true;
						break;
					}
				break;
			case "radio" : case "checkbox" :
				if(oElement.checked)
					sReturnValue = oElement.value ? oElement.value : true;
				break;
		}
		return sReturnValue;
	}

	this.setup=function ()
	{

		var i,oForm,iForms=document.forms.length;
		//alert(document.forms[0].name);
		for(i=0; i<iForms; i++){
			oForm=document.forms[i];
			if(!oForm.bProcessed)
			{
				var fnSubmit=oForm.onsubmit;
				oForm.onsubmit=function ()
				{
					if(typeof this.onbeforevalidate=="function" && this.onbeforevalidate()==false)
						return false;
					var i, oElement, iElements=this.elements.length;

					for(i=0;i<iElements;i++)
					{
						oElement=this.elements[i];
						if (!oElement.valid())
						{
							Validation.Err.raise();
							return false;
						}
					}
					if(typeof this.onaftervalidate=="function" && this.onaftervalidate()==false)
						return false;

					if (fnSubmit && fnSubmit()==false)
						return false;
				}
				oForm.bProcessed=true;
			}
			var j, iElements=oForm.elements.length;
			for(j=0; j<iElements; j++)
			{
				var oElement=oForm.elements[j];
				if(!oElement.bProcessed)
				{

					oElement.valid=function ()
					{
						if(this.value)
							this.value = this.value.trim();
						var sValue = getValueOf(this);
						if(this.REQUIRED && !sValue)
						{
							Validation.Err.add(this, "Please enter a value");
					 		return false;
						}

						var bSigned=this.SIGNED;
						if (this.FLOAT && sValue)
						{
							var re=new RegExp("^"+((bSigned)?"-?":"")+"(\\d*(,?\\d{3})*\\.?\\d+|\\d+(,?\\d{3})*\\.?\\d*)$");
							if (!re.test(sValue))
							{
								Validation.Err.add(this, "Please enter a float value");
								return false;
							}
							var iMin = this.MIN;
							if(iMin==parseFloat(iMin) && parseFloat(sValue.replace(/,/,""))<parseFloat(iMin))
							{
								Validation.Err.add(this, "Please enter a value greater than or equal to "+formatNumber(iMin));
								return false;
							}
							var iMax = this.MAX;
							if(iMax==parseFloat(iMax) && parseFloat(sValue.replace(/,/,""))>parseFloat(iMax))
							{
								Validation.Err.add(this, "Please enter a value less than or equal to "+formatNumber(iMax));
								return false;
							}
						}
						if (this.AMOUNT && sValue)
						{
							var sSigned = "(\\$?-?|-?\\$?)";
							var re = new RegExp("^"+((bSigned)?sSigned:"\\$?")+"((\\d{1,3})*(,?\\d{3})*\\.?\\d{2}|\\d{1,3}(,?\\d{3})*\\.?(\\d{2})?)$");
							if(!re.test(sValue))
							{
								Validation.Err.add(this, "Please enter a dollar amount");
								return false;
							}
							var iMin = this.MIN;
							if(iMin==parseFloat(iMin) && parseFloat(sValue.replace(/[\$,]/,""))<parseFloat(iMin))
							{
								Validation.Err.add(this, "Please enter a value greater than or equal to $"+formatNumber(iMin));
								return false;
							}
							var iMax = this.MAX;
							if(iMax==parseFloat(iMax) && parseFloat(sValue.replace(/[\$,]/,""))>parseFloat(iMax))
							{
								Validation.Err.add(this, "Please enter a value less than or equal to $"+formatNumber(iMax));
								return false;
							}
						}
						if (this.INTEGER && sValue)
						{
							var re=new RegExp("^"+((bSigned)?"-?":"")+"\\d{1,3}(,?\\d{3})*$");
							if (!re.test(sValue))
							{
								Validation.Err.add(this, "Please enter an integer value");
								return false;
							}
							var iMin = this.MIN;
							if(iMin==parseInt(iMin) && parseInt(sValue.replace(/,/,""))<parseInt(iMin))
							{
								Validation.Err.add(this, "Please enter a value greater than or equal to "+formatNumber(iMin));
								return false;
							}
							var iMax = this.MAX;
							if(iMax==parseInt(iMax) && parseInt(sValue.replace(/,/,""))>parseInt(iMax))
							{
								Validation.Err.add(this, "Please enter a value less than or equal to "+formatNumber(iMax));
								return false;
							}

						}
						if (this.DATE && sValue)
						{
							var sFormat = "MM/DD/YYYY";
							if (!isDate(sValue,sFormat))
							{
								Validation.Err.add(this, "Please enter a date as MM/DD/YYYY");
								return false;
							}
						}
						var oRegexp=this.REGEXP;
						if(oRegexp && oRegexp.constructor==RegExp && sValue)
						{
							if(!oRegexp.test(sValue))
							{
								Validation.Err.add(this, "Please enter a valid value");
								return false;
							}
						}
						//if(this.PHONE && sValue)
						//{
						//	var sPhone  = sValue.replace(/\D/g,"");
						//	var iDigits = sPhone.length;
						//	if( !(iDigits==10 || iDigits==11 && /^1/.test(sPhone)) )
						//	{
						//		Validation.Err.add(this, "Please enter a valid phone number");
						//		return false;
						//	}
						//}

						if(this.PHONE && sValue)
						{
							var sPhone  = sValue.replace(/\D/g,"");
							var iDigits = sPhone.length;
							if( (iDigits < 5 || iDigits > 15) )
							{
								Validation.Err.add(this, "Please enter a valid phone number");
								return false;
							}
						}
						if(sValue && this.LENGTH && isNum(this.LENGTH) && sValue.length>this.LENGTH)
						{
							Validation.Err.add(this, "Please enter a value with fewer than " + formatNumber(this.LENGTH) + " characters");
							return false;
						}
						if(this.EMAIL && sValue)
						{
							if(!/^[\w_-]+(\.[\w_-]+)*@[\w_-]+(\.[\w_-]+)*\.\w{2,4}$/i.test(sValue))
							{
								Validation.Err.add(this, "Please enter a correct Email address");
								return false;
							}
						}
						if(this.ZIP && sValue)
						{
							if(!/^\d{5}(-?\d{4})?$/.test(sValue))
							{
								Validation.Err.add(this, "Please enter a valid ZIP code");
								return false;
							}
						}
						var vAnd=this.AND;
						if(vAnd && sValue)
						{
							if(vAnd.constructor!=Array)
								vAnd = vAnd.toString().split(/,/);
							var i, iFields=vAnd.length;
							for(i=0; i<iFields; i++)
							{
								var oNewElement =
									(vAnd[i].form)
									? vAnd[i]
									: this.form.elements[vAnd[i].trim()];
								if(oNewElement)
								{
									if(!getValueOf(oNewElement))
									{
										Validation.Err.add(oNewElement, "Please enter a value");
										return false;
									}
								}
							}
						}
						
						// ANDPWD
						var vAnd=this.ANDPWD;
						if(vAnd && sValue)
						{
							// If not an array, create one
							if(vAnd.constructor!=Array)
								vAnd = vAnd.toString().split(/,/);
							// Require each element in the list
							var i, iFields=vAnd.length;
							for(i=0; i<iFields; i++)
							{
								var oNewElement =
									(vAnd[i].form)
									? vAnd[i]
									: this.form.elements[vAnd[i].trim()];
								if(oNewElement)
								{
									if(!getValueOf(oNewElement))
									{
										Validation.Err.add(oNewElement, "Please enter a value");
										return false;
									}
									if (i==1 && getValueOf(this.form.elements[vAnd[i].trim()]) != getValueOf(this.form.elements[vAnd[i-1].trim()]))
									{
										Validation.Err.add(oNewElement, "Second password must be the same value");
										return false;
									}									
								}
							}
						}									
						
						var vOr=typeof this.OR=="object";
						if(vOr && !sValue)
						{
							vOr = this.OR["fields"];
							if(vOr)
							{
								// If not an array, create one
								if(vOr.constructor!=Array)
									vOr = vOr.toString().split(/,/);
								// Require each element in the list
								var i, iFields=vOr.length;
								var bValue=false;
								for(i=0; i<iFields; i++)
								{
									var oNewElement =
										(vOr[i].form)
										? vOr[i]
										: this.form.elements[vOr[i].trim()];
									if(oNewElement)
										bValue |= !!getValueOf(oNewElement);
								}
								if(!bValue)
								{
									Validation.Err.add(this, this.OR["msg"]?this.OR["msg"]:"Please enter a value");
									return false;
								}
							}
						}
						if (this.NOSPACE && this.value)
							this.value=this.value.replace(/\s/g,"");

						if (this.UPPERCASE && this.value)
							this.value=this.value.toUpperCase();

						if (this.LOWERCASE && this.value)
							this.value=this.value.toLowerCase();

						if(typeof this.onvalidate=="function" && this.onvalidate()==false)
							return false;

						return true;
					}
					oElement.bProcessed=true;
				}
			}
		}
	}

	if("".replace && window.RegExp)
	{

		String.prototype.trim=function ()
		{
			return this.replace(/^\s+|\s+$/g,"");
		}

		if(document.forms)
		{
			this.setup();
			this.Err=new Err;
		}
	}
}

Validation=new Validation;