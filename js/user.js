function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0

  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}


function LinkWindow(Link) {
  if (window.event) {
    window.event.cancelBubble = true;
    window.event.returnValue = false;
  }
	var width, height;
	try {
		width = Math.ceil(screen.width/2);
		height = Math.ceil(screen.height*0.6);
	}
	catch (error) {
		width = 600;
		height = 400;
	}
  if (width < 600) { width = 600; }
  if (height < 400) { height = 400; }
	var left, top;
	try {
		left = Math.ceil(screen.width/4);
		top = Math.ceil(screen.height*0.1);
	}
	catch (error) {
		left = 100;
		top = 100;
	}
  if (typeof(oTarget) != "undefined") {
    oTarget.close();
  }
  var time = (new Date()).getTime();
  oTarget = window.open(Link, "Additional" + time, "height="+height+",width="+width+",scrollbars=1,left="+left+",top="+top);
  oTarget.focus();
  return false;
};

function OpenWindow(Link) {
  if (window.event) {
    window.event.cancelBubble = true;
    window.event.returnValue = false;
  }
	var width, height;
	try {
		width = Math.ceil(screen.width/1.3);
		height = Math.ceil(screen.height - 100);
	}
	catch (error) {
		width = 600;
		height = 400;
	}
  if (width < 600) { width = 600; }
  if (height < 400) { height = 400; }
	var left, top;
	try {
		left = Math.ceil(screen.width/6);
		top = Math.ceil(screen.height*0.01);
	}
	catch (error) {
		left = 100;
		top = 100;
	}
  if (typeof(oTarget) != "undefined") {
    oTarget.close();
  }
  var time = (new Date()).getTime();
  oTarget = window.open(Link, "Additional1" + time, "height="+height+",width="+width+",scrollbars=1,left="+left+",top="+top);
  oTarget.focus();
  return false;
};

function fixFlash()
{
	var objects = document.getElementsByTagName("object");
	for (var i = 0; i < objects.length; i++)
	{
		objects[i].outerHTML = objects[i].outerHTML;
	}
}