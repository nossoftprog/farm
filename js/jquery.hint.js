(function ($) {
    $.fn.needHint = function () {  
        
        return this.each(function () {
            var title = $(this).attr('data-title');
            var width = $(this).width();
            var height = $(this).height();
            var heigthalf = (height)/2;
            $(this).wrap('<div class="wrapper_hint">').css({cursor : 'pointer'});
            $(this).parent('.wrapper_hint').append('<div class="hint">'+title+'<span class="arrow_hint"></span></div></div>');
            var width_hint = $(this).siblings('.hint').width();
            var width_arrow = $(this).siblings('.hint').find('.arrow_hint').width();
            var leftPos = (width_hint-width)/2+width_arrow;
            //$(this).siblings('.hint').css({left : -leftPos, bottom : height+10 });
            $(this).siblings('.hint').css({left : 20, top : 0  });
            
            $(this).hover(function(){
                $(this).parent('.wrapper_hint').find('.hint').stop(true,true).show('scale',{direction:'down'},300);
            },function(){
                $(this).parent('.wrapper_hint').find('.hint').stop(true,true).hide('scale',{direction:'down'},200);
            })
                      
        });
    };
})(jQuery);