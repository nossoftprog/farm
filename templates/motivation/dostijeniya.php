<?php
$debitorDate = date_create(MOT_MONTH.'/'.MOT_DAY.'/'.MOT_YEAR);
$debitorDate->modify('-1 day');
?>
<div class="motivation-wrapper">
    <div class="row mot-top-menu">
        <a href="/lk/motiv-prog/" class="chapter">&#9658;������� ����������</a>
        <a href="/lk/motiv-prog/pravila" class="chapter">&#9658;������� � ������ ���������</a>
        <a href="/lk/motiv-prog/otkaz" class="chapter">&#9658;���������� �� �������</a>
        <?php 
        if ((MOT_CURRENT_DATE >= MOT_BONUS_DATE_BEGIN) && (MOT_CURRENT_DATE <= MOT_BONUS_DATE_END)){
            if (!empty($logOld)){
                echo '<a href="/lk/motiv-prog/poluchit-bonus" class="chapter">&#9658;�������� �����</a>';
            }
        }
        ?>
        <a class="chapter active-button">����������� ����������</a>
    </div>
    <?php if(!empty($curYearLog) && $curYearLog['prirost_fakt'] > '14' && empty($curYearLog['data_vrucheniya'])){ ?>
        <div class="row"><p class="bolder dark-red-text">��������, �� �������� ������ � ������� <?php echo number_format($curYearLog['bonus_fakt'], 0, ',', ' '); ?> ���. �������� ����� �� 18-01-17, �������� ����� �� 28-01-17.</p></div>
    <?php } ?>
    <?php if(!empty($bonusLog)){ ?>
    <?php foreach ($bonusLog as $bl){ ?>
        <div class="row"><p class="bolder">����� � �������: <?php echo number_format($bl['bonus_fakt'], 0, ',', ' '); ?> ���. ��� ������� <?php echo date('d/m/Y',strtotime($bl['data_vrucheniya'])); ?></p></div>
    <?php }} ?>
    <?php if(!empty($curYearLog)){
        if(!empty($motProgDate[$curYearLog['god_tek']])){//���� � ������� ���� ������� ���, �� ������ ������� �� ����
            $motStartDate = $motProgDate[$curYearLog['god_tek']][0];
            $motEndDate = $motProgDate[$curYearLog['god_tek']][1];
        }else{//���� � ������� ���, �� ������� ����������� ��������
            $motStartDate = '01/01/'.$curYearLog['god_tek'];
            $motEndDate = '01/31/'.$curYearLog['god_tek'];
        }
        if(!empty($motProgDate[$curYearLog['god_tek']-1])){//���� � ������� ���� ������� ���, �� ������ ������� �� ����
            $motStartDatePred = $motProgDate[$curYearLog['god_tek']-1][0];
            $motEndDatePred = $motProgDate[$curYearLog['god_tek']-1][1];
        }else{//���� � ������� ���, �� ������� ����������� ��������
            $motStartDatePred = '01/01/'.$curYearLog['god_tek']-1;
            $motEndDatePred = '01/31/'.$curYearLog['god_tek']-1;
        }
        ?>
        <div style="clear: both;"></div>
        <div class="col-mod-6">
            <div class="row"><p>������ ���������: <span class="dark-red-text">� <?php echo $motStartDate; ?> �� <?php echo $motEndDate; ?></span></p></div>
            <div class="row"><p>&#9658;������������� �� <?php echo $debitorDate->format('d/m/Y'); ?>: <span class="dark-red-text"><?php echo number_format($curYearLog['summa_debitorki'], 0, ',', ' '); ?> ���.</span></p></div>
            <?php if(!empty((int)$curYearLog['summa_prosrochki'])){ ?>
                <div class="row dark-red-text bolder"><p>&#9658;������������ �������������: <span class=""><?php echo number_format($curYearLog['summa_prosrochki'], 0, ',', ' '); ?> ���.</span></p></div>
            <?php } ?>
        </div>
        <div class="col-mod-6">
        <div class="row"><p>&#9658;������ � <?php echo $motStartDatePred; ?> �� <?php echo $motEndDatePred; ?>: <span class="dark-red-text"><?php echo number_format($curYearLog['otgruzki_PRED'], 0, ',', ' '); ?> ���.</span></p></div>
        <div class="row"><p>&#9658;������ � <?php echo $motStartDate; ?> �� <?php echo $motEndDate; ?>: <span class="dark-red-text"><?php echo number_format($curYearLog['otgruzki_TEK'], 0, ',', ' '); ?> ���.</span></p></div>
        </div>
        <div style="clear: both;"></div>
        <table class="row" style="text-align: center;">
            <thead class="motivation-th">
                <tr>
                    <th class="motivation-th motivation-cells"></th>
                    <th class="motivation-th motivation-cells">I ��.</th>
                    <th class="motivation-th motivation-cells">II ��.</th>
                    <th class="motivation-th motivation-cells">III ��.</th>
                    <th class="motivation-th motivation-cells">IV ��.</th>
                    <th class="motivation-th motivation-cells">�����</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="motivation-left-column motivation-cells">�������� � <?php echo $curYearLog['god_tek']-1; ?>�.:</td>
                    <td class="motivation-cells"><?php echo number_format($curYearLog['otgruzki_1kvartalPRED'], 0, ',', ' '); ?></td>
                    <td class="motivation-cells"><?php echo number_format($curYearLog['otgruzki_2kvartalPRED'], 0, ',', ' '); ?></td>
                    <td class="motivation-cells"><?php echo number_format($curYearLog['otgruzki_3kvartalPRED'], 0, ',', ' '); ?></td>
                    <td class="motivation-cells"><?php echo number_format($curYearLog['otgruzki_4kvartalPRED'], 0, ',', ' '); ?></td>
                    <td class="motivation-cells"><?php echo number_format($curYearLog['otgruzki_PRED'], 0, ',', ' '); ?></td>
                </tr>
                <tr>
                    <td class="motivation-left-column motivation-cells">�������� � <?php echo $curYearLog['god_tek']; ?>�.:</td>
                    <td class="motivation-cells"><?php echo number_format($curYearLog['otgruzki_1kvartalTEK'], 0, ',', ' '); ?></td>
                    <td class="motivation-cells"><?php echo number_format($curYearLog['otgruzki_2kvartalTEK'], 0, ',', ' '); ?></td>
                    <td class="motivation-cells"><?php echo number_format($curYearLog['otgruzki_3kvartalTEK'], 0, ',', ' '); ?></td>
                    <td class="motivation-cells"><?php echo number_format($curYearLog['otgruzki_4kvartalTEK'], 0, ',', ' '); ?></td>
                    <td class="motivation-cells"><?php echo number_format($curYearLog['otgruzki_TEK'], 0, ',', ' '); ?></td>
                </tr>
                <tr>
                    <td class="motivation-left-column motivation-cells">������� � % � ������������ ������� <?php echo $curYearLog['god_tek']-1; ?>�.:</td>
                    <td class="dark-red-text motivation-cells"><?php echo (int)$curYearLog['norma_1kvartal']; ?>%</td>
                    <td class="dark-red-text motivation-cells"><?php echo (int)$curYearLog['norma_2kvartal']; ?>%</td>
                    <td class="dark-red-text motivation-cells"><?php echo (int)$curYearLog['norma_3kvartal']; ?>%</td>
                    <td class="dark-red-text motivation-cells"><?php echo (int)$curYearLog['norma_4kvartal']; ?>%</td>
                    <td class="dark-red-text motivation-cells dark-red-text"><?php echo (int)$curYearLog['prirost_fakt']; ?>%</td>
                </tr>
            
            </tbody>
        </table>
    <?php } ?>
        
    
</div>
