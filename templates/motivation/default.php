<?php
$debitorDate = date_create(MOT_MONTH.'/'.MOT_DAY.'/'.MOT_YEAR);
$debitorDate->modify('-1 day');
?>
<div class="motivation-wrapper">
    <div class="row mot-top-menu">
        <a class="chapter active-chapter">&#9658;������� ����������</a>
        <a href="/lk/motiv-prog/pravila" class="chapter">&#9658;������� � ������ ���������</a>
        <a href="/lk/motiv-prog/otkaz" class="chapter">&#9658;���������� �� �������</a>
        <?php
        if ((MOT_CURRENT_DATE >= MOT_BONUS_DATE_BEGIN) && (MOT_CURRENT_DATE <= MOT_BONUS_DATE_END)){
            if(empty($logOld['bonus_date'])){
                echo '<a href="/lk/motiv-prog/poluchit-bonus" class="chapter">&#9658;�������� �����</a>';
            }
            if(!empty($logOld)){
                echo '<a href="/lk/motiv-prog/dostijeniya" class="chapter active-button">����������� ����������</a>';
            }
        }elseif(!empty($logOld['bonus_date'])){
            echo '<a href="/lk/motiv-prog/dostijeniya" class="chapter active-button">����������� ����������</a>';
        }
        ?>
    </div>
    <?php if(!empty($log)){
        if(!empty($motProgDate[$log['god_tek']])){//���� � ������� ���� ������� ���, �� ������ ������� �� ����
            $motStartDate = $motProgDate[$log['god_tek']][0];
            $motEndDate = $motProgDate[$log['god_tek']][1];
        }else{//���� � ������� ���, �� ������� ����������� ��������
            $motStartDate = '01/01/'.$log['god_tek'];
            $motEndDate = '01/31/'.$log['god_tek'];
        }
        if(!empty($motProgDate[$log['god_tek']-1])){//���� � ������� ���� ������� ���, �� ������ ������� �� ����
            $motStartDatePred = $motProgDate[$log['god_tek']-1][0];
            $motEndDatePred = $motProgDate[$log['god_tek']-1][1];
        }else{//���� � ������� ���, �� ������� ����������� ��������
            $motStartDatePred = '01/01/'.$log['god_tek']-1;
            $motEndDatePred = '01/31/'.$log['god_tek']-1;
        }
    ?>
    <div style="clear: both;"></div>
    <div class="col-mod-6">
        <div class="row"><p>������ ���������: <span class="dark-red-text">� <?php echo $motStartDate; ?> �� <?php echo $motEndDate; ?></span></p></div>
        <div class="row"><p>&#9658;������������� �� <?php echo $debitorDate->format('d/m/Y'); ?>: <span class="dark-red-text"><?php echo number_format($log['summa_debitorki'], 0, ',', ' '); ?> ���.</span></p></div>
        <?php if(!empty((int)$log['summa_prosrochki'])){ ?>
            <div class="row dark-red-text bolder"><p>&#9658;������������ �������������: <span class=""><?php echo number_format($log['summa_prosrochki'], 0, ',', ' '); ?> ���.</span></p></div>
        <?php } ?>
    </div>
    <div class="col-mod-6">
        <div class="row"><p>&#9658;������ � <?php echo $motStartDatePred; ?> �� <?php echo $motEndDatePred; ?>: <span class="dark-red-text"><?php echo number_format($log['otgruzki_PRED'], 0, ',', ' '); ?> ���.</span></p></div>
        <div class="row"><p>&#9658;������ � <?php echo $motStartDate; ?> �� <?php echo MOT_DAY.'/'.MOT_MONTH.'/'.MOT_YEAR; ?>: <span class="dark-red-text"><?php echo number_format($log['otgruzki_TEK'], 0, ',', ' '); ?> ���.</span></p></div>
        <?php if(!empty($log['prirost_fakt']) && $log['prirost_fakt'] >= '15'){ ?>
        <div class="row">
            <p class="bolder dark-red-text">�� �������� �������� ������� � <?php echo (int)$log['prirost_fakt']; ?>%. ��� ����� = <?php echo number_format($log['bonus_fakt'], 0, ',', ' '); ?> ���.</p>
        </div>
        <?php }else{ ?>
            <p class="bolder dark-red-text">����� �� ���������.</p>
        <?php } ?>
    </div>
    <div style="clear: both;"></div>
    <table class="row" style="text-align: center;">
        <thead class="motivation-th">
            <tr>
                <th class="motivation-th motivation-cells"></th>
                <th class="motivation-th motivation-cells">I ��.</th>
                <th class="motivation-th motivation-cells">II ��.</th>
                <th class="motivation-th motivation-cells">III ��.</th>
                <th class="motivation-th motivation-cells">IV ��.</th>
                <th class="motivation-th motivation-cells">�����</th>
            </tr>
        </thead>
        <tbody>
                <tr>
                    <td class="motivation-left-column motivation-cells">�������� � <?php echo $log['god_tek']-1; ?>�.:</td>
                    <td class="motivation-cells"><?php echo number_format($log['otgruzki_1kvartalPRED'], 0, ',', ' '); ?></td>
                    <td class="motivation-cells"><?php echo number_format($log['otgruzki_2kvartalPRED'], 0, ',', ' '); ?></td>
                    <td class="motivation-cells"><?php echo number_format($log['otgruzki_3kvartalPRED'], 0, ',', ' '); ?></td>
                    <td class="motivation-cells"><?php echo number_format($log['otgruzki_4kvartalPRED'], 0, ',', ' '); ?></td>
                    <td class="motivation-cells"><?php echo number_format($log['otgruzki_PRED'], 0, ',', ' '); ?></td>
                </tr>
                <tr>
                    <td class="motivation-left-column motivation-cells">�������� � <?php echo $log['god_tek']; ?>�.:</td>
                    <td class="motivation-cells"><?php echo number_format($log['otgruzki_1kvartalTEK'], 0, ',', ' '); ?></td>
                    <td class="motivation-cells"><?php echo number_format($log['otgruzki_2kvartalTEK'], 0, ',', ' '); ?></td>
                    <td class="motivation-cells"><?php echo number_format($log['otgruzki_3kvartalTEK'], 0, ',', ' '); ?></td>
                    <td class="motivation-cells"><?php echo number_format($log['otgruzki_4kvartalTEK'], 0, ',', ' '); ?></td>
                    <td class="motivation-cells"><?php echo number_format($log['otgruzki_TEK'], 0, ',', ' '); ?></td>
                </tr>
                <tr>
                    <td class="motivation-left-column motivation-cells">������� � % � ������������ ������� <?php echo $log['god_tek']-1; ?>�.:</td>
                    <td class="dark-red-text motivation-cells"><?php echo (int)$log['norma_1kvartal']; ?>%</td>
                    <td class="dark-red-text motivation-cells"><?php echo (int)$log['norma_2kvartal']; ?>%</td>
                    <td class="dark-red-text motivation-cells"><?php echo (int)$log['norma_3kvartal']; ?>%</td>
                    <td class="dark-red-text motivation-cells"><?php echo (int)$log['norma_4kvartal']; ?>%</td>
                    <td class="dark-red-text motivation-cells dark-red-text"><?php echo (int)$log['prirost_fakt']; ?>%</td>
                </tr>
        </tbody>
    </table>
    <div class="row with-count">
        <?php if (!(MOT_MONTH=='01' && MOT_DAY<'10')){ ?>
        <span class="bolder" >�� ����� ��������� ��������:</span>
        <div id="counter" class="motivation-counter">
            <div class="count-digit-cont">
                <?php if(!empty($dayToEndOfYear)){ ?>
                <?php foreach ($dayToEndOfYear as $digit){ ?>
                <div class="motivation-count-digit"><?php echo $digit; ?></div>
                <?php }} ?>
            </div>
            <span>����</span>
        </div>
    <?php } ?>
    </div>
    <div class="row"><p class="bolder">������ �������� �������� � ��������� ������:</p></div>
    <table class="row" style="text-align: center;">
        <thead class="motivation-th">
            <tr>
                <th class="motivation-th motivation-cells"></th>
                <th class="motivation-th motivation-cells">������, ���.</th>
                <th class="motivation-th motivation-cells">�������� ���������, ���.</th>
                <th class="motivation-th motivation-cells">������ ������, ���.</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="motivation-left-column motivation-cells">������� <?php echo (int)$log['prirost_plan1']; ?>%</td>
                <td class="motivation-cells"><?php echo number_format($log['cel_1'], 0, ',', ' '); ?></td>
                <td class="motivation-cells"><?php if(is_numeric($log['otgruzki_plan1'])){echo number_format($log['otgruzki_plan1'], 0, ',', ' ');}else{echo $log['otgruzki_plan1'];}; ?></td>
                <td class="motivation-cells"><?php if(is_numeric($log['bonus_plan1'])){echo number_format($log['bonus_plan1'], 0, ',', ' ');}else{echo $log['bonus_plan1'];}; ?></td>
            </tr>
            <tr>
                <td class="motivation-left-column motivation-cells">������� <?php echo (int)$log['prirost_plan2']; ?>%</td>
                <td class="motivation-cells"><?php echo number_format($log['cel_2'], 0, ',', ' '); ?></td>
                <td class="motivation-cells"><?php if(is_numeric($log['otgruzki_plan2'])){echo number_format($log['otgruzki_plan2'], 0, ',', ' ');}else{echo $log['otgruzki_plan2'];}; ?></td>
                <td class="motivation-cells"><?php if(is_numeric($log['bonus_plan2'])){echo number_format($log['bonus_plan2'], 0, ',', ' ');}else{echo $log['bonus_plan2'];}; ?></td>
            </tr>
            <tr>
                <td class="motivation-left-column motivation-cells">������� <?php echo (int)$log['prirost_plan3']; ?>%</td>
                <td class="motivation-cells"><?php echo number_format($log['cel_3'], 0, ',', ' '); ?></td>
                <td class="motivation-cells"><?php if(is_numeric($log['otgruzki_plan3'])){echo number_format($log['otgruzki_plan3'], 0, ',', ' ');}else{echo $log['otgruzki_plan3'];}; ?></td>
                <td class="motivation-cells"><?php if(is_numeric($log['bonus_plan3'])){echo number_format($log['bonus_plan3'], 0, ',', ' ');}else{echo $log['bonus_plan3'];}; ?></td>
            </tr>
            <tr>
                <td class="motivation-left-column motivation-cells">������� <?php echo (int)$log['prirost_plan4']; ?>%</td>
                <td class="motivation-cells"><?php echo number_format($log['cel_4'], 0, ',', ' '); ?></td>
                <td class="motivation-cells"><?php if(is_numeric($log['otgruzki_plan4'])){echo number_format($log['otgruzki_plan4'], 0, ',', ' ');}else{echo $log['otgruzki_plan4'];}; ?></td>
                <td class="motivation-cells"><?php if(is_numeric($log['bonus_plan4'])){echo number_format($log['bonus_plan4'], 0, ',', ' ');}else{echo $log['bonus_plan4'];}; ?></td>
            </tr>
        </tbody>
    </table>
    <?php }else{ ?>
    <div class="row">
        ��� ����������!
    </div>
    <?php } ?>
</div>
