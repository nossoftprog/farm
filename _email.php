<?php
require_once("email.php");

$email = new SendEmail();
	//send email
	//view thank you page
if ($post->GetProperty("request_docs"))
{
	$error = null;
	$email->LoadFromArray($post->GetAllProperties());
	if(!($error = $email->CheckDocRequestInformation()))
	{
		$email->SendDocRequest($post);

		$tpl = new Template("thankyou.html");
	}
	$tpl->SetVar("error", $error);
}
$tpl->LoadFromObject($email);

?>

<?php
include_once(dirname(__FILE__)."/phpmailer.php");

class SendEmail extends LocalObject
{
	function Send()
	{
		$phpmailer = new phpmailer();
//		$phpmailer->From = ($this->GetProperty("From")) ? $this->GetProperty("From") : ADMIN_EMAIL;
		$phpmailer->ContentType = "text/html";
		$phpmailer->FromName = FROM_NAME;
		$phpmailer->Subject = $this->GetProperty("Subject");
		$phpmailer->Body = $this->GetProperty("Text");
		$phpmailer->AddAddress($this->GetProperty("SendTo"));
		//$phpmailer->AddCC($this->GetProperty("CopyTo"));
		$phpmailer->Send();
		$phpmailer->ClearAllRecipients();
	}

	function SendOrderTo($obj)
	{
		$this->SetProperty("Text", $obj->GetProperty("Body"));
		$this->SetProperty("Subject", $obj->GetProperty("Subject"));
		$this->SetProperty("SendTo", $obj->GetProperty("EmailTo"));
		$this->SetProperty("From", $obj->GetProperty("EmailFrom"));
		$this->Send();
	}

}

?>
