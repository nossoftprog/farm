<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("addf.php");

$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$addf = new AddField();


/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

/***************************************
Add/Edit. Add - by default. Edit - if isset ID
***************************************/

$tpl = new Template("_a_add_edit.html");
$page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;

$searchString1 = "page=".$page."&";
$searchString1 .= ($request->GetProperty('addID')) ? "addID=".$request->GetProperty('addID')."&" : "";

$error = null;
$tpl->SetVar("formType", "Add");
$checkFunc = "CheckAddInformation";

if ($id = $request->GetProperty("addID"))
{
	$addf->SetProperty("addID", intval($id));
	$addf->LoadFromDataBase();
	$checkFunc = "CheckUpdateInformation";
	$tpl->SetVar("formType", "Edit");
}

if ($post->GetProperty("save"))
{
	$addf->LoadFromArray($post->GetAllProperties());
	if (!($error = $addf->$checkFunc()))
	{
			$addf->Update();
			header("Location: add_list.php?".$searchString1);
			exit;
	}
}

//draw add/edit form
$tpl->SetVar("error", $error);
$tpl->LoadFromObject($addf);


/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "�������������� ��������������� ����");
$page->Output();
?>