<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("dez_sred.php");

$auth = new Authorization();
$tpl = new Template("_a_dez_sred.html");
$post = new LocalObject($_POST);
$files = new LocalObject($_FILES);

$dez = new DezSred();

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

if ($post->GetProperty("save"))
{
	$dez->LoadFromArray($files->GetProperty("name")); 
  $dez->Add();
}

$tpl->SetVar("UrlDezSred", HTTP_DEZ_SRED);

$page = Page::CreateForAdmin($tpl, $user, "Картинки");
$page->Output();
?>