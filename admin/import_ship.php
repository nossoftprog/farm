<?
error_reporting(E_ALL);
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
include_once('mysql.php'); 
require_once "ship.php";
require_once "XML2Array.php";
define('DEBUG', 1);

$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$ship = new Ship();

ini_set("magic_quotes_runtime", 0);
ini_set('max_execution_time', 99999);


/**************************
Validate user access rights
***************************/

$rights = Array(1,1,1,0,0,0);
if (!$user = $auth->Validate($rights))
{
	//header("Location: index.php");
	//exit;
}


  $dbc = new Mysql;

  $ftp_server = FTP_SERVER;
  $ftp_user_name = FTP_USER_NAME;
  $ftp_user_pass = FTP_USER_PASS;
  $name_dir = NAME_DIR_SHIP;

  //error_reporting(E_ALL | E_STRICT) ;
  //ini_set('display_errors', 'On');


  $conn_id = ftp_connect($ftp_server); 
  ftp_pasv($conn_id, true);
  // login � username � password
  $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass); 
    // ��������� ����������

  if ((!$conn_id) || (!$login_result)) { 
    $error = "������ ����������! CONNID=" . $conn_id . ", LOGINRESULT=" . $login_result;
    $pageContent = "������ �� ����������";
    echo $error;
    exit;
  } else {
    //echo "Connected to ftp".$ftp_server."....<br /><br />";
	}

             //���������� ������ ������ XML
  $files_ship = ftp_nlist($conn_id, $name_dir);
  sort($files_ship);

  if((count($files_ship) == 0) || !$files_ship) {
    echo "��� ������ � ����� ".$name_dir; 
    exit;
  }

  foreach($files_ship as $user){
    echo $user . "<br>\n";

    //$down = ftp_get($conn_id, "xml/input__".$user, $name_dir."/".$user, FTP_BINARY); 
    exec('wget -O ' . '../admin/xml/input__' . $user .  ' ftp://192.168.10.151/ship' . '/' .$user);
    exec('chmod 666 ' . 'xml/input__'.$user);
    $filefile = fread($fp = fopen("xml/input__".$user, 'r'), filesize("xml/input__".$user));
    fclose($fp);

          ob_end_flush();
          ob_flush();
          flush();
          ob_start();

    $temp_array=split("\.",$user);
    $name_file=$temp_array[0];
    $temp_array=split("_",$name_file);
    $userguid=$temp_array[1];
    $datedoc = $temp_array[2];
    $sql = "SELECT admins.* from users_ LEFT JOIN admins on users_.UserAdminID = admins.AdminCoflexID where UserName='" . $userguid . "'";
    $admin = get_results($sql);
    $adminname = $admin[0]['AdminName'];
    $adminemail = $admin[0]['AdminEmail'];
    $adminphone = $admin[0]['AdminPhone'];

    if(strstr($user, ".xml")){
      $filefile = iconv("cp1251","utf-8",$filefile);

      $goodxml = 1;
      try {
        $array = XML2Array::createArray($filefile);
      } catch(Exception $e) {
        $goodxml = 0;        
      }

      if ($goodxml == 1) {
      $begin = $array['summary_table']['@attributes']['data_begin'];
      $end =  $array['summary_table']['@attributes']['data_end'];
      if (strlen($begin) !=8 || strlen($end) !=8 ) { $goodxml = 0; }
      if ($goodxml == 1) {
      $begin = substr($begin, 0, 4) . "-" . substr($begin, 4, 2) . "-" . substr($begin, 6, 2);
      $end = substr($end, 0, 4) . "-" . substr($end, 4, 2) . "-" . substr($end, 6, 2);
      $sql = "delete from ship where ShipUserGUID='" . $userguid . "' AND ShipDate >= '" . $begin . "' AND ShipDate <= '" . $end . "'";
      update_results($sql);
      //echo "aaaa";
      $ok = 0;
      if ( isset($array['summary_table']['td'][0]) ) {  
        $root = $array['summary_table']['td'];
        $ok = 1;
      } else {
        if ( isset($array['summary_table']['td']) ) {
          $root = $array['summary_table'];
          $ok = 1;
        }
      }

      //echo"<pre>";print_r($array);echo"</pre>";
      if ($ok == 1) {
      foreach ($root as $code ) {
        if (isset($code['@attributes']['num'])) {
          $a = Array();
          $a['guid']=iconv("utf-8", "cp1251", $code['@attributes']['guid']);
          $a['num']=iconv("utf-8", "cp1251", $code['@attributes']['num']);
          $a['payment']=number_format(iconv("utf-8", "cp1251", str_replace(',','.',$code['@attributes']['payment'])), 2, '.', '');
          $a['date']=iconv("utf-8", "cp1251", $code['@attributes']['date']);
          $a['tk_name']=iconv("utf-8", "cp1251", str_replace("'",'',$code['@attributes']['tk_name']));
          $a['tk_site']=iconv("utf-8", "cp1251", $code['@attributes']['tk_site']);
          $a['tnn']=iconv("utf-8", "cp1251", $code['@attributes']['tnn']);
          $a['arrival_date']=iconv("utf-8", "cp1251", $code['@attributes']['arrival_date']);
          $a['fio_logist']=iconv("utf-8", "cp1251", $code['@attributes']['fio_logist']);
          $a['email_logist']=iconv("utf-8", "cp1251", $code['@attributes']['email_logist']);
          $a['tel_logist']=iconv("utf-8", "cp1251", $code['@attributes']['tel_logist']);
          $a['comment_logist']=iconv("utf-8", "cp1251", $code['@attributes']['comment_logist']);
          $a['urlico']=iconv("utf-8", "cp1251", str_replace("'",'',$code['@attributes']['ur_lico']));
          $a['weight']=iconv("utf-8", "cp1251", str_replace("'",'',$code['@attributes']['weight']));
          $a['volume']=iconv("utf-8", "cp1251", str_replace("'",'',$code['@attributes']['volume']));
          $a['places']=iconv("utf-8", "cp1251", str_replace("'",'',$code['@attributes']['places']));
          $sql = "SELECT * FROM ship WHERE ShipNUM='" . $a['num'] . "'";
          $add=count(get_results($sql));
          $ship->AddImport($a, $userguid, $datedoc, $adminname, $adminemail, $adminphone, $add);
        }
      }
      }
      ftp_delete($conn_id, $name_dir."/".$user);
      }
      }
    } 
  } 

  ftp_close($conn_id);
  echo "������ ��������";

function update_results($sql_) {
  global $dbc;
  $query = $sql_;
  return $dbc->executeQuery($query);
}

function get_results($sql_) {
  global $dbc;
  $query = $sql_;
  $dbc->executeQuery($query);
  if ($dbc->fetchResult()) {
    $result = $dbc->Result;
  } else {
    $result = array();
  }
  return $result;
}

function XMLread($user_file){
  $xml = new XMLReader(); 
  $xml->XML($user_file); 
  $xmluser = xml2assoc($xml); 
  $xml->close(); 
  //echo "<pre>";
  //var_dump($xmluser[0]); 
  //echo "</pre>";
  return $xmluser[0];
}


function xml2assoc($xml) { 
     $tree = null; 
     while($xml->read()) 
         switch ($xml->nodeType) { 
             case XMLReader::END_ELEMENT: return $tree; 
             case XMLReader::ELEMENT: 
                 $node = $xml->isEmptyElement ? '' : array($xml->name => xml2assoc($xml)); 
                 if($xml->hasAttributes) 
                     while($xml->moveToNextAttribute()) 
                         $node[$xml->name] = iconv("utf-8", "cp1251", $xml->value); 
                 $tree[] = $node; 
             break; 
             case XMLReader::TEXT: 
             case XMLReader::CDATA: 
                 $tree .= iconv("utf-8", "cp1251", $xml->value); 
         } 
     return $tree; 
 }  

function utf8_to_cp1251($s) {
  $tbl = $GLOBALS['unicode_to_cp1251_tbl'];
  $uc = 0;
  $bits = 0;
  $r = "";
  for($i = 0, $l = strlen($s); $i < $l; $i++) {
  $c = $s{$i};
  $b = ord($c);
  if($b & 0x80) {
  if($b & 0x40) {
  if($b & 0x20) {
  $uc = ($b & 0x0F) << 12;
  $bits = 12;
  }
  else {
  $uc = ($b & 0x1F) << 6;
  $bits = 6;
  }
  }
  else {
  $bits -= 6;
  if($bits) {
  $uc |= ($b & 0x3F) << $bits;
  }
  else {
  $uc |= $b & 0x3F;
  if($cc = @$tbl[$uc]) {
  $r .= $cc;
  }
  else {
  $r .= '?';
  }
  }
  }
  }
  else {
  $r .= $c;
  }
  }
  return $r;
}

?>


