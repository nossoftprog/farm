<?php
// Config

require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
//require_once("template.php");
require_once("product.php");
require_once("page.php");
include_once('mysql.php');
ini_set("display_errors", 1);
error_reporting(E_ALL ^ E_DEPRECATED);
//
$dir = $_SERVER['DOCUMENT_ROOT'].'/admin/xmldir';
$pattern = 'promo_*_*-*-*-*-*-*.xml';
$permitted = Array (1,2,3,4,5);
$ftp_server = FTP_SERVER;
$ftp_user_name = FTP_USER_NAME;
$ftp_user_pass = FTP_USER_PASS;
//$userType = $_SESSION['userType'];
$ftpdir = '/sale';
//

function dbJob($row){
    $stmt = GetStatement();
    
    $query = "SELECT * from motivation_data where god_tek ='".$row['god_tek']."' AND guid='".$row['guid']."'";
    $result = $stmt->FetchRow($query);
    if(!empty($result)){
       if ($result['prirost_fakt'] < $row['prirost_fakt']){
            $stmt = GetStatement();
            $query = "SELECT * FROM users_ WHERE CoflexID = '".$row['guid']."'";
            $userEmails = $stmt->FetchList($query);
            $stmt = GetStatement();
            $query = "SELECT * FROM motivated_users WHERE guid = '".$row['guid']."' LIMIT 1";
            $userData = $stmt->FetchList($query);
            $percent = 0;
            switch ($row['prirost_fakt']){
                case 15:
                    $percent = '1.5';
                    break;
                case 20:
                    $percent = '1.9';
                    break;
                case 25:
                    $percent = '2.2';
                    break;

                case 30:
                    $percent = '2.6';
                    break;
            }
            if(!empty($percent)){
                $tpl = new Template("motivation/mail/bonus_up.html");
                $tpl->SetVar("fio", $userData[0]['fio']);
                $tpl->SetVar("bonus_percent", $percent);
                $tpl->SetVar("bonus", number_format($row['bonus_fakt'], 0, ',', ' '));
                $tpl->SetVar("url", HTTP_URL.ROUTE_MOT_REGISTER);
                $message = $tpl->grab();
                $subject = '������ ���������: ��������� ����� '.$percent.'%';
                sendMotivationEmail($userEmails[0]['UserEmail'],$subject,$message);
                $user = getUserInfo($row['guid']);
                $admin = getAdminInfo($user['UserAdminID']);
                $tpl = new Template("motivation/mail/manager_bonus_up.html");
                $tpl->SetVar("company", $user['UserCompany']);
                $tpl->SetVar("admin", $admin['AdminName']);
                $tpl->SetVar("bonus_percent", $percent);
                $tpl->SetVar("bonus", number_format($row['bonus_fakt'], 0, ',', ' '));
                $message = $tpl->grab();
                $subject = '���������� '.$user['UserCompany'].' ������ ���������� ������ '.$percent.'%';
                sendMotivationEmail($userEmails[0]['UserAdminEmail'],$subject,$message);
            }
        }
        if(empty($result['data_vrucheniya']) && !empty($row['data_vrucheniya']) && !empty($result['bonus_type'])){
            $user = getUserInfo($row['guid']);
            $motUser = getMotUserInfo($row['guid']);
            $admin = getAdminInfo($user['UserAdminID']);
            $tpl = new Template("motivation/mail/get-bonus.html");
            $tpl->SetVar("fio", $motUser['fio']);
            $tpl->SetVar("bonus", number_format($motData['bonus_fakt'], 0, ',', ' '));
            $tpl->SetVar("bonusType", $result['bonus_type']);
            $message = $tpl->grab();
            $subject = '������ ���������: �����������! �� �������� �����';
            sendMotivationEmail($user['UserEmail'],$subject,$message);
            $tpl = new Template("motivation/mail/manager-get-bonus.html");
            $tpl->SetVar("company", $user['UserCompany']);
            $tpl->SetVar("admin", $admin['AdminName']);
            $tpl->SetVar("bonus", number_format($motData['bonus_fakt'], 0, ',', ' '));
            $tpl->SetVar("bonusType", $result['bonus_type']);
            $message = $tpl->grab();
            $subject = '������ ���������: ���������� '.$user['UserCompany'].' ������� �����';
            sendMotivationEmail($user['UserAdminEmail'],$subject,$message);
        }
    }
    
    if (!empty($row['data_moderacii'])){
        $stmt = GetStatement();
        $query = "SELECT * FROM motivated_users WHERE guid = '".$row['guid']."' LIMIT 1";
        $userData = $stmt->FetchList($query);
            if (!empty($userData)){
                if((empty($userData[0]['approved_at']))&&(empty($userData[0]['aborted_at']))){
                $stmt = GetStatement();
                $query = "SELECT * FROM users_ WHERE CoflexID = '".$row['guid']."'";
                $userEmails = $stmt->FetchList($query);
                $query = "UPDATE motivated_users SET approved_at ='".$row['data_moderacii']."'   WHERE guid = '".$row['guid']."'";
                $stmt->Execute($query);
                $tpl = new Template("motivation/mail/approved_user.html");
                $tpl->SetVar("fio", $userData[0]['fio']);
                $tpl->SetVar("url", HTTP_URL.ROUTE_MOT_REGISTER);
                $message = $tpl->grab();
                $subject = '������ ���������: �� ��������� ���������� ���������';
                sendMotivationEmail($userEmails[0]['UserEmail'],$subject,$message);
            }
        }
        
    }
    if (empty($result)){
        $query = "INSERT INTO motivation_data SET ";
        foreach ($row as $key=>$value){
            if(!((($key == 'bonus_type')||($key == 'bonus_date')||($key == 'data_moderacii')||($key == 'data_vrucheniya'))&&(empty($value)))){
                $query .= $key.'="'.$value.'", ';
            }
        }
        $query = substr($query,0,-2);
        $stmt->Execute($query);
    } else {
        $query = "UPDATE motivation_data SET ";
        foreach ($row as $key=>$value){
            if(!((($key == 'bonus_type')||($key == 'bonus_date')||($key == 'data_moderacii')||($key == 'data_vrucheniya'))&&(empty($value)))){
                $query .= $key.'="'.$value.'", ';
            }
        }
        $query = substr($query,0,-2);
        $query .= "where god_tek ='".$row['god_tek']."' AND guid='".$row['guid']."'";
        $stmt->Execute($query);
    }
    
}
function parseXML($xml){
       $content = simplexml_load_file($xml);
       
       foreach ($content->td as $td){
           $parsed = array();
           foreach ($td->attributes() as $attribute => $value){
               $value = iconv('UTF8','CP1251',$value); 
               $parsed[$attribute] = str_replace(',', '.', (string)$value);
            }
            dbJob($parsed);
       }
       unlink($xml);
       
    }

if (true) // !empty($userType)&&(in_array($userType, $permitted)) If user validation will realy need
{
    
    $conn_id = ftp_connect($ftp_server);
    ftp_pasv($conn_id, true);
    // login � username � password
    $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass); 
      // ��������� ����������
    if ((!$conn_id) || (!$login_result)) { 
            echo "������ ����������! CONNID=" . $conn_id . ", LOGINRESULT=" . $login_result."<br>";
            echo "������ �� ����������";
    }else{
        echo "���������� � ������� ".$ftp_server."....<br /><br />";
    }
    //��������� XML 
    $filelist = ftp_nlist($conn_id, $ftpdir);
        if ($filelist){
            foreach ($filelist as $file){
                $filename = explode('/', $file);
                $filename = $filename[count($filename)-1];
                if (fnmatch($pattern, $filename)){
                    if(ftp_get($conn_id, "xmldir/".$filename, $file, FTP_BINARY)){
                        echo '���� '.$filename.' ������.<br>';
                        ftp_delete($conn_id, $file);
                        echo '���� '.$filename.' ������ � �������.<br>';

                    } else {
                        echo '���� '.$filename.' �� ������� �������.<br>';
                    }
                } else {
                    //echo '� �������� �� ������� ��� ���������� ������.<br>';
                }
            }
        } else {
            echo '� �������� �� ������� ��� ���������� ������.<br>';
            //die();
        }
    
    $parseDir = opendir($dir);
    while($parsefilename = readdir($parseDir)){
        if (fnmatch($pattern, $parsefilename)){
            
            parseXML($dir.'/'.$parsefilename);
        }
    }
    
    
} else {
    echo '������ ��������';
    die();
}

