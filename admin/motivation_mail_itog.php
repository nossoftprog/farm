<?php

require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("product.php");
require_once("page.php");
include_once('mysql.php');
if (function_exists('sendMotivationEmail')) {
    echo "sendMotivationEmail ������� ��������.<br />\n";
}
$stmt = GetStatement();
$query = "SELECT * FROM motivation_data WHERE prirost_fakt >= 15 AND  bonus_date IS NULL AND god_tek = ".(MOT_YEAR-1);
$userData = $stmt->FetchList($query);
foreach ($userData as $user){
    $stmt = GetStatement();
    $query = "SELECT fio FROM motivated_users WHERE guid = '".$user['guid']."'";
    $userData = $stmt->FetchRow($query);
    $fio = $userData['fio'];
    $userData = getUserInfo($user['guid']);
    $address = $userData['UserEmail'];
    $tpl = new Template("motivation/mail/end_bonus.html");
    $tpl->SetVar("fio", $fio);
    switch ($user['prirost_fakt']){
        case 15:
            $percent = '1.5';
            break;
        case 20:
            $percent = '1.9';
            break;
        case 25:
            $percent = '2.2';
            break;
        
        case 30:
            $percent = '2.6';
            break;
    }
    $tpl->SetVar("bonus_percent", $percent);
    $tpl->SetVar("bonus", number_format($user['bonus_fakt'], 0, ',', ' '));
    $tpl->SetVar("url", HTTP_URL.ROUTE_CHOOSE_BONUS);
    $message = $tpl->grab();
    $subject = '������ ���������: �����������! �������� ��� ����� '.$percent.'%';
    if (empty($user['aborted_at'])){
        if (sendMotivationEmail($address,$subject, $message)) {
            echo '��������� '.$address.' ����������.';
        } else {
            echo '��������� '.$address.' �� ����������.';
        } 
    }
    $admin = getAdminInfo($userData['UserAdminID']);
    $tpl = new Template("motivation/mail/manager_end_bonus.html");
    $tpl->SetVar("company", $userData['UserCompany']);
    $tpl->SetVar("admin", $admin['AdminName']);
    $tpl->SetVar("bonus_percent", $percent);
    $tpl->SetVar("bonus", number_format($user['bonus_fakt'], 0, ',', ' '));
    $message = $tpl->grab();
    $subject = '������ ���������: ��������� ���������. ���������� '.$userData['UserCompany'].' ������ ������ '.$percent.'%';
    sendMotivationEmail($userData['UserAdminEmail'],$subject,$message);
    
}

///----LOSER
$stmt = GetStatement();
$query = "SELECT * FROM motivation_data WHERE prirost_fakt = 0 AND  bonus_date IS NULL AND god_tek = ".(MOT_YEAR-1);
$userData = $stmt->FetchList($query);
foreach ($userData as $user){
    $stmt = GetStatement();
    $query = "SELECT fio FROM motivated_users WHERE guid = '".$user['guid']."'";
    $userData = $stmt->FetchRow($query);
    $fio = $userData['fio'];
    $userData = getUserInfo($user['guid']);
    $address = $userData['UserEmail'];
    $tpl = new Template("motivation/mail/end_bonus_loser.html");
    $tpl->SetVar("fio", $fio);
    $tpl->SetVar("url", HTTP_URL.ROUTE_MOT_REGISTER);
    $message = $tpl->grab();
    $subject = '������ ���������: ���������  ���������';
    
    if (empty($user['aborted_at'])){
        if (sendMotivationEmail($address,$subject, $message)) {
            echo '��������� '.$address.' ����������.';
        } else {
            echo '��������� '.$address.' �� ����������.';
        }
    }
    $admin = getAdminInfo($userData['UserAdminID']);
    $tpl = new Template("motivation/mail/manager_end_bonus_loser.html");
    $tpl->SetVar("company", $userData['UserCompany']);
    $tpl->SetVar("admin", $admin['AdminName']);
    $message = $tpl->grab();
    $subject = '������ ���������: ���������� '.$userData['UserCompany'].' �� ������ ������';
    sendMotivationEmail($userData['UserAdminEmail'],$subject,$message);
}


