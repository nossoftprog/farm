<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("ost.php");


$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$ost = new Ost();


/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

/***************************************
Add/Edit. Add - by default. Edit - if isset ID
***************************************/

$tpl = new Template("_a_ost_edit.html");
$page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;

$searchString1 = "page=".$page."&";
$searchString1 .= ($request->GetProperty('ostID')) ? "ostID=".$request->GetProperty('ostID')."&" : "";

$error = null;
$tpl->SetVar("formType", "Add");
$tpl->SetVar("ostValue", "1");
$tpl->SetVar("ostMaxValue", "1000000");
$checkFunc = "CheckAddInformation";

if ($id = $request->GetProperty("ostID"))
{
	$ost->SetProperty("ostID", intval($id));
	$ost->LoadFromDataBase();
	$checkFunc = "CheckUpdateInformation";
	$tpl->SetVar("formType", "Edit");
}

if ($post->GetProperty("save"))
{
	$ost->LoadFromArray($post->GetAllProperties());
	if (!($error = $ost->$checkFunc()))
	{
			$ost->Update();
			header("Location: filtr_list.php?".$searchString1);
			exit;
	}
}

//draw add/edit form
$tpl->SetVar("error", $error);
$tpl->LoadFromObject($ost);


/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "�������������� ��������������� ����");
$page->Output();
?>