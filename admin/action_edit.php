<?php




require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("action.php");
require_once(dirname(__FILE__)."/../fckeditor/fckeditor.php");


$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$action = new Action();


/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

/***************************************
Add/Edit. Add - by default. Edit - if isset ID
***************************************/

$tpl = new Template("_a_action_edit.html");
$page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;

$searchString1 = "page=".$page."&";
$searchString1 .= ($request->GetProperty('ActionID')) ? "ActionID=".$request->GetProperty('ActionID')."&" : "";

$error = null;
$tpl->SetVar("formType", "Add");
$checkFunc = "CheckAddInformation";

if ($id = $request->GetProperty("ActionID"))
{
	$action->SetProperty("ActionID", intval($id));
	$action->LoadFromDataBase();
	$checkFunc = "CheckUpdateInformation";
	$tpl->SetVar("formType", "Edit");
}

if ($post->GetProperty("save"))
{
	$action->LoadFromArray($post->GetAllProperties());
	if (!($error = $action->$checkFunc()))
	{
			$action->Update();
			header("Location: actions_list.php?".$searchString1);
			exit;
	}
}

//fck editor 1
$fck = new FCKeditor("ActionDesc");
$fck->basePath = FCK_BASE_PATH;
$fck->width = '100%';
$fck->height = '400';
$fck->value = $action->GetProperty('ActionDesc');
$tpl->setVar("WYSIWYG_NewsContent", $fck->CreateHtml());


//draw add/edit form
$tpl->SetVar("error", $error);
$tpl->LoadFromObject($action);


//list of langs
$url = "action_edit.php";
$tpl->SetVar("URL", $url);
$tpl->SetVar("searchString1", $searchString1);


/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "�������������� �����");
$page->Output();
?>
