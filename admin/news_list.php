<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("newslist.php");
require_once("paging.php");

$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$newslist = new NewsList();
$corporate = 0;

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
} 

$tpl = new Template("_a_news_list.html");

$page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;
$error = null;

$searchString = "";
$tpl->SetVar("searchString", $searchString);

/**************
Delete objects
**************/
if ($request->GetProperty("op") == "delete_obj")
{
	$newslist->Delete($request->GetProperty("newIds"));
	header("Location: news_list.php?".$searchString);
	exit;
}

if ($request->GetProperty('sortMove') && $request->GetProperty('NewsID'))
{
	$newslist->MoveField($request->GetProperty('NewsID'), $request->GetProperty('sortMove'));
}


$newslist->LoadFromDataBase($page);
$tpl->LoadFromObjectsList("newslist", $newslist);

//paging
$paging  = new Paging($page, $newslist->GetTotalCount(), $tpl);

//list of langs
$url = "news_list.php";
$tpl->SetVar("URL", $url);
$searchString1 = "page=".$page."&";
$tpl->SetVar("searchString1", $searchString1);

$tpl->SetVar("error", $error);

/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "�������");
$page->Output();
?>