<?php
// Config

require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("product.php");
require_once("page.php");
include_once('mysql.php');
ini_set("display_errors", 1);
error_reporting(E_ALL ^ E_DEPRECATED);
//
$dir = $_SERVER['DOCUMENT_ROOT'].'/admin/xmldir/export';
$pattern = 'promo_*_*-*-*-*-*-*.xml';
$permitted = Array (1,2,3,4,5);
$ftp_server = FTP_SERVER;
$ftp_user_name = FTP_USER_NAME;
$ftp_user_pass = FTP_USER_PASS;
//$userType = $_SESSION['userType'];
$ftpdir = '/sale/export';
//

function utf8_converter($array)
{
    array_walk_recursive($array, function(&$item, $key){
        if(!mb_detect_encoding($item, 'utf-8', true)){
                $item = iconv('CP1251','UTF8',$item);
        }
    });
 
    return $array;
}
function dbJob(){
    $stmt = GetStatement();
    
    $query = "SELECT m.cod_contragenta, u.created_at, m.bonus_date, u.aborted_at, m.bonus_type, u.guid FROM motivated_users AS u LEFT JOIN motivation_data AS m ON u.guid=m.guid WHERE (m.god_tek = ".MOT_YEAR." OR m.god_tek = ".(MOT_YEAR-1)." OR m.god_tek IS NULL) AND (u.aborted_at IS NOT NULL OR (m.bonus_date IS NOT NULL AND m.data_vrucheniya IS NULL))";
    $result = $stmt->FetchList($query);
    return $result;
}

function createXML($dir,$attributes){
    $dom = new domDocument("1.0", "utf-8");
    $root = $dom->createElement("summary_table");
    $dom->appendChild($root);
    foreach ($attributes as $row){
        $td = $dom->createElement("td");
        foreach ($row as $attribute => $value){
            $td->setAttribute($attribute, $value);
        }
        $root->appendChild($td);
    }
    $filename = "/export_".MOT_YEAR."_".date('Y-m-d-H-i-s').".xml";
    $dom->save($dir.$filename);
    return $filename;
    }

function sendToFtp($ftp_server,$ftp_user_name,$ftp_user_pass,$dir,$ftpdir,$filename){
    $conn_id = ftp_connect($ftp_server);
    ftp_pasv($conn_id, true);
    // login � username � password
    $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass); 
      // ��������� ����������
    if ((!$conn_id) || (!$login_result)) { 
            echo "������ ����������! CONNID=" . $conn_id . ", LOGINRESULT=" . $login_result."<br>";
            echo "������� �� ����������";
    }else{
        echo "���������� � ������� ".$ftp_server."....<br /><br />";
    }
    if (ftp_put($conn_id,$ftpdir.$filename,$dir.$filename,FTP_BINARY)){
        //ftp_chmod($conn_id, 0666, $ftpdir.$filename); //not work on windows server
        echo '���� '.$filename.' ������� �������� �� FTP ������';
        unlink($dir.$filename);
    } else {
        echo '������ �������� ����� �� FTP ������';
    }
    
}

if (true) // !empty($userType)&&(in_array($userType, $permitted)) If user validation will realy need
{
    $attributes = dbJob();
    $attributes = utf8_converter($attributes);
    $filename = createXML($dir,$attributes);
    sendToFtp($ftp_server,$ftp_user_name,$ftp_user_pass,$dir,$ftpdir,$filename);
} else {
    echo '������ ��������';
    die();
}

