<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");

$auth = new Authorization();
$tpl = new Template("login.html");
$post = new LocalObject($_POST);

if ($auth->Logout())
{
	header("Location: index.php");
	exit;
}

$authfile = Array();
$authfile[1] = SUPERADMIN_FIRST_FILE;
$authfile[2] = ADMIN_FIRST_FILE;
$authfile[3] = MANAGER_FIRST_FILE;
$authfile[4] = EDITOR_FIRST_FILE;
$authfile[5] = PERSON_FIRST_FILE;
//echo $authfile[1];
//exit;

$user = $auth->GetUser();
$rights = Array(1,1,1,1,1,1);
if ($auth->Validate($rights))
{
	header("Location: ".$authfile[$_SESSION["userType"]]);
	exit;
} else {
	$tpl->SetVar('login', $post->GetProperty("Login")); 
	$tpl->SetVar('error', $auth->GetErrorMessage());
}

$page = Page::CreateForAdmin($tpl, $user);
$page->Output();
?>