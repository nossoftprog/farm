<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("vacancylist.php");
require_once("paging.php");

$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$vacancy = new VacancyList();
$corporate = 0;

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,1);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

$tpl = new Template("_a_vacan_list.html");

$page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;
$error = null;

$searchString = "";
$tpl->SetVar("searchString", $searchString);

/**************
Delete objects
**************/
if ($request->GetProperty("op") == "delete_obj")
{
	$vacancy->Delete($request->GetProperty("newIds"));
	header("Location: vacan_list.php?".$searchString);
	exit;
}

if ($request->GetProperty('sortMove') && $request->GetProperty('vacID'))
{
	$vacancy->MoveField($request->GetProperty('vacID'), $request->GetProperty('sortMove'));
}


$vacancy->LoadFromDataBase($page);
$tpl->LoadFromObjectsList("vacancylist", $vacancy);

//paging
$paging  = new Paging($page, $vacancy->GetTotalCount(), $tpl);

//list of langs
$url = "vacan_list.php";
$tpl->SetVar("URL", $url);
$searchString1 = "page=".$page."&";
$tpl->SetVar("searchString1", $searchString1);

$tpl->SetVar("error", $error);

/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "Вакансии");
$page->Output();
?>