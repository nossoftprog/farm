<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("paging.php");
require_once("productlist.php");
require_once("userlist.php");
require_once("adminlist.php");

$products = new ProductList();
$users = new UserList();
$managers = new AdminList();

$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,1);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

$results_sum = new LocalObjectList();

$p_select = "";
$u_select = "";
$m_select = "";

$tpl = new Template("_a_stat_list1.html");
$summa1 = "";
if ($post->GetProperty("save"))
{
  $tpl->SetVar("StartDate", $post->GetProperty("StartDate"));
  $tpl->SetVar("EndDate", $post->GetProperty("EndDate"));
  $p_select = $post->GetProperty("priceID");
  $u_select = $post->GetProperty("UserID");
  $m_select = $post->GetProperty("AdminCoflexID");

  $summa1 = 0;
  $addtables = "";
  $addwhere = "";
  $addtables_1 = "";
  $addwhere_1 = "";
  $fields_ = "";
  $groupby_ = "";

  if ($post->GetProperty("priceID") != "0") 
  {
    $addtables = $addtables . ", price";
    $addwhere = $addwhere . " AND price.priceID=".$post->GetProperty("priceID");
  }

  if ($post->GetProperty("UserID") != "0") 
  {
    $addtables = $addtables . ", users_";
    $addwhere = $addwhere . " AND users_.UserID=".$post->GetProperty("UserID");
  }

  if ($post->GetProperty("AdminCoflexID") != "0") 
  {
    if ($post->GetProperty("UserID") != "0")
    {
      $addtables = $addtables . ", admins";
      $addwhere = $addwhere . " AND admins.AdminCoflexID=".$post->GetProperty("AdminCoflexID");
    } else {
      $addtables = $addtables . ", admins, users_";
      $addwhere = $addwhere . " AND admins.AdminCoflexID=".$post->GetProperty("AdminCoflexID");
    }
  }

  if ($post->GetProperty("StartDate") != "") 
  {
    $dates = split("-", $post->GetProperty('StartDate'));
    $addwhere = $addwhere . " AND orders_.OrderDateCreated >= '".$dates[2]."-".$dates[1]."-".$dates[0]."'";
  }

  if ($post->GetProperty("EndDate") != "") 
  {
    $dates = split("-", $post->GetProperty('EndDate'));
    $addwhere = $addwhere . " AND orders_.OrderDateCreated <= '".$dates[2]."-".$dates[1]."-".$dates[0]."'";
  }

  if ($post->GetProperty("calc2") != "") 
  {
//    $fields_ = ", AdminName";
   $fields_ = ", count(DISTINCT HistUserID) as users, count(DISTINCT HistOrderID) as orders, AdminName";
    $groupby_ = " GROUP by AdminName";
  }

  if ($post->GetProperty("calc3") != "") 
  {
    $fields_ = ", AdminName, UserCompany";
    $groupby_ = " GROUP by AdminName, UserCompany";
  }

  if ($post->GetProperty("calc4") != "") 
  {
    $fields_ = ", AdminName, UserCompany, PriceDesc";
    $groupby_ = " GROUP by AdminName, UserCompany, PriceDesc";
  }

  $having = " HAVING summa>".$post->GetProperty("SUMMA1");

  $query2 = "SELECT FORMAT(SUM(HistPriceSum),2) as summa FROM history_, orders_".$addtables." WHERE orders_.OrderSubmitted='0' AND history_.HistOrderID=orders_.OrderID".$addwhere;

  $query = "SELECT FORMAT(SUM(HistPriceSum),2) as summa FROM history_, orders_, price, users_, admins WHERE orders_.OrderSubmitted='0' AND history_.HistOrderID=orders_.OrderID AND PriceHistory=1 AND price.priceID=history_.HistPriceID AND users_.UserID=orders_.OrderUserID AND (users_.UserAdminID=admins.AdminCoflexID OR users_.UserAdminID=admins.AdminID)".$addwhere;

  //echo($query);
  //exit;

  $conn = GetConnection();
  $stmt = $conn->CreateStatement();
  $rs = $stmt->Execute($query);
  //echo($stmt->GetNumRows());
  //exit;
  if ($stmt->GetNumRows())
  {
    if ($row = $rs->NextRow())
    {
      if (isset($row['summa']))
      {
        $summa1 = $row['summa'];
      }
    }
  }  

  if ($post->GetProperty("calc1") == "") {
    $query = "SELECT FORMAT(SUM(HistPriceSum),2) as summa".$fields_." FROM history_, orders_, price, users_, admins WHERE orders_.OrderSubmitted='0' AND history_.HistOrderID=orders_.OrderID AND PriceHistory=1 AND price.priceID=history_.HistPriceID AND users_.UserID=orders_.OrderUserID AND (users_.UserAdminID=admins.AdminCoflexID OR users_.UserAdminID=admins.AdminID)".$addwhere.$groupby_.$having;

    //echo($query);

    $results_sum->LoadFromSQL($query);

    if ($post->GetProperty("calc2") != "") {
      $totalusers = 0;
      $totalorders = 0;
	foreach($results_sum->items as $res_item){
	      $totalusers  += $res_item['users'];
	      $totalorders += $res_item['orders'];
	}
      $tpl->SetVar("summausers", $totalusers);
      $tpl->SetVar("summaorders", $totalorders);
      
      $tpl->LoadFromObjectsList("results_sum2", $results_sum);
    }
    if ($post->GetProperty("calc3") != "") {
      $tpl->LoadFromObjectsList("results_sum3", $results_sum);
    }
    if ($post->GetProperty("calc4") != "") {
      $tpl->LoadFromObjectsList("results_sum4", $results_sum);
    }
  }

}

$summa_ = 0;
if (isset($post)) {
  $summa_ = $post->GetProperty("SUMMA1");
}

$tpl->SetVar("SUMMA", $summa1);
$tpl->SetVar("SUMMA1", $summa_);


$products->LoadFromDataBaseStatistic($p_select);
$users->LoadFromDataBase($u_select);
$managers->LoadFromDataBaseManager("'".$m_select."'");

$tpl->LoadFromObjectsList("results_sum", $results_sum);
$tpl->LoadFromObjectsList("productslist", $products);
$tpl->LoadFromObjectsList("userslist", $users);
$tpl->LoadFromObjectsList("adminslist", $managers);

$page = Page::CreateForAdmin($tpl, $user, "Статистика");
$page->Output();
?>