<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("product.php");
require_once("metodlist.php");
require_once("pricestatuslist.php");
require_once(dirname(__FILE__)."/../fckeditor/fckeditor.php");


$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$price = new Product();
$metodlist = array();
$pricestatuslist = array();
$corporate=0;



/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,1,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

/***************************************
Add/Edit. Add - by default. Edit - if isset ID
***************************************/

$tpl = new Template("_a_product_edit.html");
$page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;

$searchString1 = "page=".$page."&";
$searchString1 .= ($request->GetProperty('priceID')) ? "priceID=".$request->GetProperty('priceID')."&" : "";

$error = null;
$tpl->SetVar("formType", "Add");
$checkFunc = "CheckAddInformation";

if ($id = $request->GetProperty("priceID"))
{
	$price->SetProperty("priceID", $id);
	$price->LoadFromDataBaseAdd();
	$checkFunc = "CheckUpdateInformation";
	$tpl->SetVar("formType", "Edit");
  $metodlist = new MetodList();
  $metodlist->LoadFromDataBase($price->GetProperty("priceAddmetID"));
  $tpl->LoadFromObjectsList("metodlist", $metodlist);
  $pricestatuslist = new PriceStatusList();
  $pricestatuslist->LoadFromDataBase1($price->GetProperty("priceAddTemp"));
  $tpl->LoadFromObjectsList("pricestatuslist", $pricestatuslist);
  $tpl->SetVar("priceAddTemp", $price->GetProperty("priceAddTemp"));
}

if ($post->GetProperty("save"))
{
	$price->LoadFromArray($post->GetAllProperties());
	if (!($error = $price->$checkFunc()))
	{
			$price->Update();
			header("Location: product_list.php?".$searchString1);
			exit;
	}
}

//fck editor 1
$fck = new FCKeditor("priceAddDesc");
$fck->basePath = FCK_BASE_PATH;
$fck->width = '100%';
$fck->height = '400';
$fck->value = $price->GetProperty('priceAddDesc');
$tpl->setVar("WYSIWYG_NewsContent", $fck->CreateHtml());

$fck = new FCKeditor("PriceAddTech");
$fck->basePath = FCK_BASE_PATH;
$fck->width = '100%';
$fck->height = '400';
$fck->value = $price->GetProperty('PriceAddTech');
$tpl->setVar("WYSIWYG_NewsContent1", $fck->CreateHtml());


//fck editor for files
$fck = new FCKeditor("PriceAddFiles");
$fck->basePath = FCK_BASE_PATH;
$fck->width = '100%';
$fck->height = '150';
$fck->value = $price->GetProperty('PriceAddFiles');
$tpl->setVar("WYSIWYG_priceAddFiles", $fck->CreateHtml());


//draw add/edit form
$tpl->SetVar("error", $error);
$tpl->LoadFromObject($price);


//list of langs
$url = "price_edit.php";
$tpl->SetVar("URL", $url);
$tpl->SetVar("searchString1", $searchString1);


/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "�������������� ������");
$page->Output();
?>