<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("metod.php");

$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$metod = new Metod();
$files = new LocalObject($_FILES);

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

/***************************************
Add/Edit. Add - by default. Edit - if isset ID
***************************************/

$tpl = new Template("_a_metod_edit.html");
$error = null;
$tpl->SetVar("formType", "Add");
$checkFunc = "CheckAddInformation";

if ($id = $request->GetProperty("metID"))
{
	$metod->SetProperty("metID", intval($id));
	$metod->LoadFromDataBase();
	$checkFunc = "CheckUpdateInformation";
	$tpl->SetVar("formType", "Edit");
}

if ($post->GetProperty("save"))
{
	$metod->LoadFromArray($post->GetAllProperties());
  $metod->SetProperty("metName", $post->GetProperty('metName'));
	
  if ($post->GetProperty('AddDoc'))
	{
		$fileName = Add();
		$metod->SetProperty("metDoc", $fileName);
	}
	if (!$error)
  {
		$metod->Update();
		header("Location: metod_list.php?".$searchString1);
		exit;
	}
}

  function Add()
	{
    global $files;
		if (move_uploaded_file($_FILES['nameDoc']['tmp_name'], DIR_DOCS.$_FILES['nameDoc']['name']))
		{
			return $_FILES['nameDoc']['name'];
		} else {
      return false;
    }
	}


//draw add/edit form
$tpl->SetVar("error", $error);
$tpl->LoadFromObject($metod);


/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "�������������� ������������� ��������");
$page->Output();
?>