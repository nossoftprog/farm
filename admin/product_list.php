<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("productlist.php");
require_once("paging.php");

global $sess;
$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$products = new ProductList();

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,1,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

$tpl = new Template("_a_product_list.html");

$page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;
$error = null;

if (isset($_POST['searchProd']))
{
  $_SESSION['searchProd'] = $_POST['searchProd'];
  $page = 1;
}

$searchString = "";
$tpl->SetVar("searchString", $searchString);
if (isset($_SESSION['searchProd'])) 
{
   $tpl->SetVar("searchProd", $_SESSION['searchProd']);
}

$products->LoadFromDataBase($page);
$tpl->LoadFromObjectsList("productlist", $products);

//paging
$paging  = new Paging($page, $products->GetTotalCount(), $tpl);

//list 
$url = "product_list.php";
$tpl->SetVar("URL", $url);
$searchString1 = "page=".$page."&";
$tpl->SetVar("searchString1", $searchString1);

$tpl->SetVar("error", $error);

/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "������");
$page->Output();
?>