<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("ostlist.php");
require_once("paging.php");

$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$ostlist = new OstList();

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

$tpl = new Template("_a_ost_list.html");

$page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;
$error = null;

$searchString = "";
$tpl->SetVar("searchString", $searchString);

/**************
Delete objects
**************/
if ($request->GetProperty("op") == "delete_obj")
{
	$ostlist->Delete($request->GetProperty("newIds"));
	header("Location: filtr_list.php?".$searchString);
	exit;
}

$ostlist->LoadFromDataBase($page);
$tpl->LoadFromObjectsList("ostlist", $ostlist);

//paging
$paging  = new Paging($page, $ostlist->GetTotalCount(), $tpl);

//list of langs
$url = "filtr_list.php";
$tpl->SetVar("URL", $url);
$searchString1 = "page=".$page."&";
$tpl->SetVar("searchString1", $searchString1);

$tpl->SetVar("error", $error);

/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "������ ��������");
$page->Output();
?>