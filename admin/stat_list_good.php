<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("paging.php");
require_once("productlist.php");
require_once("userlist.php");
require_once("adminlist.php");

$products = new ProductList();
$users = new UserList();
$managers = new AdminList();

$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,1);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

$p_select = "";
$u_select = "";
$m_select = "";

$tpl = new Template("_a_stat_list.html");

if ($post->GetProperty("save"))
{
  $tpl->SetVar("StartDate", $post->GetProperty("StartDate"));
  $tpl->SetVar("EndDate", $post->GetProperty("EndDate"));
  $p_select = $post->GetProperty("priceID");
  $u_select = $post->GetProperty("UserID");
  $m_select = $post->GetProperty("AdminID");

  $summa1 = 0;
  $addtables = "";
  $addwhere = "";
  $addtables_1 = "";
  $addwhere_1 = "";

  if ($post->GetProperty("priceID") != "0") 
  {
    $addtables = $addtables . ", price";
    $addtables_1 = $addtables_1 . ", price";
    $addwhere = $addwhere . " AND PriceHistory=1 AND price.priceID=history_.HistPriceID AND price.priceID=".$post->GetProperty("priceID");
    $addwhere_1 = $addwhere_1 . " AND PriceHistory=1 AND price.priceID=history_.HistPriceID";
  }

  if ($post->GetProperty("UserID") != "0") 
  {
    $addtables = $addtables . ", users_";
    $addtables_1 = $addtables_1 . ", users_";
    $addwhere = $addwhere . " AND users_.UserID=orders_.OrderUserID AND users_.UserID=".$post->GetProperty("UserID");
    $addwhere_1 = $addwhere_1 . " AND users_.UserID=orders_.OrderUserID";
  }

  if ($post->GetProperty("AdminID") != "0") 
  {
    if ($post->GetProperty("UserID") != "0")
    {
      $addtables = $addtables . ", admins";
      $addtables_1 = $addtables_1 . ", admins";
      $addwhere = $addwhere . " AND users_.UserAdminID=admins.AdminID AND admins.AdminID=".$post->GetProperty("AdminID");
      $addwhere_1 = $addwhere_1 . " AND users_.UserAdminID=admins.AdminID";
    } else {
      $addtables = $addtables . ", admins, users_";
      $addtables_1 = $addtables_1 . ", admins, users_";
      $addwhere = $addwhere . " AND users_.UserAdminID=admins.AdminID AND users_.UserID=orders_.OrderUserID AND admins.AdminID=".$post->GetProperty("AdminID");
      $addwhere_1 = $addwhere_1 . " AND users_.UserAdminID=admins.AdminID AND users_.UserID=orders_.OrderUserID";
    }
  }

  if ($post->GetProperty("StartDate") != "") 
  {
    $dates = split("-", $post->GetProperty('StartDate'));
    $addwhere = $addwhere . " AND orders_.OrderDateCreated >= '".$dates[2]."-".$dates[1]."-".$dates[0]."'";
    $addwhere_1 = $addwhere_1 . " AND orders_.OrderDateCreated >= '".$dates[2]."-".$dates[1]."-".$dates[0]."'";
  }

  if ($post->GetProperty("EndDate") != "") 
  {
    $dates = split("-", $post->GetProperty('EndDate'));
    $addwhere = $addwhere . " AND orders_.OrderDateCreated <= '".$dates[2]."-".$dates[1]."-".$dates[0]."'";
    $addwhere_1 = $addwhere_1 . " AND orders_.OrderDateCreated <= '".$dates[2]."-".$dates[1]."-".$dates[0]."'";
  }

  $query = "SELECT SUM(HistPriceSum) as summa FROM history_, orders_".$addtables." WHERE orders_.OrderSubmitted='0' AND history_.HistOrderID=orders_.OrderID".$addwhere;

  //echo($query);
  //exit;

  $conn = GetConnection();
  $stmt = $conn->CreateStatement();
  $rs = $stmt->Execute($query);
  if ($stmt->GetNumRows())
  {
    if ($row = $rs->NextRow())
    {
      if (isset($row['summa']))
      {
        $summa1 = $row['summa'];
      }
    }
  }
}

$tpl->SetVar("SUMMA", $summa1);

$products->LoadFromDataBaseStatistic($p_select);
$users->LoadFromDataBase($u_select);
$managers->LoadFromDataBaseManager("'".$m_select."'");

$tpl->LoadFromObjectsList("productslist", $products);
$tpl->LoadFromObjectsList("userslist", $users);
$tpl->LoadFromObjectsList("adminslist", $managers);

$page = Page::CreateForAdmin($tpl, $user, "Статистика");
$page->Output();
?>