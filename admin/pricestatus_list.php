<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("pricestatuslist.php");

$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$pricestatuslist = new PriceStatusList();

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

$error = null;
/**************
Delete objects
**************/
if ($request->GetProperty("op") == "delete_obj")
{
	$pricestatuslist->Delete($request->GetProperty("lanIds"));
	$error = $pricestatuslist->message;
}

$tpl = new Template("_a_pricestatus_list.html");
$pricestatuslist->LoadFromDataBase();
$tpl->LoadFromObjectsList("pricestatuslist", $pricestatuslist);
$tpl->SetVar("error", $error);

/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "������� ������");
$page->Output();
?>