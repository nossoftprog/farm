<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("accommodationlist.php");
require_once("region.php");

require_once (dirname(__FILE__)."/../ajax/config.php");
require_once (dirname(__FILE__)."/../ajax/Subsys/JsHttpRequest/Php.php");

$JsHttpRequest =& new Subsys_JsHttpRequest_Php("utf-8");
$request = new LocalObject($_REQUEST);
$accommodationlist = new AccommodationList();



if ($request->GetProperty('get_accom'))
{
	$accommodationlist->LoadAccommodationList($request->GetProperty('RegionID'));
	$_RESULT = array("Accommodations" => $accommodationlist->GetItemsArray());
}
elseif ($request->GetProperty('get_regionmap'))
{
	$region = new Region();
	list($mapPicture, $mapWidth, $mapHeight) = $region->GetRegionMap($request->GetProperty('RegionID'), $region->currLang);
	$_RESULT = array("MapPicture" => $mapPicture, "MapWidth" => $mapWidth, "MapHeight" => $mapHeight);
}
pre_print_r($_RESULT);

?>