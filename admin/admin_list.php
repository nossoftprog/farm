<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("adminlist.php");
require_once("paging.php");

$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$adminlist = new AdminList();

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,0,0,0,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

$error = null;
/**************
Delete objects
**************/
if ($request->GetProperty("op") == "delete_obj")
{
	$adminlist->Delete($request->GetProperty("lanIds"));
	$error = $adminlist->message;
}

//sort objects
if ($request->GetProperty('sortMove') && $request->GetProperty('AdminID'))
{
	$adminlist->MoveField($request->GetProperty('AdminID'), $request->GetProperty('sortMove'));
}

$page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;

$tpl = new Template("_a_admin_list.html");
$adminlist->LoadFromDataBase($page);
$tpl->LoadFromObjectsList("adminlist", $adminlist);
$tpl->SetVar("error", $error);

//paging
$paging  = new Paging($page, $adminlist->GetTotalCount(), $tpl);

//list
$url = "admin_list.php";
$tpl->SetVar("URL", $url);
$searchString1 = "page=".$page."&";
$tpl->SetVar("searchString1", $searchString1);

$tpl->SetVar("error", $error);

/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "��������������");
$page->Output();
?>