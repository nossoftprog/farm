<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("userlist.php");

$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$userlist = new UserList();

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

$error = null;
/**************
Delete objects
**************/
if ($request->GetProperty("op") == "delete_obj")
{
	$userlist->Delete($request->GetProperty("lanIds"));
	$error = $userlist->message;
}

if ($request->GetProperty("sort") != "")
{
	$_SESSION['ZakazSort'] = $request->GetProperty("sort");
}


$tpl = new Template("_a_user_list.html");
$userlist->LoadFromDataBase();
//print_r($userlist);

/*��������� ��������
foreach($userlist->items as $u){
	$text .= $u['CoflexID']."\t".$u['UserCompany']."\t".$u['UserLastName']." ".$u['UserFirstName']." ".$u['UserMiddleName']."\t".$u['UserEmail']."\t".$u['AdminName']."\t".$u['UserName']."\n";
}
    $user_file = fwrite($fp = fopen("users.csv", 'w'), $text);
    fclose($fp);
*/

$tpl->LoadFromObjectsList("userlist", $userlist);
$tpl->SetVar("error", $error);


/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "������������");
$page->Output();
?>