<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("metodlist.php");

$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$metodlist = new MetodList();

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

$error = null;
/**************
Delete objects
**************/
if ($request->GetProperty("op") == "delete_obj")
{
	$metodlist->Delete($request->GetProperty("lanIds"));
	$error = $metodlist->message;
}

$tpl = new Template("_a_metod_list.html");
$metodlist->LoadFromDataBase();
$tpl->LoadFromObjectsList("metodlist", $metodlist);
$tpl->SetVar("error", $error);

/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "Методические указания");
$page->Output();
?>