function MM_findObj(n, d) { 
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function MM_swapImgRestore() { 
  var i,x,a=document.MM_sr; 
  for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() {
  var d=document; 
  if(d.images){ 
    if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; 
    for(i=0; i<a.length; i++) {
      if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}
    }  
  }
}

function MM_swapImage() { 
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; 
  for(i=0;i<(a.length-2);i+=3) {
    if ((x=MM_findObj(a[i]))!=null){
      document.MM_sr[j++]=x; 
      if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];
    }
  }  
}

function mOvr(src,clrOver) { 
  if (!src.contains(event.fromElement)) { src.style.cursor = 'hand'; src.bgColor = clrOver;}
}

function mOut(src,clrIn) { 
  if (!src.contains(event.toElement)) { src.style.cursor = 'default'; src.bgColor = clrIn; }
} 

function MM_showHideLayers() {
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) {
    if ((obj=MM_findObj(args[i]))!=null) { 
      v=args[i+2];
      if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v='hide')?'hidden':v; }
      obj.visibility=v;
    }
   }
}    


function MM_validateEmail(s) { 
  var p, v, i, isValid;
  var forbiddenChar = new Array("," ," ", ";", "|", "'", '"');
  v = s;  p = val.indexOf('@');
  isValid =  !(p < 1 || p == (v.length-1) || v.lastIndexOf('.') > (v.length-3) 
       || v.lastIndexOf('.') < (v.length-5) || p != v.lastIndexOf('@') );
  if (!isValid) return false;
  for (i = 0; i < forbiddenChar.length; i++) {
    if ( v.indexOf(forbiddenChar[i]) >= 0 ) return false;
  }     
  return true;       
}

function MM_validatePassword(lg1,pwd,cpwd,nMin) { 
  var vpwd, vcpwd, errors = '';
  var lg, small_len, char_word, not_equal, error_mess, pwd_mess, cpwd_mess

  lg = lg1-1;
  small_len  = new Array(" must consist from not less "," ������ �������� �� ����� ");  
  char_word  = new Array(" characters"," ��������");
  not_equal  = new Array(" is not equal to "," �� ����� ");
  error_mess = new Array("The following error(s) occurred","���������� ��������� ������");
  pwd_mess   = new Array("Password","������");
  cpwd_mess  = new Array("Confirm Password","����������� ������");

  if (document.MM_returnValue) {
    vpwd = MM_findObj(pwd); vpwd = vpwd.value;
    vcpwd = MM_findObj(cpwd); vcpwd = vcpwd.value;
    if (vpwd.length < nMin )  errors += '- ' + pwd_mess[lg] + small_len[lg] + nMin + char_word[lg] +'.\n';
    if (vpwd != vcpwd)  errors += '- ' + pwd_mess[lg] + not_equal[lg] + cpwd_mess[lg] + '.\n';
    if (errors) alert(error_mess[lg] + ':\n'+errors);
    document.MM_returnValue = (errors == '');
  }  
}

function MM_validRadio(l, FormName, FieldName, FieldTitle) {
 var lg, obj, errors, err_msg  = new Array(" You must select one ","�� ������ ������� ");  
  lg = l-1;
  obj = eval("document." + FormName + "." + FieldName);
  for (var i = 0; i < obj.length; i++) { if ( obj[i].checked ) return true; }
  alert(err_msg[lg] +  FieldTitle); return false;
}


function MM_validateRadio(l, Form, e0, e1, s) {
  var lg, i, errors, err_msg  = new Array(" You must check one "," �� ������ �������� ���� ");  
  lg = l-1;
  errors = err_msg[lg] + s;
  if (document.MM_returnValue) {
    for (i = e0; i <= e1; i++) {
      if (document.forms(Form).elements[i].checked) { errors = '';  break;}
    }
    if (errors) alert(errors);
    document.MM_returnValue = (errors == '');
  }  
}

function MM_validateCheck(l, Form, e0, e1, s) {
  var lg, i, errors, err_msg  = new Array(" You must check at least one "," �� ������ �������� �� ������� ���� ���� ");  
  lg = l-1;
  errors = err_msg[lg] + s;
  if (document.MM_returnValue) {
    for (i = e0; i <= e1; i++) {
      if (document.forms(Form).elements[i].checked) { errors = '';  break;}
    }
    if (errors) alert(errors);
    document.MM_returnValue = (errors == '');
  }  
}

function MM_validateForm() { 
  var i, p, q, nm, test, num, s, min, max, errors = '', args = MM_validateForm.arguments;
  var lg, wrong_email, wrong_number, wrong_number_range, and_word, required_field, error_mess
  lg = args[0]-1;
  wrong_email        = new Array(" must contain an e-mail address"," ������ ��������� Email �����");  
  wrong_number       = new Array(" must contain a number more than zero"," ������ ��������� ����� ������� ����");
  wrong_int_number   = new Array(" must contain an integer number more than zero"," ������ ��������� ����� ����� ������� ����");
  wrong_int_number_zero = new Array(" must contain an integer number more or equal than zero"," ������ ��������� ����� ����� ������� ���� ������ ����");
  wrong_number_range = new Array(" must contain a number between "," ������ ��������� ����� ����� ");
  and_word           = new Array(" and "," � ");
  required_field     = new Array(" is required"," �������� ������������ ����� ��� �����");
  error_mess         = new Array("The following error(s) occurred","���������� ��������� ������");

  for (i = 1; i < (args.length-1); i+=3) { 
    test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { 
      nm = ( args[i+1] ) ? args[i+1] : val.name; 
      if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1 && val != "" ) { 
          if ( !MM_validateEmail(val) ) errors+='- '+nm+wrong_email[lg]+'.\n';
        } 
        else if (test!='R' && val != "") { 
          if (test.indexOf('isInt')!=-1 ) {
            num = parseInt(val);
            if (test.indexOf('isIntZero')!=-1 ) {
              if (val != num || num < 0) errors+='- '+nm+ wrong_int_number_zero[lg]+'.\n';
            }
            else {
              if (val != num || num <= 0) errors+='- '+nm+wrong_int_number[lg]+'.\n';
            }
          }
          else {
            num = parseFloat(val);
            if (val!=''+num || num <= 0) errors+='- '+nm+wrong_number[lg]+'.\n';
          } 
          if (test.indexOf('inRange') != -1) { 
            p=test.indexOf(':'); min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+wrong_number_range[lg]+min+and_word[lg]+max+'.\n';
          } 
        } 
      } 
      else if (test.charAt(0) == 'R') errors += '- '+nm+required_field[lg]+'.\n'; 
    }
  } 
  if (errors) alert(error_mess[lg]+':\n'+errors);
  document.MM_returnValue = (errors == '');
}

function page(pNum) {
	document.Search.Page.value=pNum;
	document.Search.submit();
}

function gotocatalog(cNum) {
	document.Search.ParentID.value=cNum;
	document.Search.submit();
}

function editcatalog(cNum) {
	document.AddRec.CatalogID.value=cNum;
	document.AddRec.submit();
}

function resort(s) {
	document.Search.Page.value="";
	document.Search.SortOrd.value=s;
	document.Search.submit();
}

function del(Id) {
   if (confirm("Delete ?")) {
      document.Search.Id.value=Id;
      document.Search.submit();
  }
}

function setfilter_Res() {
	document.Search.OperatedId.value = document.Filter.Operated.value;
	document.Search.OrderedId.value = document.Filter.Ordered.value;
	document.Search.ItemDateId1.value = document.Filter.ItemDate1.value;
	document.Search.ItemDateId2.value = document.Filter.ItemDate2.value;
	document.Search.submit();
}

function getControl(nForm, ctrlName) {
  return document.forms[nForm].elements[ctrlName];
}

function addList(nForm, element_src, element_dst) {
  var src, dst, i, j, dupe, o, newo;
  
  src = getControl(nForm, element_src);
  if (src == null) { alert('No selected items');  return }

  dst = getControl(nForm, element_dst);

  for (i = 0; i < src.options.length; i++) {
    o = src.options[i];
    if (o.selected) {
      o.selected = false;
      dupe = false;
      for (j = 0; j < dst.options.length && !dupe; j++) {
        if (dst.options[j].value == o.value) dupe = true;
      }    
      if (!dupe) {
        newo = document.createElement("OPTION");
        newo.text = o.text;
        newo.value = o.value;
        dst.options.add(newo);
        src.options.remove(i);
        i--;        
      }
    }
  }
  self.event.returnValue = false;   
}

function doSearch(nForm, element_dst) {
  var dst, i, o;
  if (!document.MM_returnValue) return false;
  dst = getControl(nForm, element_dst);
  if (dst.options.length == 0) {
    alert('No otutputed fields !');
    return false;
  }
  for (i = 0; i < dst.options.length; i++) {
    o = dst.options[i];
    o.selected = true;
  }
  return true;  
}

function clearRadio(nForm, element_dst) {
  var dst, i, o;
  dst = getControl(nForm, element_dst);
  for (i = 0; i < dst.length; i++) {
    o = dst[i];
    o.checked = false;
  }
  return true;  
}

