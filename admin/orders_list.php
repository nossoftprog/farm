<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("orderlist.php");
require_once("paging.php");

$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$orderlist = new OrderList();

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,1,0,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

$error = null;
/**************
Delete objects
**************/
if ($request->GetProperty("op") == "delete_obj")
{
	$orderlist->Delete($request->GetProperty("lanIds"));
	$error = $adminlist->message;
}

if ($request->GetProperty("sort") != "")
{
	$_SESSION['OrderSort'] = $request->GetProperty("sort");
}

$tpl = new Template("_a_order_list.html");
$page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;
$orderlist->LoadFromDataBase(1, $page);
$tpl->LoadFromObjectsList("orderlist", $orderlist);
$tpl->SetVar("error", $error);

//paging
$paging  = new Paging($page, $orderlist->GetTotalCount(1), $tpl);

//list
$url = "orders_list.php";
$tpl->SetVar("URL", $url);
$searchString1 = "page=".$page."&";
$tpl->SetVar("searchString1", $searchString1);

if ($_SESSION['userType'] == 1 )
{
  $tpl->SetVar("super", 1);
}

/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "������");
$page->Output();
?>