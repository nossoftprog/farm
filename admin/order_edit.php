<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("order.php");
require_once("cartlist.php");
require_once("user.php");
require_once("email.php");


$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$cartlist = new CartList();
$user_ = new User();
$order = new Order();
$email = new SendEmail();

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,1,0,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

$tpl = new Template("_a_order_edit.html");
$error = null;
$tpl->SetVar("formType", "Add");
$checkFunc = "CheckInformation";

if (isset($_REQUEST['OrderID']) && $_REQUEST['OrderID'] && isset($_REQUEST['OrderStatus']) && $_REQUEST['OrderStatus'] =='1') {
  if (($_REQUEST['aid'] == '' && $_SESSION['userType'] == 1) || $_SESSION['userType'] != 1) {
    $stmt = GetStatement();
    $query = "UPDATE orders_ SET OrderStatus=2 WHERE OrderID=".$request->GetProperty("OrderID");
    $stmt->Execute($query);
  }
}

if (isset($_REQUEST['OrderID']) && $_REQUEST['OrderID'] && isset($_REQUEST['UserID']) && $_REQUEST['UserID'] && !isset($_POST['act'])) {
  $userID = $_REQUEST['UserID'];
  $orderID = $_REQUEST['OrderID'];
}

if (isset($_POST['act'])) {
  //echo($_POST['UserID']);
  //echo($_POST['OrderID']);
  //exit;
  $userID = $_POST['UserID'];
  $orderID = $_POST['OrderID'];
  if ($_POST['act'] == "email") {
    $tpl = new Template("_a_order_email_comments.html");
    $tpl->SetVar("AdminComments", str_replace("\n","<br>", $_POST['AdminComments']));
    $comments = $_POST['AdminComments'];
  } else {
    $tpl = new Template("_a_order_email_submit.html");
    $tpl->SetVar("AdminComments", str_replace("\n","<br>", $_POST['AdminComments']));
    $comments = $_POST['AdminComments'];
  }
}

$user_->SetProperty("UserID",$userID);
$user_->LoadFromDatabase();
$order->SetProperty("OrderID",$orderID);
$order->LoadFromDatabase();
$cartlist->LoadFromDatabaseForShow($orderID);

$sum = 0;
if ($cartlist->items) {
  foreach($cartlist->items as $tmp_item)
  {
    $sum = $sum + floatval($tmp_item['HistPriceSum']);
  }
}

$tpl->LoadFromObject($user_);
$tpl->LoadFromObject($order);
$tpl->LoadFromObjectsList("cartlist", $cartlist);
$tpl->SetVar("TotalValue", $sum);
$tpl->SetVar("UserID",$userID);
$tpl->SetVar("OrderID",$orderID);
$tpl->SetVar("error", $error);

if (isset($_POST['act'])) {
  if ($_POST['act'] == "email") {
    $email->SetProperty("Subject", SUBJECT_ORDER_EMAIL);
  } else {
    $email->SetProperty("Subject", SUBJECT_ORDER_SUBMIT);

    $stmt = GetStatement();
    $query = "UPDATE orders_ SET OrderStatus=3, OrderAdminComments=".Connection::GetSQLString($comments)." WHERE OrderID=".$orderID;
    $stmt->Execute($query);
  }
  //$tpl->pparse();
  //exit;
  $text = $tpl->grab();
  $email->SetProperty("SendTo", $user_->GetProperty("UserEmail"));
  $email->SetProperty("From", $_SESSION['FromEmail']);
  $email->SetProperty("Text", $text);
  $email->Send();

	header("Location: orders_list.php");
	exit;
}

/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "�����");
$page->Output();
?>