<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("quest.php");



$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$quest = new Quest();


/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

/***************************************
Add/Edit. Add - by default. Edit - if isset ID
***************************************/

$tpl = new Template("_a_quest_edit.html");
$page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;

$searchString1 = "page=".$page."&";
$searchString1 .= ($request->GetProperty('questID')) ? "questID=".$request->GetProperty('questID')."&" : "";

$error = null;
$tpl->SetVar("formType", "Add");
$checkFunc = "CheckAddInformation";

if ($id = $request->GetProperty("questID"))
{
	$quest->SetProperty("questID", intval($id));
	$quest->LoadFromDataBase();
	$checkFunc = "CheckUpdateInformation";
	$tpl->SetVar("formType", "Edit");
}

if ($post->GetProperty("save"))
{
	$quest->LoadFromArray($post->GetAllProperties());
	if (!($error = $quest->$checkFunc()))
	{
			$quest->Update();
			header("Location: questions_list.php?".$searchString1);
			exit;
	}
}


//draw add/edit form
$tpl->SetVar("error", $error);
$tpl->LoadFromObject($quest);


//list 
$url = "quest_edit.php";
$tpl->SetVar("URL", $url);
$tpl->SetVar("searchString1", $searchString1);


/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "�������������� �������");
$page->Output();
?>