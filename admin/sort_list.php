<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("page.php");
require_once("template.php");
require_once("sortlist.php");
require_once("categorylist.php");

$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$sortlist = new SortList();
$catlist = new CatList();
$catlist->LoadCategoryTopForAdmin();

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,0,0,0,0);

if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

$tpl = new Template("_a_sort_list.html");

$error = null;

if ($request->GetProperty('sortMove') && $request->GetProperty('sID'))
{
	$sortlist->MoveField($request->GetProperty('sID'), $request->GetProperty('sortMove'));
}

$sortlist->LoadFromDataBase();
$tpl->LoadFromObjectsList("sortlist", $sortlist);
$tpl->LoadFromObjectsList("catlist", $catlist);
$tpl->SetVar("CatName_", $_SESSION['CatName_']);

//list
$url = "sort_list.php";
$tpl->SetVar("URL", $url);

$tpl->SetVar("error", $error);

/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "Сортировка категорий");
$page->Output();
?>