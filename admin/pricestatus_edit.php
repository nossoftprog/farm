<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("pricestatus.php");

$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$pricestatus = new PriceStatus();

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

/***************************************
Add/Edit. Add - by default. Edit - if isset ID
***************************************/

$tpl = new Template("_a_pricestatus_edit.html");
$error = null;
$tpl->SetVar("formType", "Add");
$checkFunc = "CheckAddInformation";

if ($id = $request->GetProperty("PriceStatusID"))
{
	$pricestatus->SetProperty("PriceStatusID", intval($id));
	$pricestatus->LoadFromDataBase();
	$checkFunc = "CheckUpdateInformation";
	$tpl->SetVar("formType", "Edit");
}

if ($post->GetProperty("save"))
{
	$pricestatus->LoadFromArray($post->GetAllProperties());
  $pricestatus->SetProperty("PriceStatusName", $post->GetProperty('PriceStatusName'));
  if ($post->GetProperty('AddDoc') == '1')
	{
		$fileName = Add();
		$pricestatus->SetProperty("metDoc", $fileName);
	} else {
		$pricestatus->SetProperty("metDoc", "empty");
  }

  if ($post->GetProperty('AddDoc1'))
	{
		$pricestatus->SetProperty("metDoc", $post->GetProperty('AddDoc1'));
	}
  
	if (!$error = $pricestatus->$checkFunc())
  {
		$pricestatus->Update();
		header("Location: pricestatus_list.php?".$searchString1);
		exit;
	}
}



  function Add()
	{
    global $files;
		if (move_uploaded_file($_FILES['nameDoc']['tmp_name'], DIR_DOCS.$_FILES['nameDoc']['name']))
		{
			return $_FILES['nameDoc']['name'];
		} else {
      return false;
    }
	}

//draw add/edit form
$tpl->SetVar("error", $error);
$tpl->LoadFromObject($pricestatus);


/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "�������������� ������� ������");
$page->Output();
?>