<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("user.php");
require_once("adminlist.php");
//require_once("image.php");

$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$user = new User();
$adminlist = new AdminList();

//$files = new LocalObject($_FILES);
//$img = new Image();

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,0);
if (!$user1 = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

/***************************************
Add/Edit. Add - by default. Edit - if isset ID
***************************************/

$tpl = new Template("_a_user_edit.html");
$error = null;
$tpl->SetVar("formType", "Add");
//$checkFunc = "CheckInformation";


if ($id = $request->GetProperty("UserID"))
{
	$user->SetProperty("UserID", $id);
	$user->LoadFromDataBase();
//������� ����������� ��������� �����
	if($user->GetProperty("OrderUpload") == 1){
		$user->SetProperty("OrderUploadTrue","selected");
	}
    $user->SetProperty("PriceType",$PricesLang[$user->GetProperty("price_type")] ? $PricesLang[$user->GetProperty("price_type")] : "VIP");
    $user->SetProperty("UserPricePercent",($user->GetProperty("UserPricePercent") == "0") ? "1" : $user->GetProperty("UserPricePercent"));
//	$checkFunc = "CheckInformation";
	$tpl->SetVar("formType", "Edit");
}

$adminlist->LoadFromDataBaseManager($user->GetPropertyForSQL("UserAdminID"));
$tpl->LoadFromObjectsList("adminlist", $adminlist);

if ($post->GetProperty("save"))
{
	$user->LoadFromArray($post->GetAllProperties());
//	if (!($error = $user->$checkFunc()))
//	{
		if ($post->GetProperty('AddImage'))
		{
		}
		elseif (!$error)
		{
			$user->UpdateAdmin();
			header("Location: users_list.php?".$searchString1);
			exit;
		}
//	}
}


//draw add/edit form
$tpl->SetVar("error", $error);
$tpl->LoadFromObject($user);


/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "�������������� ������������");
$page->Output();
?>