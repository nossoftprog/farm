<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("admin.php");
//require_once("image.php");

$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$admin = new Admin();
//$files = new LocalObject($_FILES);
//$img = new Image();

/**************************
Validate user access rights
***************************/
$rights = Array(1,1,0,0,0,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

/***************************************
Add/Edit. Add - by default. Edit - if isset ID
***************************************/

$tpl = new Template("_a_admin_edit.html");
$error = null;
$tpl->SetVar("formType", "Add");
$checkFunc = "CheckInformation";

if ($id = $request->GetProperty("AdminID"))
{
	$admin->SetProperty("AdminID", intval($id));
	$admin->LoadFromDataBase();
	$checkFunc = "CheckInformation";
	$tpl->SetVar("formType", "Edit");
	$tpl->SetVar("i".$admin->GetProperty("AdminLevel")," selected");
}

if ($post->GetProperty("save"))
{
	$admin->LoadFromArray($post->GetAllProperties());
  if ($admin->GetProperty("AdminLevel") == "1") 
  {
    $admin->SetProperty("AdminLevelName", "������������������"); 
  } elseif ($admin->GetProperty("AdminLevel") == "2") {
    $admin->SetProperty("AdminLevelName", "�������������"); 
  } elseif ($admin->GetProperty("AdminLevel") == "3") {
    $admin->SetProperty("AdminLevelName", "��������"); 
  } elseif ($admin->GetProperty("AdminLevel") == "4") {
    $admin->SetProperty("AdminLevelName", "��������"); 
  } elseif ($admin->GetProperty("AdminLevel") == "5") {
    $admin->SetProperty("AdminLevelName", "��������"); 
  } 

	if (!($error = $admin->$checkFunc()))
	{
		if ($post->GetProperty('AddImage'))
		{
		}
		elseif (!$error)
		{
			$admin->Update();
			header("Location: admin_list.php?".$searchString1);
			exit;
		}
	}
}


//draw add/edit form
$tpl->SetVar("error", $error);
$tpl->LoadFromObject($admin);


/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "�������������� ��������������");
$page->Output();
?>