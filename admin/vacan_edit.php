<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("vacancy.php");
require_once(dirname(__FILE__)."/../fckeditor/fckeditor.php");


$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$vacancy = new Vacancy();


/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,1);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

/***************************************
Add/Edit. Add - by default. Edit - if isset ID
***************************************/

$tpl = new Template("_a_vacancy_edit.html");
$page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;

$searchString1 = "page=".$page."&";
$searchString1 .= ($request->GetProperty('vacID')) ? "vacID=".$request->GetProperty('vacID')."&" : "";

$error = null;
$tpl->SetVar("formType", "Add");
$checkFunc = "CheckAddInformation";

if ($id = $request->GetProperty("vacID"))
{
	$vacancy->SetProperty("vacID", intval($id));
	$vacancy->LoadFromDataBase();
	$checkFunc = "CheckUpdateInformation";
	$tpl->SetVar("formType", "Edit");
}

if ($post->GetProperty("save"))
{
	$vacancy->LoadFromArray($post->GetAllProperties());
	if (!($error = $vacancy->$checkFunc()))
	{
			$vacancy->Update();
			header("Location: vacan_list.php?".$searchString1);
			exit;
	}
}

//fck editor 1
$fck = new FCKeditor("vacContent");
$fck->basePath = FCK_BASE_PATH;
$fck->width = '100%';
$fck->height = '400';
$fck->value = $vacancy->GetProperty('vacContent');
$tpl->setVar("WYSIWYG_NewsContent", $fck->CreateHtml());


//draw add/edit form
$tpl->SetVar("error", $error);
$tpl->LoadFromObject($vacancy);


//list 
$url = "vacan_edit.php";
$tpl->SetVar("URL", $url);
$tpl->SetVar("searchString1", $searchString1);


/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "�������������� ��������");
$page->Output();
?>