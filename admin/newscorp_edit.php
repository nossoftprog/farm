<?php
require_once(dirname(__FILE__)."/../configure.php");
require_once(dirname(__FILE__)."/../genlib.php");
require_once("authorization.php");
require_once("template.php");
require_once("page.php");
require_once("news.php");
require_once(dirname(__FILE__)."/../fckeditor/fckeditor.php");


$auth = new Authorization();
$request = new LocalObject($_REQUEST);
$post = new LocalObject($_POST);
$news = new News();
$corporate=1;


/**************************
Validate user access rights
***************************/
$rights = Array(1,1,1,0,0,0);
if (!$user = $auth->Validate($rights))
{
	header("Location: index.php");
	exit;
}

/***************************************
Add/Edit. Add - by default. Edit - if isset ID
***************************************/

$tpl = new Template("_a_newscorp_edit.html");
$page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;

$searchString1 = "page=".$page."&";
$searchString1 .= ($request->GetProperty('NewsID')) ? "NewsID=".$request->GetProperty('NewsID')."&" : "";

$error = null;
$tpl->SetVar("formType", "Add");
$checkFunc = "CheckAddInformation";

if ($id = $request->GetProperty("NewsID"))
{
	$news->SetProperty("NewsID", intval($id));
	$news->LoadFromDataBase();
	$checkFunc = "CheckUpdateInformation";
	$tpl->SetVar("formType", "Edit");
}

if ($post->GetProperty("save"))
{
	$news->LoadFromArray($post->GetAllProperties());
	if (!($error = $news->$checkFunc()))
	{
			$news->Update();
			header("Location: newscorp_list.php?".$searchString1);
			exit;
	}
}

//fck editor 1
$fck = new FCKeditor("NewsContent");
$fck->basePath = FCK_BASE_PATH;
$fck->width = '100%';
$fck->height = '400';
$fck->value = $news->GetProperty('NewsContent');
$tpl->setVar("WYSIWYG_NewsContent", $fck->CreateHtml());


//draw add/edit form
$tpl->SetVar("error", $error);
$tpl->LoadFromObject($news);


//list of langs
$url = "newsvcorp_edit.php";
$tpl->SetVar("URL", $url);
$tpl->SetVar("searchString1", $searchString1);


/******************
Output page content
*******************/
$page = Page::CreateForAdmin($tpl, $user, "�������������� ������������� �������");
$page->Output();
?>