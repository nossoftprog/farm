<?php
error_reporting(E_ALL ^ E_DEPRECATED);
session_start();
require_once(dirname(__FILE__)."/configure.php");
require_once(dirname(__FILE__)."/genlib.php");
require_once("functions.php");
require_once("template.php");
require_once("localobject.php");
require_once("authorization_front.php");
require_once("page.php");
require_once("static.php");
require_once("newslist.php");
require_once("gallerylist.php");
require_once("vacancylist.php");
require_once("productlist.php");
require_once("metodlist.php");
require_once("metod.php");
require_once("categorylist.php");
require_once("user.php");
require_once("orderlist.php");
require_once("order.php");
require_once("invoicelist.php");
require_once("invoice.php");
require_once("shiplist.php");
require_once("ship.php");
require_once("email.php");
require_once("cartlist.php");
require_once("actionslist.php");
require_once("action.php");
require_once("faqlist.php");
require_once("questlist.php");
require_once("quest.php");
require_once("cartlist.php");
require_once("pricestatuslist.php");
require_once("paging.php");
require_once("pricestatus.php");
//exit;
set_time_limit(300);
//echo ini_get('max_execution_time');
//exit;

$mtime = explode(" ",microtime());	//��������� ������� � ������������
// ���������� ���� ����� �� ������ � �����������
// � ���������� ��������� ����� � ����������
$tstart = $mtime[1] + $mtime[0];


$get = new LocalObject($_GET);
$post = new LocalObject($_POST);
$request = new LocalObject($_REQUEST);
$error = null;
$pageType = "main";

$auth = new Authorization();
$user = $auth->Validate(5);
$auth->Logout(); 

$corporate = 0;

//if (!$user)
//{
//	header("Location: index.php");
//	exit;
//}
$cmd = !empty($request->GetProperty("cmd")) ? $request->GetProperty("cmd") : '';
$view = !empty($request->GetProperty("view")) ? $request->GetProperty("view") : 'index';
$layout = !empty($request->GetProperty("layout")) ? $request->GetProperty("layout") : 'default';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $action = $post->GetProperty('action');
    if ($action == 'register') {
        list($temp_errors,$temp_data) = motRegister($post);
        if(!empty($temp_errors)){
            $_SESSION['errors'] = $temp_errors;
            $_SESSION['data'] = $temp_data;
        }
    }if ($action == 'edit') {
        list($temp_errors,$temp_data) = motRegisterEdit($post);
        if(!empty($temp_errors)){
            $_SESSION['errors'] = $temp_errors;
            $_SESSION['data'] = $temp_data;
        }
    }elseif($action == 'abort'){
        list($temp_errors,$temp_data) = abortUser($post);
        if(!empty($temp_errors)){
            $_SESSION['errors'] = $temp_errors;
            $_SESSION['data'] = $temp_data;
        }
    }elseif($action == 'bonus'){
        list($temp_errors,$temp_data) = motGetBonus($post);
        if(!empty($temp_errors)){
            $_SESSION['errors'] = $temp_errors;
            $_SESSION['data'] = $temp_data;
        }
    }
}

switch ($cmd)
{
    case "motivation":
        if(empty($motTestUsers) || in_array($_SESSION['userName'], $motTestUsers)){
            require_once("includes/".$cmd."/".$view.".php");
        }else{
            header("Location: /index.php?cmd=basket");
        }
    break;
    case "register":
        $tpl = new Template("register.html");
        $user = new User();
    $page = 1;

    $error = null;
    $tpl->SetVar("formType", "Add");
    $checkFunc = "CheckInformation";

    if ($id = $request->GetProperty("UserID"))
    {
      $user->SetProperty("UserID", intval($id));
      $user->LoadFromDataBase();
      $checkFunc = "CheckInformation";
//      $tpl->SetVar("formType", "Edit");
    }

    if ($post->GetProperty("save"))
    {
      $user->LoadFromArray($post->GetAllProperties());

      if (!($error = $user->$checkFunc()))
      {

//error_reporting(E_ALL);
//          $user->Update();
          //������������ ���� �� ����� ���������� ��������� 
        $tpl = new Template("_user_email_request.html");
        $tpl->LoadFromObject($user);
        $text = $tpl->grab();


		//����� � ����, ����� ������������ ��� ������
          if (strlen($post->GetProperty("UserID")) == 0) {
		$subject_mail_user = SUBJECT_NEW_USER;
	    }else{
		$subject_mail_user = SUBJECT_USER;
  	    }



        $email = new SendEmail();
        $email->SetProperty("SendTo", ADMIN_EMAIL);
        $email->SetProperty("Subject", $subject_mail_user);
        $email->SetProperty("Text", $text);
        $email->Send();

        //������ ��������������� ������
        $email = new SendEmail();
        $email->SetProperty("SendTo", ADMIN2_EMAIL);
        $email->SetProperty("Subject", $subject_mail_user);
        $email->SetProperty("Text", $text);
        $email->Send();




///������ ������������
		//����� � ����, ����� ������������ ��� ������
          if (strlen($post->GetProperty("UserID")) == 0) {

        $tpl = new Template("_user_email_send.html");
        $tpl->LoadFromObject($user);
        $text = $tpl->grab();

        $email = new SendEmail();
        $email->SetProperty("SendTo", $post->GetProperty("UserEmail"));
        $email->SetProperty("Subject", SUBJECT_NEW_USER);
        $email->SetProperty("Text", $text);
        $email->Send();
        }




          if (strlen($post->GetProperty("UserID")) == 0) {
            header("Location: index.php?cmd=thanks");
            exit;
          } else {
            header("Location: index.php");
            exit;
          }
      }
    }
    if (isset($_SESSION['userID'])) {
      $user->SetProperty('UserID',$_SESSION['userID']);
    }
    $user->LoadFromDataBase();
    $tpl->LoadFromObject($user);

    $url = "/index.php?cmd=register";
    $tpl->SetVar("URL", $url);
    $searchString1 = "&page=".$page."&";
    $tpl->SetVar("searchString1", $searchString1);
    $tpl->SetVar("error", $error);
    $tpl->SetVar("pageName", "�����������");

		$page = Page::CreateForCustomer($tpl, PROJECT_NAME, "main", "�����������");
		$page->Output();
		exit;
	break;

	case "thanks":
		$tpl = new Template("thanks.html");
    $tpl->SetVar("pageName", "����������� ���������");

		$page = Page::CreateForCustomer($tpl, PROJECT_NAME, "main", "����������� ���������");
		$page->Output();
		exit;
	break;



  case "orders":
    $page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;
    $orderslist = new OrderList();
    $orderslist->LoadFromDataBaseForCustomer();
		$tpl = new Template("orders_list.html");
    $tpl->SetVar("PageName", "������ �������");
    $tpl->LoadFromObjectsList("orderslist", $orderslist);
    //paging
    $paging  = new Paging($page, $orderslist->GetTotalCountForCustomer(), $tpl);
    $url = "/index.php?cmd=orders";
    $tpl->SetVar("URL", $url);
    echo $tpl->grab();
		//$page = Page::CreateForCustomer($tpl, PROJECT_NAME, "main", "������ �������");
		//$page->Output();
		exit;
	break;

  case "order";

        $order = new Order();
        $order->SetProperty("OrderID", $_REQUEST['orderID']);
        $order->LoadFromDatabase();

        $user = new User();
        $user->SetProperty('UserID',$_SESSION['userID']);
        //$user->LoadFromDataBaseForEmail();

        list($oCompanyCoflexID,$oAddressCoflexID) = split("_",$order->GetProperty("OrderAddressCoflexID"));
        $user->SetProperty('AddressCoflexID',$oAddressCoflexID);
        $user->SetProperty('CompanyCoflexID',$oCompanyCoflexID);
        $user->LoadFromDataBaseForXML();

        //��������� �� ������� ������ � ��.����
        $cartlist = new CartList();
        $cartlist->LoadFromDatabaseForShow($_REQUEST['orderID']);


        $sum = 0;
        $weight = 0;
        $volume = 0;
        if ($cartlist->items) {
          for ($i=0;$i<count($cartlist->items);$i++) 
          {
          if (isset($cartlist->items[$i])) {
            $s = $cartlist->items[$i]['HistPriceSum'];
            $p = $cartlist->items[$i]['HistPriceOne'];
            $sum = $sum + floatval(ereg_replace("[^-0-9\.]","",$s));
            $weight = $weight + floatval(ereg_replace("[^-0-9\.]","",$cartlist->items[$i]['HistWeight']));
            $volume = $volume + floatval(ereg_replace("[^-0-9\.]","",$cartlist->items[$i]['HistVolume']));
            $cartlist->items[$i]['HistPriceSum'] = number_format($s, 2 );
            $cartlist->items[$i]['HistPriceOne'] = number_format($p, 2 );
          }
          }
        }
        $sum = number_format($sum, 2 );

        //������ ������������
	      $tpl = new Template("_order_details.html");
        $tpl->LoadFromObject($user);
        $tpl->LoadFromObject($order);
        $tpl->LoadFromObjectsList("cartlist", $cartlist);
        $tpl->SetVar("TotalValue", $sum);
        $tpl->SetVar("TotalWeight", $weight);
        $tpl->SetVar("TotalVolume", $volume);
	      $tpl->SetVar("OrderID", $_REQUEST['orderID']);
        $text = $tpl->grab();
        echo $text;

    exit;
  break;

  case "invoices":
    if ($request->GetProperty("act") == "update") {
      $invoice = new Invoice();
      $invoice->UpdateCheck($post->GetProperty("ids"));
	    header("Location: index.php?cmd=price&basket=1&officetabs=2");
	    exit;
    }
    if ($request->GetProperty("act") == "show") {
      $invoice = new Invoice();
      $invoice->UpdateShow($request->GetProperty("invoiceid"));
	    header("Location: index.php?cmd=price&basket=1&officetabs=2");
	    exit;
    }
    $invoicelist = new InvoiceList();
    $invoicelist->LoadFromDataBase($_SESSION['userName']);
		$tpl = new Template("invoice_list.html");
    $tpl->SetVar("PageName", "������ ������");
    $tpl->LoadFromObjectsList("invoicelist", $invoicelist);
    echo $tpl->grab();
		//$page = Page::CreateForCustomer($tpl, PROJECT_NAME, "main", "������ ������");
		//$page->Output();
		exit;
	break;

  case "invoice";

    $invoice = new Invoice();
    $invoice->LoadFromDataBase($request->GetProperty("invoiceID"));
    $text = $invoice->GetProperty("InvoiceBody");
    $num = $invoice->GetProperty("InvoiceNum");
    if ( $invoice->GetProperty("InvoiceShip") == 1 ) {
      $text = str_replace('<TITLE>', '<TITLE>�������� ' . $num, $text);
    } else {
      $text = str_replace('<TITLE>', '<TITLE>���� ' . $num, $text);
    }
    echo $text;
    exit;
  break;

  case "ships":
    $shiplist = new ShipList();
    $shiplist->LoadFromDataBase($_SESSION['userName']);
		$tpl = new Template("ship_list.html");
    $tpl->SetVar("PageName", "��������");
    $tpl->LoadFromObjectsList("shiplist", $shiplist);
    echo $tpl->grab();
		//$page = Page::CreateForCustomer($tpl, PROJECT_NAME, "main", "������ ������");
		//$page->Output();
		exit;
	break;

  case "ship";
    $ship = new Ship();
    $ship->LoadFromDataBase($request->GetProperty("shipID"));
    $text = $ship->GetProperty("ShipHTML");
    $num = $ship->GetProperty("ShipNum");
    $text = str_replace('<TITLE>', '<TITLE>�������� ' . $num, $text);
    echo $text;
    exit;
  break;

 case "connect";
    //$shiplist = new ShipList();
    //$shiplist->LoadFromDataBase($_SESSION['userName']);
		$tpl = new Template("connect_list.html");
    //$tpl->SetVar("PageName", "��������");
    //$tpl->LoadFromObjectsList("shiplist", $shiplist);
    echo $tpl->grab();
    exit;
  break;




	case "hotnews":
		$tpl = new Template("hot_news_list.html");
		$newslist = new NewsList();

    $page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;
    $error =null;

    $searchString = "";  
    $tpl->SetVar("searchString", $searchString);

    $newslist->LoadFromDataBase($page, 1, 0, 0);
    $tpl->LoadFromObjectsList("newslist", $newslist);
    //paging
    $paging  = new Paging($page, $newslist->GetTotalCount(1), $tpl);

    //list
    $url = "/index.php?cmd=hotnews";
    $tpl->SetVar("URL", $url);
    $searchString1 = "&page=".$page."&";
    $tpl->SetVar("searchString1", $searchString1);
    $tpl->SetVar("pageName", "������� �������");
    $tpl->SetVar("All", "1");

		$page = Page::CreateForCustomer($tpl, PROJECT_NAME, "main", "������� �������");
		$page->Output();
		exit;
	break;

	case "allnews":
		$tpl = new Template("hot_news_list.html");
		$newslist = new NewsList();

    $page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;
    $error = null;

    $searchString = "";  
    $tpl->SetVar("searchString", $searchString);

    $newslist->LoadFromDataBase($page, 0, 0, 1);
    $tpl->LoadFromObjectsList("newslist", $newslist);

    //paging
    $paging  = new Paging($page, $newslist->GetTotalCount(1), $tpl);

    //list
    $url = "/index.php?cmd=allnews";
    $tpl->SetVar("URL", $url);
    $searchString1 = "&page=".$page."&";
    $tpl->SetVar("searchString1", $searchString1);
    $tpl->SetVar("pageName", "�������");

		$page = Page::CreateForCustomer($tpl, PROJECT_NAME, "main", "�������");
		$page->Output();
		exit;
	break;

  case "price":
    $online = 1;  //ONLINE
    if (!$user)
    {
      $online = 0;
      $tpl = new Template("motivation/login.html");
      $tpl->SetVar("url", $_SERVER['REQUEST_URI']);
      break;
    }
    $products = new ProductList();
    $products_hot = new ProductList();
    $products_hotweek = new ProductList();
    $page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;
    $filter = $request->GetProperty('filter') ? $request->GetProperty('filter') : '0' ;
    $showmore = 0;
    $error = null;

    $catlist = new CatList();
    //$catlist->LoadCategoryTop();

    
    $status = new PriceStatus();
    $status->LoadFromDataBaseHot();
    $statuslist = new PriceStatusList();
    $statuslist->LoadFromDataBase();


    if (!isset($_SESSION['CatType']))
    {
        $_SESSION['CatType'] = 1;
        //������ �� ��������� ����� ���������, ���� ���������� ����� ������ ��������� - �������� ��� �����.
        if(isset($_COOKIE['CatType']) && $_COOKIE['CatType'] == 1)
            $_SESSION['CatType'] = 1;
        else
            $_SESSION['CatType'] = 0;
        if(isset($_COOKIE['CatType']) && $_COOKIE['CatType'] == 2)
            $_SESSION['CatType'] = 2;
    }
    if (isset($_REQUEST['CatType']))
    {
      $_SESSION['CatType'] = $_REQUEST['CatType'];
      //��������� ����� ����������
      setcookie ("CatType", $_SESSION['CatType'], time()+COOKIE_LIFE_TIME, "/");

    }

    if ($_SESSION['CatType'] == 0) {
      if ($page == 1) {
        if ($filter != '1') {
          $tpl = new Template("product_list.html");
        } else {
          $tpl = new Template("product_list_more_ajax.html");
        }
      } else {
        if ($filter != '1') {
          $tpl = new Template("product_list_ajax.html");
        } else {
          $tpl = new Template("product_list_more_ajax.html");
        }
      }
    } elseif ($_SESSION['CatType'] == 2)  {
      if ($filter != '1') {
        $tpl = new Template("product_list_full.html");
      } else {
        $tpl = new Template("product_list_more_full_ajax.html");
      }
    } else  {
      if ($filter != '1') {
        $tpl = new Template("product_list_cat.html");
      } else {
        $tpl = new Template("product_list_more_cat_ajax.html");
      }
    }
    //Gr8Dev
    $tab = array(
        "cart" => "0",//not work
        "orders" => "1",
        "bill" => '2',
        "info" => "3",
        "answers" => "4",
        "motivation" => "5"
        );
    if(isset($tab[$get->GetProperty("tab")])){
        $tpl->SetVar("tab", $tab[$get->GetProperty("tab")]);
    }
    if(!empty($layout)){
        $tpl->SetVar("layout", $layout);
    }
    if(empty($motTestUsers) || in_array($_SESSION['userName'], $motTestUsers)){
        $tpl->SetVar("mot_tab", 1);
    }
    //end Gr8Dev
    //$tpl->LoadFromObjectsList("catlist", $catlist);
     $tpl->LoadFromObject($status);

    if (isset($_POST['searchProd']))
    {
      $_SESSION['searchProd'] = $_POST['searchProd'];
      $page = 1;
    }

    //exit;

    if (isset($_POST['userCheck'])) {
					$_SESSION['userCheck1'] = intval($_POST['userCheck1']);
					$_SESSION['userCheck2'] = intval($_POST['userCheck2']);
					$_SESSION['userCheck3'] = intval($_POST['userCheck3']);
          if (isset($_SESSION['userID'])) {
             $user->SetProperty('UserID',$_SESSION['userID']);
             $user->SetProperty('UserCheck1',$_SESSION['userCheck1']);
             $user->SetProperty('UserCheck2',$_SESSION['userCheck2']);
             $user->SetProperty('UserCheck3',$_SESSION['userCheck3']);
             $user->UpdateCheck();
          }
    }

    if (isset($_REQUEST['basket']))
    {
      $_SESSION['basket'] = $_REQUEST['basket'] == '1' ? '1':'0';
    }

    if (isset($_POST['basket']))
    {
      $_SESSION['basket'] = $_POST['basket'] == '1' ? '1':'0';
    }

    $searchString = "";
    $tpl->SetVar("searchString", $searchString);

    if (isset($_SESSION['searchProd'])) 
    {
       $tpl->SetVar("searchProd", $_SESSION['searchProd']);
    }


    if (!isset($_SESSION['userCheck1'])) {
					$_SESSION['userCheck1'] = 1;
					$_SESSION['userCheck2'] = 1;
					$_SESSION['userCheck3'] = 0;
    }
    $tpl->SetVar("userCheck1", $_SESSION['userCheck1']);
    $tpl->SetVar("userCheck2", $_SESSION['userCheck2']);
    $tpl->SetVar("userCheck3", $_SESSION['userCheck3']);

    //$products_hot->LoadFromDataBaseForCustomerHot($page);
    if ($page == 1 && $filter != '1') {
      $products_hotweek->LoadFromDataBaseForCustomerHotWeek();
    }
    $count_ = 0;
    $products->LoadFromDataBaseForCustomer($page, $filter);

    if ($_SESSION['CatType'] != 1) {
      $count_ = count($products->items);
    } else {
      if (isset($_SESSION['searchProd']) && $_SESSION['searchProd'] != '') {
        $count_ = $_SESSION['count_'];
      } else {
        $count_ = -1;
      }
    }
    //echo $count_;
    if (!$_SESSION['userCheck1'] || !$_SESSION['userCheck2'] || !$_SESSION['userCheck3'] ) {
    if ($count_ != -1 && $count_ < ITEMS_PER_PAGE_ALPHABET_FIRST ) {
      $showmore = 1;
    }
    }


    if ($_SESSION['CatType'] != 1) {
      $tpl->LoadFromObjectsList("productlist", $products);
    } else {
      $catlistcat = Array();
      $tpl->LoadFromArray("catlist", $catlist);
      $tpl->LoadFromObjectsList("productlist", $products);
    }



    //$tpl->LoadFromObjectsList("productlist_hot", $products_hot);
    if ($page == 1 && $filter != '1') {
      $tpl->LoadFromObjectsList("productlist_hotweek", $products_hotweek);
      $tpl->LoadFromObjectsList("statuslist", $statuslist);
    }

    //paging
    if (WHOLE_PRICE != 1) {
      $paging  = new Paging($page, $products->GetTotalCountForCustomer(), $tpl);
    }

    // Time of refresh
    if ($page == 1 && $filter != '1') {
      $timestamp = time();
      $date_time_array = getdate($timestamp);

      $hours = $date_time_array['hours'];
      $minutes = $date_time_array['minutes'];
      $seconds = $date_time_array['seconds'];
      $month = $date_time_array['mon'];
      $day = $date_time_array['mday'];
      $year = $date_time_array['year'];
      $tpl->SetVar("Obn", $hours . ":" . $minutes);
      $timestamp = mktime($hours,$minutes + 30,$seconds,$month,$day,$year);
      $date_time_array = getdate($timestamp);
      $hours1 = $date_time_array['hours'];
      $minutes1 = $date_time_array['minutes'];
      if ( $hours1 == $hours ) {
        $minutes  = "00";
        $minutes1 = "30";
      } else {
        $minutes1 = "00";
        $minutes  = "30";
      }
      $tpl->SetVar("Obn", $hours . ":" . $minutes);
      $tpl->SetVar("Obn_next", $hours1 . ":" . $minutes1);




      //list 
      $url = "/index.php?cmd=price";
      $tpl->SetVar("URL", $url);
      $searchString1 = "page=".$page."&";
      $tpl->SetVar("searchString1", $searchString1);
      $tpl->SetVar("pageName", "�����-����");
    }

    $tpl->SetVar("ShowOst", SHOW_OSTATKI);
    if (isset($_SESSION['CatID']) && $_SESSION['CatID'] != 12764092) {
      $tpl->SetVar("ShowOst1", SHOW_OSTATKI_1);
    } else {
      $tpl->SetVar("ShowOst1", 0);
    }
    $tpl->SetVar("Online", $online);
    if (isset($_SESSION['userID'])) {
      $tpl->SetVar("userID", $_SESSION['userID']);
      if (isset($_REQUEST['basket']) && $_REQUEST['basket'])
      {
        $_SESSION['basket'] = $_REQUEST['basket'] == '1' ? '1':'0';
      }
    }
    if (isset($_SESSION['basket']) && $_SESSION['basket'] != '0') {
      $tpl->SetVar("basketshow", $_SESSION['basket']);
    }
    $tpl->SetVar("CatName", $_SESSION['CatName']);

    //writing search words
      if (!empty($_SESSION['searchProd'])){
          $log_text = date("d.m.y H:s",time()).";".$_SESSION['searchProd'].";".$products->GetTotalCountForCustomer()."\n";
          //echo $log_text; //exit;
          $fp = @fopen(LOG_BASE_PATH."search_queries.csv","a");
          if($fp){
              fwrite($fp, $log_text);
              fclose($fp);
          }
      }

$mtime = microtime();
$mtime = explode(" ",$mtime);
$mtime = $mtime[1] + $mtime[0];
$totaltime = ($mtime - $tstart);//��������� �������
// ������� �� �����
//printf ("�������� ������������� �� %f ������ !", $totaltime);

    $invoicelist = new InvoiceList();
    $newcount = $invoicelist->GetTotalCountNew();
    $tpl->SetVar("newcount", $newcount);
    $tpl->SetVar("showmore", $showmore);
    if ($page == 1 && $filter != '1') {
      $page = Page::CreateForCustomer($tpl, PROJECT_NAME, "main", "�����-����");
      $page->Output();
    } else {
      $tpl->pparse();
    }
		exit;
	break;

  case "priceshort";
    $tempcat = $request->GetProperty("catid_");
    $products = new ProductList();
		$tpl = new Template("product_list_cat_short.html");
    $products->LoadFromDataBaseForCustomerShort($tempcat);
    if (isset($_SESSION['userID'])) {
      $tpl->SetVar("userID", $_SESSION['userID']);
    }
    $tpl->SetVar("ShowOst", SHOW_OSTATKI);
    $tpl->LoadFromObjectsList("productlistcat", $products);

    $tpl->pparse();
		exit;
  break;

	case "basket":
    if (isset($_SESSION['userID']) && $_SESSION['userID']) 
    {

    $sum = 0;
    $weight = 0;
    $volume = 0;
		$tpl = new Template("cart.html");
		$order = new Order();
		$cartlist = new CartList();
    $products_hotweek = new ProductList();
    if (isset($_REQUEST["id"]) && $request->GetProperty("id")) {
      $_SESSION['basket'] = '1';
    }
    if ($user) {
      $error = null;
      $searchString = "";  
      $tpl->SetVar("searchString", $searchString);

      //�������� ����� ��� �������� ����������� �������� ������
        $tmpuser = new User();
        $tmpuser->SetProperty('UserID',$_SESSION['userID']);
	  $tmpuser->LoadFromDataBase();

    if ($tmpuser->GetProperty("OrderUpload") == "1") {
      $tpl->SetVar("OrderUpload", "1");
   	}
      $order->SetProperty("OrderID", $_SESSION['OrderID']);
      $order->LoadFromDatabase();
      $cartlist->LoadFromDatabase($_SESSION['OrderID']);
      $ids1 = array();
      if ($cartlist->items) {
        foreach($cartlist->items as $tmp_item)
        {                        
//          $sum = $sum + floatval($tmp_item['HistPriceSum']);
          $sum = $sum + floatval(ereg_replace("[^-0-9\.]","",$tmp_item['HistPriceSum']));
          $weight = $weight + floatval(ereg_replace("[^-0-9\.]","",$tmp_item['HistWeight']));
          $volume = $volume + floatval(ereg_replace("[^-0-9\.]","",$tmp_item['HistVolume']));
          if ( $tmp_item['HistValue'] != 0 ) { $ids1[] = $tmp_item['HistPriceID']; }
        }                        
      }

      if ($id = $request->GetProperty("id"))
      {
        $val = $request->GetProperty("val");
        $exists = 0;
		    $items = array();
        if ($cartlist->items) {
          foreach($cartlist->items as $tmp_item)
          {
            if ($tmp_item['HistPriceID'] == $id) 
            {
              $tmp_item['HistValue'] = $val;
              $exists = 1;
            }
            $items[] = $tmp_item;
          }
        }
        if ($exists == 0) {
          $item = FALSE;
          $item['HistPriceID'] = $id;
          $item['HistValue'] = $val;
          $item['HistOrderID'] = $_SESSION['OrderID'];
          $items[] = $item;
        }
        $cartlist->items = $items;
        $cartlist->Update($_SESSION['OrderID']);
        $cartlist->LoadFromDatabase($_SESSION['OrderID']);
        $sum = 0;
        $weight = 0;
        $volume = 0;
        $ids1 = array();
        if ($cartlist->items) {
          foreach($cartlist->items as $tmp_item)
          {
//        $sum = $sum + floatval($tmp_item['HistPriceSum']);
          $sum = $sum + floatval(ereg_replace("[^-0-9\.]","",$tmp_item['HistPriceSum']));
          $weight = $weight + floatval(ereg_replace("[^-0-9\.]","",$tmp_item['HistWeight']));
          $volume = $volume + floatval(ereg_replace("[^-0-9\.]","",$tmp_item['HistVolume']));
          if ( $tmp_item['HistValue'] != 0 ) { $ids1[] = $tmp_item['HistPriceID']; }
          }
        }
      }
      if ($post->GetProperty("act"))
      {
	$rewrite_cart = false;
      if ($post->GetProperty("act") == "file")       //������ ������������ �����
      {
		if($_FILES["FileOrder"]["tmp_name"]){
    			$order_file = fread($fp = fopen($_FILES["FileOrder"]["tmp_name"], 'r'), $_FILES["FileOrder"]["size"]);
		      fclose($fp);
			$order_file_array = explode("\n", $order_file);
			$order_comment = $_SESSION['order_comment'] = $order_file_array[0];
                  for($i=1;$i<count($order_file_array);$i++){
                   list($ids[],$vals[]) = split("\t",$order_file_array[$i]);
			}
			$rewrite_cart = true;
		}else{
		  $error = "��������, �������� ������ �����!";
	        $vals = $post->GetProperty("vals");
      	  $ids = $post->GetProperty("ids");
		}

      }else{						    //����� �� ����, ������ ������������� */
        $vals = $post->GetProperty("vals");
        $ids = $post->GetProperty("ids");
      }

        if ($cartlist->items || $rewrite_cart) {
     	    $items = array();
     	    if(!$rewrite_cart){
          foreach($cartlist->items as $tmp_item)
          {
            for($i=0;$i<count($ids);$i++)
            {
              if ($tmp_item['HistPriceID'] == $ids[$i]) 
              {
                $tmp_item['HistValue'] = intval($vals[$i]);
              }
            }
            $items[] = $tmp_item;
          }
         }else{
         	for($i=0;$i<count($ids);$i++)
	      {
		          $item = FALSE;
		          $item['HistPriceID'] = $ids[$i];
		          $item['HistValue'] = intval($vals[$i]);
		          $item['HistOrderID'] = $_SESSION['OrderID'];
		          $items[] = $item;

		}

        } 
          
        
          $cartlist->items = $items;
	    $error = $cartlist->Update($_SESSION['OrderID']);
          if ($error != "")
		{
            $tpl->SetVar("Error", $error);
		}
          $cartlist->LoadFromDatabase($_SESSION['OrderID']);
          $sum = 0;
          $weight = 0;
          $volume = 0;
          $ids1 = array();
          if ($cartlist->items) {
            foreach($cartlist->items as $tmp_item)
            {
//              $sum = $sum + floatval($tmp_item['HistPriceSum']);
          $sum = $sum + floatval(ereg_replace("[^-0-9\.]","",$tmp_item['HistPriceSum']));
          $weight = $weight + floatval(ereg_replace("[^-0-9\.]","",$tmp_item['HistWeight']));
          $volume = $volume + floatval(ereg_replace("[^-0-9\.]","",$tmp_item['HistVolume']));
          if ( $tmp_item['HistValue'] != 0 ) { $ids1[] = $tmp_item['HistPriceID']; }
            }
          }
        }
      }

      if ($post->GetProperty("act") == "check")
      {
        $tpl = new Template("cartcheck.html");
        if(!$order_comment && $post->GetProperty("HistComments")) $order_comment = $post->GetProperty("HistComments");
        if(!$order_comment && $_SESSION['order_comment']) $order_comment = $_SESSION['order_comment'];
        $tpl->SetVar("HistComments", $order_comment);

        $user = new User();
        if (isset($_SESSION['userID'])) {
          $user->SetProperty('UserID',$_SESSION['userID']);
        }
        $user->LoadFromDataBase();
        $tpl->LoadFromObject($user);
        $companylist = new Companies();
	  $companylist->LoadFromDataBaseCompany($user->GetProperty("CoflexID"));
	  $tpl->LoadFromObjectsList("companylist", $companylist);
//	  var_dump($companylist);

      }

      if ($post->GetProperty("act_") == "sendorder")
      {
        $user = new User();
        $user->SetProperty('UserID',$_SESSION['userID']);
        //��������� �� ������� ������ � ��.����
        list($oCompanyCoflexID,$oAddressCoflexID) = split("_",$post->GetProperty("AddressCoflexID"));
        $user->SetProperty('AddressCoflexID',$oAddressCoflexID);
        $user->SetProperty('CompanyCoflexID',$oCompanyCoflexID);
        $user->LoadFromDataBaseForXML();
//var_dump($user); exit;
        $cartlist->UpdateSend($_SESSION['OrderID'], $sum);
        $cartlist->LoadFromDatabase($_SESSION['OrderID']);
        $order = new Order();
        $order->SetProperty("OrderID", $_SESSION['OrderID']);
        $order->LoadFromDatabase();

        $sum = 0;
        $weight = 0;
        $volume = 0;
        if ($cartlist->items) {
          foreach($cartlist->items as $tmp_item)
          {
//            $sum = $sum + floatval($tmp_item['HistPriceSum']);
          $sum = $sum + floatval(ereg_replace("[^-0-9\.]","",$tmp_item['HistPriceSum']));
          $weight = $weight + floatval(ereg_replace("[^-0-9\.]","",$tmp_item['HistWeight']));
          $volume = $volume + floatval(ereg_replace("[^-0-9\.]","",$tmp_item['HistVolume']));
          }
        }


                    $orderEmail1 = explode(";",$user->GetProperty('UserAdminEmail'));
                    $_SESSION['OrderEmail1'] = array_shift($orderEmail1);
                    $_SESSION['OrderEmailCopy1'] = count($orderEmail1) ? $orderEmail1 : false;

    /*echo $_SESSION['OrderEmail1'] . "<br>";
    echo $_SESSION['OrderEmailCopy1'] . "<br>";
    echo $_SESSION['OrderEmail'] . "<br>";
    echo $_SESSION['OrderEmailCopy'] . "<br>";
    exit;*/

        //������ ������������
	  $tpl = new Template("_order_email_user.html");
        $tpl->LoadFromObject($user);
        $tpl->LoadFromObject($order);
        $tpl->LoadFromObjectsList("cartlist", $cartlist);
        $tpl->SetVar("TotalValue", $sum);
        $tpl->SetVar("TotalWeight", $weight);
        $tpl->SetVar("TotalVolume", $volume);
	  $tpl->SetVar("OrderID", $_SESSION['OrderID']);
        $email = new SendEmail();
        $text = $tpl->grab();
        $email->SetProperty("SendTo", $user->GetProperty("UserEmail"));
        $email->SetProperty("Subject", SUBJECT_ORDER . " � �������� ������");
        $email->SetProperty("Text", $text);
        $email->Send();  




        //�������� ������
		
		  $ftp_server = FTP_SERVER;
		  $ftp_user_name = FTP_USER_NAME;
		  $ftp_user_pass = FTP_USER_PASS;
		  $name_dir = NAME_DIR_ORDERS;
              //��������� ������
		  $tpl = new Template("_order_xml.html");
	        $tpl->LoadFromObject($user);
	        $tpl->LoadFromObject($order);
	        $tpl->LoadFromObjectsList("cartlist", $cartlist);
	        $tpl->SetVar("TotalValue", $sum);
          $tpl->SetVar("TotalWeight", $weight);
          $tpl->SetVar("TotalVolume", $volume);
	        $tpl->SetVar("Comment", preg_replace("'\r\n's"," ",$order->GetProperty("OrderComments")));
	        $text = $tpl->grab();

              //������ ���� � �������
//		  $order_remote_file = $name_dir."/".$_SESSION['OrderID'].".xml";
//		  $order_file = "admin/orders/local/".$_SESSION['OrderID'].".xml";
          $ordernum_ext = date("ymdHis",time());
		  $order_remote_file = $name_dir."/".$_SESSION['OrderID']."_".$ordernum_ext.".xml";
		  $order_file = "admin/orders/local/".$_SESSION['OrderID']."_".$ordernum_ext.".xml";

			fwrite($fp = fopen($order_file, 'w'), $text);
			fclose($fp);
		  $conn_id = @ftp_connect($ftp_server);
		  @ftp_pasv($conn_id, true);
		  $login_result = @ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
		    // ��������� ����������
		  if ($conn_id && $login_result) { 
			  $subjext = "";
			  $upload = @ftp_put($conn_id, $order_remote_file, $order_file, FTP_BINARY);
			  if(!$upload){
				 $subjext = "!!! ��������� ������ ���� ������ � 1�! (����� �� �������� � 1�) ";

			  }else{
				 $subjext = "";
		  	  }
		  }else{
			  $subjext = "!!! ��������� ������ ���� ������ � 1�! (��� ����� � 1�) ";
		  }
		  // ������� ����� FTP
		  ftp_close($conn_id);
          //����� ��������  */      

        //������ ���������
	  $tpl = new Template("_order_email_request.html");
        $tpl->LoadFromObject($user);
        $tpl->LoadFromObject($order);
        $tpl->LoadFromObjectsList("cartlist", $cartlist);
        $tpl->SetVar("TotalValue", $sum);
        $tpl->SetVar("TotalWeight", $weight);
        $tpl->SetVar("TotalVolume", $volume);
	  $tpl->SetVar("OrderID", $_SESSION['OrderID']);
	  $tpl->SetVar("ErrorUpload", $subjext);
        $email = new SendEmail();
        $text = $tpl->grab();
        $email->SetProperty("SendTo", $_SESSION['OrderEmail1']);
          if($_SESSION['OrderEmailCopy1'])
              $email->SetProperty("SendCopyTo", $_SESSION['OrderEmailCopy1']);
//        $email->SetProperty("Subject", $subjext.SUBJECT_ORDER . " ��� " . $user->GetProperty("AdminName"));
	  $email->SetProperty("Subject", $subjext.SUBJECT_ORDER . " ��� " . $user->GetProperty("AdminName") . "  �� " . $user->GetProperty("UserCompany") . " (" . $user->GetProperty("CompanyName") . ")");

        $email->SetProperty("Text", $text);
        $email->Send();  





	  $tpl = new Template("cartsend.html");
        //$_SESSION['OrderID'] = null;
          unset($_SESSION['OrderID']);
        $tpl->pparse();
        exit;
      }
      $status = new PriceStatus();
      $status->LoadFromDataBaseHot();
      $products_hotweek->LoadFromDataBaseForCustomerHotWeek('1'); //$ids1); 
      $tpl->LoadFromObject($status);
      $tpl->LoadFromObjectsList("cartlist", $cartlist);
      $tpl->LoadFromObjectsList("cartlistweek", $products_hotweek);
      //list
      $url = "/index.php?cmd=basket";
      $tpl->SetVar("TotalValue", $sum);
        $tpl->SetVar("TotalWeight", $weight);
        $tpl->SetVar("TotalVolume", $volume);
      $tpl->SetVar("URL", $url);
      $tpl->SetVar("OrderID", $_SESSION['OrderID']);
      //$tpl->SetVar("searchString1", $searchString1);
      if(!$order_comment && $_SESSION['order_comment']) $order_comment = $_SESSION['order_comment'];
      $tpl->SetVar("HistComments", $order_comment);

    }
    $tpl->SetVar("pageName", "�������");
    $tpl->pparse();
    
		exit;
    } else {
        echo "<html><body><script>parent.document.location='index.php';</script></body></html>";
        exit;
    }
	break;

	case "addit":
		$tpl = new Template("product_descr.html");
		$price = new Product();
    $metod = new Metod();
    $error = null;
    if ($id = $request->GetProperty("addID"))
    {
      $price->SetProperty("priceID", $id);
      $price->LoadFromDataBaseAdd();
  	  $metod->SetProperty("metID", $price->GetProperty('priceAddmetID'));
  	  $metod->LoadFromDataBase();
    }

    $tpl->LoadFromObject($metod);
    $tpl->LoadFromObject($price);
    $tpl->SetVar("KeyStandard1", PROJECT_KEYWORDS);
    $tpl->SetVar("KeyStandard", PROJECT_NAME);
    //���. �������� ��� ���������� ���� ������
    if (isset($_SESSION['userID'])) {
      $tpl->SetVar("userID", $_SESSION['userID']);
    }
    $tpl->SetVar("ShowOst", SHOW_OSTATKI);
     //����� ��� ����������
        /*if($price->GetProperty('PriceAddFiles')){
            //����� ����, ���� ��������� �� � ������ ����� ��������, ���� �����
            if(stripos($price->GetProperty('priceAddDesc'),"==FILES==") !== false){
                $tpl->SetVar("priceAddDesc", preg_replace("/===FILES===/i",$price->GetProperty('PriceAddFiles'),$price->GetProperty('priceAddDesc')));
            }else{
                $tpl->SetVar("priceAddDesc", $price->GetProperty('PriceAddFiles')."<br>".$price->GetProperty('priceAddDesc'));
            }
        }else{
            //�� ������ ������ ������� ������ ������
            $tpl->SetVar("priceAddDesc", preg_replace("/===FILES===/","",$price->GetProperty('priceAddDesc')));
        }*/
        $plain="";
        $plain1="";
        $string = $price->GetProperty('priceAddDesc');
        if ($string=='""') {
          $string='';
        }
	      $string = strip_tags($string);
        if ($string == $price->GetProperty('priceAddDesc')) {$plain="1";}
       	$string = substr($string, 0, 500);
      	$string = rtrim($string, "!,.-");	
        $string = substr($string, 0, strrpos($string, ' '))." ...";

        $string1 = $price->GetProperty('PriceAddTech');
        if ($string1=='""') {
          $string1='';
        }
	      $string1 = strip_tags($string1);
        if ($string1 == $price->GetProperty('PriceAddTech')) {$plain1="1";}
      	$string1 = substr($string1, 0, 500);
      	$string1 = rtrim($string1, "!,.-");	
        $string1 = substr($string1, 0, strrpos($string1, ' '))." ...";

        $images = split(';', $price->GetProperty('PriceAddImages'));
        $imagesarr = array();
        
        $count = 0;
        foreach ( $images as $image ) {
          if (trim($image) != "" ) {
				    $imagesarr[$count] = array('Images'=>trim($image)); 
            $count = $count + 1;
          }
        }

        $stringdesc = $price->GetProperty('priceAddDesc');
        if ($stringdesc=='""') {
          $stringdesc='';
        }
        if ($plain == "1") { $stringdesc = str_replace("\n","<br>", $stringdesc); }
        //$stringdesc = str_replace("\n","\n<br/>",$stringdesc);
        //$stringdesc = str_replace("\r","<br/>",$stringdesc);
        $stringtech = $price->GetProperty('PriceAddTech');
        if ($stringtech=='""') {
          $stringtech='';
        }
        if ($plain1 == "1") { $stringtech = str_replace("\n","<br>", $stringtech); }

        $tpl->SetVar("priceAddDesc", preg_replace("/===FILES===/","",$stringdesc));
        $tpl->SetVar("priceAddDescS", $string);
        $tpl->SetVar("priceAddTech", $stringtech);
        $tpl->SetVar("priceAddTechS", $string1);
        $tpl->SetVar("priceAddFiles", $price->GetProperty('PriceAddFiles'));
        $tpl->SetVar("ImagesCount", $count);
        //$tpl->SetVar("plain", $plain);
        //$tpl->SetVar("plain1", $plain);
        $tpl->SetLoop("priceImages", $imagesarr);




        //����� ����� ������� �������� ��������� �� �����-����
        if (isset($_COOKIE['isAdmin']) && 0 < intval($_COOKIE['isAdmin']) && intval($_COOKIE['isAdmin']) < 6) {
            $tpl->SetVar("isAdmin", intval($_COOKIE['isAdmin']));
        }

        $tpl->pparse();
		exit;
	break;

	case "cgallery":
		$tpl = new Template("gallery_list.html");
		$gallerylist = new GalleryList();
    $corporate = 0;

    $page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;
    $error = null;

    $searchString = "";  
    $tpl->SetVar("searchString", $searchString);

    $gallerylist->LoadFromDataBase($page, 1);
    $tpl->LoadFromObjectsList("gallerylist", $gallerylist);

    //paging
    $paging  = new Paging($page, $gallerylist->GetTotalCount(1), $tpl);

    //list
    $url = "/index.php?cmd=cgallery";
    $tpl->SetVar("URL", $url);
    $searchString1 = "&page=".$page."&";
    $tpl->SetVar("searchString1", $searchString1);
    $tpl->SetVar("pageName", "������������� �������");

		$page = Page::CreateForCustomer($tpl, PROJECT_NAME, "main", "������������� �������");
		$page->Output();
		exit;
	break;

	case "vacancy":
		$tpl = new Template("vacancy_list.html");

    $page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;
    $error = null;

    $searchString = "";  
    $tpl->SetVar("searchString", $searchString);

    $vacancylist = new VacancyList();
    $vacancylist->LoadFromDataBase(1, 1);
    $tpl->LoadFromObjectsList("vacancylist", $vacancylist);

    //paging
    $paging  = new Paging($page, $vacancylist->GetTotalCount(1), $tpl);

    //list
    $url = "/index.php?cmd=vacancy";
    $tpl->SetVar("URL", $url);
    $searchString1 = "&page=".$page."&";
    $tpl->SetVar("searchString1", $searchString1);
    $tpl->SetVar("pageName", "��������");

		$page = Page::CreateForCustomer($tpl, PROJECT_NAME, "main", "��������");
		$page->Output();
		exit;
	break;

	case "faq":
    $faqlist = new FaqList();
    $tpl = new Template("faq_list.html");
    $faqlist->LoadFromDataBaseForCustomer();
    $tpl->LoadFromObjectsList("faqlist", $faqlist);
    //$tpl->LoadFromObjectsList("faqlist1", $faqlist);
    $tpl->SetVar("pageName", "FAQ");
		$page = Page::CreateForCustomer($tpl, PROJECT_NAME, "main", "FAQ");
		$page->Output();
		exit;
	break;

	case "actions":
    $actions = new ActionsList();
    $tpl = new Template("actions_list.html");
    $actions->LoadFromDataBase(1,1);
    $tpl->LoadFromObjectsList("actionslist", $actions);
    $tpl->SetVar("pageName", "���� �����");
		$page = Page::CreateForCustomer($tpl, PROJECT_NAME, "main", "FAQ");
		$page->Output();
		exit;
	break;

	case "quest":
		$tpl = new Template("quest_list.html");

    $page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;
    $error = null;

    $searchString = "";  
    $tpl->SetVar("searchString", $searchString);

    if ($post->GetProperty("save"))
    {
      $quest = new Quest();
      $quest->LoadFromArray($post->GetAllProperties());
      $checkFunc = "CheckInformationUser";
      if($quest->GetProperty('key') == $_SESSION['key']) {
        if (!($error = $quest->$checkFunc()))
        {
          $quest->Update();
          $tpl->SetVar("sMessage", "��� ������ ������ � �������� � ����� ���������� � ��������� �����");
          $tpl->SetVar("questUser", "");
          $tpl->SetVar("questQuest", "");
        } else {
          $tpl->LoadFromObject($quest);
          $tpl->SetVar("sMessage", $error);
        }
      } else {
        $tpl->LoadFromObject($quest);
        $tpl->SetVar("sMessage", "�� ����� �������� ���");
      }      

    }

    $questlist = new QuestList();
    $questlist->LoadFromDataBase($page, 1);
    $tpl->LoadFromObjectsList("questlist", $questlist);

    //paging
    $paging  = new Paging($page, $questlist->GetTotalCount(1), $tpl);

    //list
    $url = "/index.php?cmd=quest";
    $tpl->SetVar("URL", $url);
    $searchString1 = "&page=".$page."&";
    $tpl->SetVar("searchString1", $searchString1);
    $tpl->SetVar("pageName", "�������� �����");

		$page = Page::CreateForCustomer($tpl, PROJECT_NAME, "main", "�������� �����");
		$page->Output();
		exit;
	break;

	case "metod":
		$tpl = new Template("metod_list.html");

    $page = $request->GetProperty('page') ? $request->GetProperty('page') : 1 ;
    $error = null;

    $searchString = "";  
    $tpl->SetVar("searchString", $searchString);

    $metodlist = new MetodList();
    $metodlist->LoadFromDataBase();
    $tpl->LoadFromObjectsList("metodlist", $metodlist);

    //list
    $url = "/index.php?cmd=metod";
    $tpl->SetVar("URL", $url);
    $searchString1 = "&page=".$page."&";
    $tpl->SetVar("searchString1", $searchString1);
    $tpl->SetVar("httpdocs", HTTP_DOCS);
    $tpl->SetVar("pageName", "������������ ��������");

		$page = Page::CreateForCustomer($tpl, PROJECT_NAME, "main", "������������ ��������");
		$page->Output();
		exit;
	break;

	case "empty":
		$tpl = new Template("static_page.html");
    $tpl->SetVar("statName", "������� ��������");
    $tpl->SetVar("statContent", "�����-�� �����");

		$page = Page::CreateForCustomer($tpl, PROJECT_NAME, "main", "������� ��������");
		$page->Output();
		exit;
	break;

  case "action":
	  $tpl = new Template("action.html");
    $action = new Action();
    $actionP = "1";
    if (isset($_REQUEST['ActionID'])) {
      $actionP = $_REQUEST['ActionID'];
    }
    $action->SetProperty("ActionID", intval($actionP));
    $action->LoadFromDataBase();
    $tpl->SetVar("statName", "����� ��������");
    $tpl->LoadFromObject($action);
		$page = Page::CreateForCustomer($tpl, PROJECT_NAME, "main", "����� ��������");
		$page->Output();

  break;

	default:
		if (file_exists(dirname(__FILE__)."/staticpages/".$get->GetProperty("cmd").".html"))
		{
			$tpl = new Template("../staticpages/" . $get->GetProperty("cmd").".html");
			$pageType = "sub_page";
		}
		else
		{
      if (isset($_REQUEST['lang'])) {
        if ($_REQUEST['lang'] == "en") {
          $tpl = new Template("en.html");
        } else {
          $tpl = new Template("ch.html");
        }
      } else {
        //$tpl = new Template("index.html");
        $pageType = "main";
        $tpl = new Template("static_page.html");
        $stat = new News();
        $statP = "1";
        if (isset($_REQUEST['statID'])) {
          $statP = $_REQUEST['statID'];
        }
        $stat->SetProperty("NewsID", intval($statP));
        $stat->LoadFromDataBase();
        //$tpl->LoadFromObject($stat);
        $tpl->SetVar("statName", $stat->GetProperty("NewsHeader"));
        $tpl->SetVar("statContent", $stat->GetProperty("NewsContent"));
        $keywords = $stat->GetProperty("NewsKeywords");
        $statName__ =$stat->GetProperty("NewsHeader");
      }
		}
	break;
}
if (!isset($keywords)) {
  $keywords = "";
}
if (!isset($statName__)) {
  $statName__ = "";
}
//����� ����� ������� ������� �� �����-����
if (isset($_COOKIE['isAdmin']) && 0 < intval($_COOKIE['isAdmin']) && intval($_COOKIE['isAdmin']) < 6) {
    $tpl->SetVar("statID", intval($statP));
    $tpl->SetVar("isAdmin", intval($_COOKIE['isAdmin']));
}

$tpl->SetVar("error", $error);
$page = Page::CreateForCustomer($tpl, PROJECT_NAME, $pageType, "Index Page", $keywords, $statName__);


// ������ ��� �� �� �����, ����� �������� ������� �����
$page->Output();

?>
