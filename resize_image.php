<?php
require_once(dirname(__FILE__)."/configure.php");
require_once(dirname(__FILE__)."/genlib.php");
require_once("image.php");

$request = new LocalObject($_REQUEST);
if ($dir = $request->GetProperty('dir'))
{
	$img = new Image($dir);
}
else
{
	$img = new Image();
}

if ($request->GetProperty("mode") == "noResize" || !$request->GetProperty('width') || !$request->GetProperty('height'))
{
	echo file_get_contents($img->filesDir."/".$request->GetProperty('fileName'));
}
else
{
	if($request->GetProperty('width') && $request->GetProperty('height'))
	{
		if ($request->GetProperty('fileName'))
		{
			$buffer = $img->ResizeImage($img->filesDir, $request->GetProperty('fileName'), $request->GetProperty('width'), $request->GetProperty('height'), $request->GetProperty('mode'));
		}
		else
		{
			//echo file_get_contents(HTTP_URL_IMAGES."_nofoto.jpg");
			$img->ResizeImage($img->filesDir, "_nofoto.jpg", $request->GetProperty('width'), $request->GetProperty('height'));
		}
	}
}
?>