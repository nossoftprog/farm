<?php
//require_once("configure.php");
require_once("recordset.php");
require_once("statement.php");
require_once("connection.php");

function GetConnection()
{
	static $connection;
	if (!$connection)
	{
		$connection = new Connection(DB_SERVER, DB_NAME, DB_USERNAME, DB_PASSWORD);
	}
	return $connection;
}

function GetStatement()
{
	$connection = GetConnection();
	return $connection->CreateStatement();
}

function JoinPath()
{
	if (substr(PHP_OS, 0, 3) == 'WIN')
	{
		$sep = "\\";
	}
	else
	{
		$sep = "/";
	}
    
	$args = func_get_args();
    return implode($sep, $args);
}

function pre_print_r($variable, $return = false)
{
	if ($return)
	{
		return print_r($variable, true);
	}
	else
	{
		echo "<pre>";
		print_r($variable);
		echo "</pre>";
	}
}
/*
if (!isset($GLOBALS["RandLoaded"]))
{
	$stmt = GetStatement();
	for ($i=rand(10,20); $i>0; $i--)
	{
		$query = "SELECT RAND()";
		$stmt->ExecuteSelect($query);
	}
	$GLOBALS["RandLoaded"] = 1;
}
*/
?>